import {
   ArrayField, Corruption, ObjectMeta, StringField, NumberField, BooleanField,
   UnknownField, ArrayMeta, ObjectField, TFields, ObjectConversionTree, IterableConversionTree,
   ObjectArrayField, ArrayMapField, FunctionField, ObjectArrayMeta, DuplicateEntry
} from '../../circle';


export const CORRUPTION_META = new ObjectMeta({
   builder: Corruption,
   fields: {
      value: new UnknownField({ isNullable: true }),
      key: new StringField(),
      message: new StringField(),
   }
});

export const DUPLICATE_ENTRY_META = new ObjectMeta({
   builder: DuplicateEntry,
   fields: {
      dataKey: new StringField(),
      jsonKey: new StringField(),
   }
});

export const CORRUPTION_ARRAY_FIELD = new ObjectArrayField({
   isNullable: true,
   meta: CORRUPTION_META
});

const BRANCHES_FIELD = new ArrayField({
   isNullable: true,
   meta: new ArrayMeta([
      new ObjectField({ get meta() { return OBJECT_CONVERSION_META; }, }),
      new ObjectField({ get meta() { return ITERABLE_CONVERSION_META; } }),
   ] as [ObjectField<ObjectConversionTree>, ObjectField<ObjectConversionTree | IterableConversionTree>]),
});

const OBJECT_CONVERSION_FIELDS: TFields<ObjectConversionTree> = {
   dataKey: new StringField(),
   jsonKey: new StringField({ isNullable: true }),
   hash: new StringField({ isNullable: true }),
   isTotallyCorrupted: new BooleanField({ isNullable: true }),
   isValid: new BooleanField({ isCalculated: true }),
   corrupted: CORRUPTION_ARRAY_FIELD,
   missing: CORRUPTION_ARRAY_FIELD,
   excluded: CORRUPTION_ARRAY_FIELD,
   semantic: CORRUPTION_ARRAY_FIELD,
   branches: BRANCHES_FIELD,
};



export const OBJECT_CONVERSION_META = new ObjectMeta({
   builder: ObjectConversionTree,
   fields: OBJECT_CONVERSION_FIELDS
});

const ITERABLE_CONVERSION_FIELDS: TFields<IterableConversionTree> = {
   ...OBJECT_CONVERSION_FIELDS,
   duration: new NumberField(),
   valuesCount: new NumberField(),
   hashes: new ArrayMapField({
      entry: {
         meta: new ObjectArrayMeta({ meta: DUPLICATE_ENTRY_META }),
      }
   }),
   addHash: new FunctionField(),
   checkHashDuplicates: new FunctionField(),
}

export const ITERABLE_CONVERSION_META = new ObjectMeta({
   builder: IterableConversionTree,
   isOfType: tree => tree.hasOwnProperty(`duration`),
   fields: ITERABLE_CONVERSION_FIELDS
});
