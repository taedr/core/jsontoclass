export interface IValidationError<T> {
   id: string;
   message: string;
   value: T;
}

export type TValidator<T> = (value: T) => string | undefined;

export type TValidatorEntry<T> = [string, TValidator<T>];
