import { DIVIDER } from '@taedr/utils';

export const ERRORS = {
   meta: {
      example:  [`01`, `Missing fields in iterable`],
      custom: [`02`, `'builder' must create custom class, not JS native`],
      duplicate: [`03`, `Second attempt to create meta (new ObjectMeta) with same class builder`],
      innerIterable: [`04`, `Iterable can have only one inner iterable entry`],
      parrent: [`05`, `Meta for the parrent class should be created before creating meta for the child class`],
      get: [`06`, `Can't get meta from such value`],
   },
   runtime: {
      update: `Entries validation failed`,
      exclude: `Validation failed for all fields`,
      date: `Can't create Date from such value`,
      hashDuplicates: `Duplicated hash`,
      unknwonKey: `Unknown key`,
   },
}

const groupByCode: Record<string, string> = Object.entries(ERRORS).reduce((acc, [key, list]) => {
   Object.values(list).map(([code]) => code)
      .forEach(code => acc[code] = key);
   return acc;
}, {} as Record<string, string>);

const ref = `https://taedr.gitbook.io/jsontoclass`;
export const getError = (id: string, key: string, [code, message]: string[]) => {
   const path = [id, key].filter(Boolean).join(DIVIDER.ARROW);
   const group = groupByCode[code];
   const docsRef = `See in doc: ${ref}/${group}/errors#${code}`;
   const mess = [path, message].filter(Boolean).join(` - `);

   console.error(docsRef);

   return new Error(mess);
}
