import { minSize, maxSize, truthy, falsy, enumerable, maxDate, maxLength, maxNumber, minDate, minLength, minNumber, pattern } from "../../fields";

export const VALIDATORS = {
   enum: enumerable,
   pattern,
   truthy,
   falsy,
   min: {
      number: minNumber,
      length: minLength,
      size: minSize,
      date: minDate,
   },
   max: {
      number: maxNumber,
      length: maxLength,
      size: maxSize,
      date: maxDate,
   },
};
