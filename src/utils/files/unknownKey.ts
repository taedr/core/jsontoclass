import { ERRORS, ObjectConversionTree, Corruption, ObjectMeta } from '../../circle';
import { TConsumer, FN_NULL } from '@taedr/utils';
// ==================================================
//                     Model
// ==================================================
export type TSUnknownKey = 'SAVE' | 'IGNORE' | 'ERROR';
export type TUnknownKeyHandler = TConsumer<IUnknownKeyCtx>;

export interface IUnknownKeyCtx {
   readonly meta: ObjectMeta<any>;
   readonly json: any;
   readonly data: any;
   readonly convertion: ObjectConversionTree;
}
// ==================================================
//                     Strategies
// ==================================================
function error({ meta, json, convertion }: IUnknownKeyCtx) {
   for (const key in json) {
      if (!meta.fields.strings.has(key)) {
         const error = new Corruption(json[key], key, ERRORS.runtime.unknwonKey);
         convertion.excluded.push(error);
      }
   }
}


function save({ meta, json, data }: IUnknownKeyCtx) {
   for (const key in json) {
      if (!meta.fields.strings.has(key)) {
         data[key] = json[key];
      }
   }
}
// ==================================================
//                     Allocator
// ==================================================
export const SUnknownKey: {
   [key in TSUnknownKey]: TUnknownKeyHandler;
} = {
   ERROR: error,
   SAVE: save,
   IGNORE: FN_NULL, // No need to do anything, because unkmown keys wasn't setted to instance
}
