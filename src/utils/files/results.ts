import { IterableConversionTree } from './corruption';

// ==================================================
//                     Result
// ==================================================
export interface IDuplicateEntry<T> {
   value: T;
   index: number;
}

export interface IConverted<T> {
   /** Array of all converted objects */
   readonly all: T[];
   /** Array of converted objects, where all fields passed validation */
   readonly valid: T[];
   /** Array of converted objects, where one or more field validation failed */
   readonly corrupted: T[];
   readonly duplicates: ReadonlyMap<string, IDuplicateEntry<T>[]>;
}

export interface IOrigin {
   /** Array of initial objects, same as provided in "values" */
   readonly all: any[];
   /** Array of initial objects, where one or more field validation failed   */
   readonly corrupted: any[];
   /** Array of initial objects, which didn't match any expected meta */
   readonly excluded: unknown[];
   /** Map
    * * key - object hash
    * * value - array of duplicates
    */
   readonly duplicates: ReadonlyMap<string, IDuplicateEntry<any>[]>;
}

export class ConvertResult<T> {
   /** Info about converted objects */
   readonly converted: IConverted<T>;
   /** Info about initial objects provided in "values" */
   readonly origin: IOrigin;
   /** Info about convertation,
    * which can be used to see log with "JTC.log.asString"
    * and "JTC.logToConsole"*/
   readonly corruption: IterableConversionTree;

   /** Will be 'true' if validation failed once or more times */
   public get isCorrupted() { return !this.corruption.isValid; }

   constructor(
      data: T[],
      tree: IterableConversionTree,
      json: any[],
   ) {
      this.corruption = tree;
      this.converted = {
         all: data,
         get valid() {
            return data.filter((_, i) => !tree.branches.find(({ dataKey }) => dataKey === i + ``));
         },
         get corrupted() {
            return data.filter((_, i) => tree.branches.find(({ dataKey }) => dataKey === i + ``));
         },
         get duplicates(): ReadonlyMap<string, IDuplicateEntry<T>[]> {
            const map = new Map<string, IDuplicateEntry<T>[]>();
            for (const hash in tree.hashes) {
               const entries = tree.hashes[hash];
               const values = entries.map(({ dataKey }) => ({ value: data[+dataKey], index: +dataKey }));
               map.set(hash, values)
            }
            return map;
         }
      };
      this.origin = {
         all: json,
         get corrupted() { return tree.branches.map(({ jsonKey }) => json[jsonKey as any]) },
         get excluded() {
            const branchesKeys = tree.branches
               .filter(({ isTotallyCorrupted }) => isTotallyCorrupted)
               .map(({ jsonKey }) => jsonKey);
            const exlucedKeys = tree.excluded.map(({ key }) => key);
            return [...branchesKeys, ...exlucedKeys].map(key => json[key as any]);
         },
         get duplicates(): ReadonlyMap<string, IDuplicateEntry<any>[]> {
            const map = new Map<string, IDuplicateEntry<any>[]>();
            for (const hash in tree.hashes) {
               const entries = tree.hashes[hash];
               const values = entries.map(({ jsonKey }) => ({ value: json[+jsonKey], index: +jsonKey }));
               map.set(hash, values)
            }
            return map;
         }
      };
   }
}
