import {
   Converter,
   ConvertResult, Deconverter,

   getLogAsString, getObjectMeta, IConvertCtx, IDeconverterCtx,
   IterableConversionTree, IUpdaterCtx,

   logCorruption, ObjectMeta, Updater
} from '../../circle';


export class JsonToClass {
   readonly log = Object.freeze({
      toConsole(tree: IterableConversionTree) { return logCorruption(tree); },
      asString(tree: IterableConversionTree) { return getLogAsString(tree); },
   });

   public getMeta<T extends object>(value: T): ObjectMeta<T> {
      return getObjectMeta(value);
   }

   public convert<T>(ctx: IConvertCtx<T>): ConvertResult<T> {
      return new Converter().convert(ctx);
   }


   public deconvert<T extends object>(ctx: IDeconverterCtx<T>): object {
      return new Deconverter().deconvert(ctx);
   }


   public update<T extends object>(ctx: IUpdaterCtx<T>) {
      return new Updater().update(ctx);
   }
}

/** Object that accumulates main capabilities of `jsontoclass` package */
export const JTC = new JsonToClass();
