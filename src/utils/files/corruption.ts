import { EType } from '@taedr/utils';
import { ERRORS } from '../../circle';


// ===============================================
//                   Entry
// ===============================================
export class Corruption {
   constructor(
      readonly value: unknown,
      readonly key: string,
      readonly message: string,
      type?: EType,
   ) {
      if (type === EType.object) this.value = `{...}`;
      if (type === EType.array) this.value = `[...]`;
   }
}


export class DuplicateEntry {
   constructor(
      readonly dataKey: string,
      readonly jsonKey: string
   ) { }
}

// ===============================================
//                   Tree
// ===============================================
export class ObjectConversionTree {
   public isTotallyCorrupted?: true;
   public hash?: string;

   readonly corrupted: Corruption[] = [];
   readonly missing: Corruption[] = [];
   readonly excluded: Corruption[] = [];
   readonly semantic: Corruption[] = [];
   readonly branches: ObjectConversionTree[] = [];

   public get isValid(): boolean {
      return [
         this.corrupted,
         this.excluded,
         this.branches,
         this.semantic,
         this.missing
      ].every(({ length }) => length === 0);
   }

   constructor(
      public dataKey: string,
      public jsonKey?: string,
   ) { }

   public static addBranch(
      tree: ObjectConversionTree,
      branch: ObjectConversionTree
   ): boolean {
      if (!branch.isValid) tree.branches.push(branch);
      if (branch.isTotallyCorrupted) {
         branch.dataKey = ``;
         return false;
      }

      return true;
   }
}


export class IterableConversionTree extends ObjectConversionTree {
   public hashes: Record<string, DuplicateEntry[]> = {};
   public duration = 0;
   public valuesCount = 0;

   public addHash(branch: ObjectConversionTree) {
      const hash = branch.hash ?? ``;
      const indexes = this.hashes[hash] ?? [];
      const entry = new DuplicateEntry(branch.dataKey, branch.jsonKey + ``);

      indexes.push(entry);
      this.hashes[hash] = indexes;
   }

   public checkHashDuplicates() {
      for (const hash in this.hashes) {
         const indexes = this.hashes[hash];
         if (indexes.length > 1) {
            const mess = indexes.map(({ jsonKey, dataKey }) => {
               if (jsonKey !== dataKey) return `${jsonKey}(${dataKey})`;
               return jsonKey;
            }).join(`, `);
            this.semantic.push(new Corruption(hash, mess, ERRORS.runtime.hashDuplicates));
         } else {
            delete this.hashes[hash];
         }
      }
   }
}
