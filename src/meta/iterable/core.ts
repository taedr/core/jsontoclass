import { EType, getType } from '@taedr/utils';
import {
   AField,
   AIterableField,

   ERRORS, getError,


   ObjectField,

   TValidator, UnknownField
} from '../../circle';

// ===============================================
//              Core
// ===============================================
export type TIterableValue<T> = Record<string, T> | Array<T>;


export interface IIterableMetaCtx<T> {
   entries: [AField<T>, ...AField<T>[]];
   validators: TValidator<T>[];
}

export abstract class AIterableMeta<T> {
   private _matcher: (type: EType, json: any) => AField<any> | undefined;
   private _isUnknownEntry?: true;

   readonly isNullableEntry?: true;

   constructor(
      readonly entries: [AField<T>, ...AField<T>[]],
   ) {
      const iterable = entries.filter(entry => entry instanceof AIterableField);
      const isNullable = entries.find(({ isNullable }) => isNullable);

      if (entries.length === 0) throw getError(``, ``, ERRORS.meta.example);
      if (iterable.length > 1) throw getError(``, ``, ERRORS.meta.innerIterable);
      if (isNullable) this.isNullableEntry = true;
      this._matcher = this.getMatcher(entries);
   }
   // -----------------------------------------------
   //                   Api
   // -----------------------------------------------
   public getMatchingEntry(value: any): AField<T> | undefined {
      const type = getType(value);
      const field = this._matcher(type, value);
      if (!field) {
         const isNull = type === EType.null && this.isNullableEntry;
         if (isNull || this._isUnknownEntry) return new UnknownField();
      }

      return field;
   }
   // -----------------------------------------------
   //                   Strategy
   // -----------------------------------------------
   private getMatcher(entries: AField<any>[]) {
      const isAllObjects = entries.every((field) => field instanceof ObjectField);
      const isHasUnknown = entries.find(field => field instanceof UnknownField);

      if (isHasUnknown) this._isUnknownEntry = true;

      if (entries.length === 1) {
         return this.singleEntryMatcher;
      } else if (isAllObjects) {
         return this.multiObjectmatcher;
      } else {
         return this.multiEntryMatcher;
      }
   }


   private singleEntryMatcher(type: EType) {
      const [first] = this.entries;
      const fieldType = (<any>first.constructor).type;
      if (type !== fieldType && !(type === EType.object && fieldType === EType.map)) return; // #Return#
      return first;
   }


   private multiObjectmatcher(type: EType, json: any): ObjectField<any> | undefined {
      if (type !== EType.object) return; // #Return#
      return this.getMetaForObject(json, this.entries as any);
   }


   private multiEntryMatcher(type: EType, json: any): AField<any> | undefined {
      if (type === EType.object) {
         const metas = this.entries.filter((field) => {
            return field instanceof ObjectField
         }) as ObjectField<any>[];
         return this.getMetaForObject(json, metas);
      } else {
         return this.entries.find(field => {
            const fieldType = (<any>field.constructor).type;
            return fieldType === type;
         });
      }
   }


   private getMetaForObject(json: any, entries: ObjectField<any>[]): ObjectField<any> | undefined {
      const jsonFields = Object.keys(json);
      const sorted = entries.sort((a, b) => +!!a.meta.isOfType - +!!b.meta.isOfType);
      let sampleMeta: ObjectField<object> | undefined;
      let topKeyMatchCount = 0;

      for (const entry of sorted) {
         let keyMatchCount = 0;

         if (entry.meta.isOfType) { // #Return#
            if (entry.meta.isOfType(json)) {
               return entry;
            } else {
               continue;
            }
         }

         for (const jsonField of jsonFields) {
            const isHasKey = entry.meta.fields.variables.has(jsonField as never);
            if (isHasKey) keyMatchCount++;
         }

         if (keyMatchCount > topKeyMatchCount) {
            topKeyMatchCount = keyMatchCount;
            sampleMeta = entry;
         } else if (sampleMeta && keyMatchCount === topKeyMatchCount) {
            const currDiff = entry.meta.fields.variables.size - keyMatchCount;
            const prevDiff = sampleMeta.meta.fields.variables.size - topKeyMatchCount;
            if (currDiff < prevDiff) sampleMeta = entry;
         }
      }

      return sampleMeta ?? entries[0];
   }
}



export interface IIterableValidators {
   primitives?: ReadonlySet<AField<unknown>>;
   iterables?: ReadonlySet<AIterableField<any, TIterableValue<any>>>;
   objects?: ReadonlySet<ObjectField<object>>;
}
