import { TStringKeys } from "@taedr/utils";
import {
   AField, UnknownField, IMapFieldCtx,
   BigintField, BooleanField, IArrayFieldCtx, IBigintFieldCtx, IBooleanFieldCtx, IFieldCtx, INumberFieldCtx,
   IObjectFieldCtx, IStringFieldCtx, NumberField, IUnknownFieldCtx,
   ObjectField, StringField, TEntryCtx, ArrayField, MapField, MapMeta
} from "../../circle";
import { AIterableMeta } from "./core";

// ===============================================
//                Core
// ===============================================
export class ArrayMeta<T> extends AIterableMeta<T> {
   constructor(entries: [AField<T>, ...AField<T>[]]) {
      super(entries);
   }
}
// ===============================================
//                Primitives
// ===============================================
export interface ITypedArrayMetaCtx<T, E extends IFieldCtx<T>> {
   entry?: Omit<E, 'isCalculated'>;
}


export class NumberArrayMeta extends ArrayMeta<number>{
   constructor(ctx?: TEntryCtx<number, INumberFieldCtx>) {
      super([new NumberField(ctx)]);
   }
}

export class StringArrayMeta extends ArrayMeta<string>{
   constructor(ctx?: TEntryCtx<string, IStringFieldCtx>) {
      super([new StringField(ctx)]);
   }
}

export class BooleanArrayMeta extends ArrayMeta<boolean>{
   constructor(ctx?: TEntryCtx<boolean, IBooleanFieldCtx>) {
      super([new BooleanField(ctx)]);
   }
}

export class BigintArrayMeta extends ArrayMeta<bigint>{
   constructor(ctx?: TEntryCtx<bigint, IBigintFieldCtx>) {
      super([new BigintField(ctx)]);
   }
}

export class UnknownArrayMeta extends ArrayMeta<bigint>{
   constructor(ctx?: TEntryCtx<unknown, IUnknownFieldCtx>) {
      super([new UnknownField(ctx)]);
   }
}
// ===============================================
//                Object
// ===============================================
export class ObjectArrayMeta<T extends object> extends ArrayMeta<T> {
   constructor(ctx: TEntryCtx<T, IObjectFieldCtx<T>>) {
      super([new ObjectField(ctx)]);
   }
}
// ===============================================
//                Dimensional
// ===============================================
export class DimensionalArrayMeta<T> extends ArrayMeta<T[]>{
   constructor(ctx: TEntryCtx<T[], IArrayFieldCtx<T>>) {
      super([new ArrayField(ctx)]);
   }
}
// ===============================================
//                Map
// ===============================================
export class MapArrayMeta<T > extends ArrayMeta<Record<string, T>>{
   constructor(ctx: TEntryCtx<Record<string, T>, IMapFieldCtx<T>>) {
      super([new MapField(ctx)]);
   }
}
// ===============================================
//                Type
// ===============================================
export type TArrayMeta<T> =
   T extends string ? StringArrayMeta :
   T extends number ? NumberArrayMeta :
   T extends boolean ? BooleanArrayMeta :
   T extends bigint ? BigintArrayMeta :
   T extends object ? T extends unknown[] ? DimensionalArrayMeta<T[number]> :
   T extends Record<string, TStringKeys<T>> ? ArrayMeta<MapMeta<T[TStringKeys<T>]>> :
   ObjectArrayMeta<T> : ArrayMeta<T>;
