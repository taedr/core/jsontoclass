import { TStringKeys } from "@taedr/utils";
import {
   AField, IFieldCtx, TEntryCtx, NumberField, INumberFieldCtx, IStringFieldCtx, StringField, IBooleanFieldCtx,
   BooleanField, IBigintFieldCtx, BigintField, IObjectFieldCtx, ObjectField, ArrayMeta
} from "../../circle";
import { AIterableMeta } from "./core";


// ===============================================
//                Core
// ===============================================
export class MapMeta<T> extends AIterableMeta<T> {
   constructor(entries: [AField<T>, ...AField<T>[]]) {
      super(entries);
   }
}
// ===============================================
//                Primitives
// ===============================================
export interface ITypedMapMetaCtx<T, E extends IFieldCtx<T>> {
   entry?: Omit<E, 'isCalculated'>;
}


export class NumberMapMeta extends MapMeta<number>{
   constructor(ctx?: TEntryCtx<number, INumberFieldCtx>) {
      super([new NumberField(ctx)]);
   }
}

export class StringMapMeta extends MapMeta<string>{
   constructor(ctx?: TEntryCtx<string, IStringFieldCtx>) {
      super([new StringField(ctx)]);
   }
}

export class BooleanMapMeta extends MapMeta<boolean>{
   constructor(ctx?: TEntryCtx<boolean, IBooleanFieldCtx>) {
      super([new BooleanField(ctx)]);
   }
}

export class BigintMapMeta extends MapMeta<bigint>{
   constructor(ctx?: TEntryCtx<bigint, IBigintFieldCtx>) {
      super([new BigintField(ctx)]);
   }
}
// ===============================================
//                Object
// ===============================================
export class ObjectMapMeta<T extends object> extends MapMeta<T> {
   constructor(ctx: TEntryCtx<T, IObjectFieldCtx<T>>) {
      super([new ObjectField(ctx)]);
   }
}
// ===============================================
//                Type
// ===============================================
export type TMapMeta<T> =
   T extends string ? StringMapMeta :
   T extends number ? NumberMapMeta :
   T extends boolean ? BooleanMapMeta :
   T extends bigint ? BigintMapMeta :
   T extends object ? T extends unknown[] ? MapMeta<ArrayMeta<T[number]>> :
   T extends Record<string, TStringKeys<T>> ? MapMeta<T[TStringKeys<T>]> :
   ObjectMapMeta<T> : MapMeta<T>;
