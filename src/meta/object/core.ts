import { IS, TNew, TPrimitive, TStringKeys, TSymbolKeys } from '@taedr/utils';
import {
   ADateField, AField, AIterableField,
   APrimitiveField, ERRORS,
   FieldValidationResult, FunctionField, getError,
   IFieldValidationCtx,
   IIterableFields, IStringFields,
   ISymbolFields, IValidationError,
   ObjectField,
   TFields,
   TIterableValue, TValidator
} from '../../circle';

export class ObjectMeta<T extends object> {
   readonly validators = new Map<string, TValidator<T>>();
   readonly fields: IObjectFields<T>;
   readonly builder: TNew<T>;
   readonly parrent?: ObjectMeta<object>;

   public get id(): string { return this.builder.name; }

   constructor(ctx: IObjectMetaCtx<T>) {
      this.builder = ctx.builder;

      const isNative = [
         Object, String, Number, Boolean,
         BigInt, Function, Array, Symbol
      ].some(native => <any>this.builder === native);

      if (ctx.builder.hasOwnProperty(META)) { // #Error#
         throw getError(this.id, ``, ERRORS.meta.duplicate);
      } else {
         const meta = this;
         Object.defineProperty(this.builder, META, { get() { return meta; } });
      }

      if (isNative) { // #Error#
         throw getError(ObjectMeta.name, this.id, ERRORS.meta.custom);
      }

      const parrent = this.getParrent();
      const getHash = ctx.getHash ?? parrent?.getHash ?? null;

      this.fields = this.getFields(ctx);

      if (getHash) this.getHash = getHash;
      if (ctx.isOfType) this.isOfType = ctx.isOfType;
      if (parrent) this.parrent = parrent;
      if (ctx.validators) this.validators = new Map(ctx.validators);
   }
   // -----------------------------------------------
   //                   Instance
   // -----------------------------------------------
   public getHash?(instance: T): string;
   public isOfType?(instance: T): boolean;
   // -----------------------------------------------
   //                   Validate
   // -----------------------------------------------
   public validate({
      value: instance,
      isCheckIterables = true,
      isCheckObjects = true,
      isCheckPrimitives = true,
      isCheckInstance = true,
      isCheckFunctions = true
   }: IObjectValidationCtx<T>): ObjectValidationResult<T> {
      const primitives = new Map<string, IValidationError<TPrimitive | Date>[]>();
      const objects = new Map<string, IValidationError<object>[]>();
      const iterables = new Map<string, IValidationError<TIterableValue<unknown>>[]>();
      const general: IValidationError<T>[] = [];
      const functions = new Map<string, IValidationError<Function>[]>();

      if (isCheckInstance) {
         for (const [id, validator] of this.validators) {
            const message = validator(instance);
            if (message) general.push({ id, message, value: instance });
         }
      }

      if (isCheckPrimitives) {
         for (const [key, field] of this.fields.primitives) {
            const value = (<any>instance)[key];
            const { errors } = field.validate({ value });
            if (errors.length) primitives.set(key + ``, errors as IValidationError<TPrimitive>[]);
         }
      }

      if (isCheckObjects) {
         for (const [key, field] of this.fields.objects) {
            const value = (<any>instance)[key];
            const { all } = field.meta.validate({ value });

            for (const [path, errors] of all) {
               if (errors.length) objects.set(`${key}.${path}`, errors as IValidationError<object>[]);
            }
         }
      }

      if (isCheckIterables) {
         for (const [key, field] of this.fields.iterables) {
            const value = (<any>instance)[key];
            const { all } = field.validate({ value, });

            for (const [path, errors] of all) {
               if (errors.length) iterables.set(`${key}.${path}`, errors);
            }
         }
      }

      if (isCheckFunctions) {
         for (const [key, field] of this.fields.functions) {
            const value = (<any>instance)[key].bind(instance);
            const { errors } = field.validate({ value });
            if (errors.length) functions.set(key + ``, errors);
         }
      }

      return new ObjectValidationResult(primitives, objects, iterables, functions, general);
   }
   // -----------------------------------------------
   //                   Fields
   // -----------------------------------------------
   private getFields({ fields }: IObjectMetaCtx<T>): IObjectFields<T> {
      const strings = new Map<TStringKeys<T>, AField<any>>();
      const symbols = new Map<TSymbolKeys<T>, AField<any>>();
      const variables = new Map<string, AField<unknown>>();
      const primitives = new Map<string, AField<any>>();
      const iterables = new Map<string, AIterableField<any, TIterableValue<any>>>();
      const objects = new Map<string, ObjectField<object>>();
      const functions = new Map<string, FunctionField<any>>();
      const calculated = new Map<string, AField<any>>();

      for (const _key in fields) {
         const key = _key as TStringKeys<T>;
         const field: AField<unknown> = (<any>fields)[key];
         strings.set(key, field);

         if (field instanceof APrimitiveField || field instanceof ADateField) {
            primitives.set(key, field);
         } else if (field instanceof FunctionField) {
            functions.set(key, field);
         } else if (field instanceof ObjectField) {
            objects.set(key, field);
         } else if (field instanceof AIterableField) {
            iterables.set(key, field as any);
         }

         if (field.isCalculated) {
            calculated.set(key, field);
         } else if (!(field instanceof FunctionField)) {
            variables.set(key, field);
         }
      }

      for (const _key of Object.getOwnPropertySymbols(fields)) {
         const key = _key as TSymbolKeys<T>;
         const field: AField<unknown> = (<any>fields)[key];

         symbols.set(key, field);
      }

      return {
         strings: strings as unknown as IStringFields<T, TStringKeys<T>>,
         symbols: symbols as unknown as ISymbolFields<T, TSymbolKeys<T>>,
         variables: variables as unknown as IIterableFields<T, TStringKeys<T>>,
         iterables,
         primitives,
         functions,
         objects,
         calculated,
      };
   }
   // -----------------------------------------------
   //                   Validators
   // -----------------------------------------------
   private getParrent(): ObjectMeta<object> | null {
      const builder = this.builder['prototype']['__proto__']['constructor'];
      if (builder === Object) return null; // #Return#
      const meta = builder[META];

      if (!meta) { // #Error#
         throw getError(this.id, builder.name, ERRORS.meta.parrent);
      }

      return meta;
   }
}
// ===============================================
//                   Ctx
// ===============================================
export const META = Symbol(`JTC_META`);

export interface IObjectMetaCtx<T extends object> {
   /** Class definition */
   builder: TNew<T>;
   /** Additional info about class instance fields */
   fields: TFields<T>;
   /** Validator for class instance */
   validators?: [string, TValidator<T>][];
   /** Checks if meta is suitable for such object */
   isOfType?(json: T): boolean;
   /** Will check for field `id` presents and
    *  based on it find duplicates in the root array */
   getHash?(instance: T): string;
}
// ===============================================
//                   Validation
// ===============================================
export interface IObjectValidationCtx<T extends object> extends IFieldValidationCtx<T> {
   isCheckPrimitives?: boolean;
   isCheckFunctions?: boolean;
   isCheckObjects?: boolean;
   isCheckInstance?: boolean;
   isCheckIterables?: boolean;
}

export class ObjectValidationResult<T extends object> extends FieldValidationResult<any> {
   private _all?: ReadonlyMap<string, IValidationError<any>[]>;

   public get all(): ReadonlyMap<string, IValidationError<any>[]> {
      if (!IS.null(this._all)) return this._all; // #Return#
      const map = new Map<string, IValidationError<any>[]>();
      if (this.instance.length) map.set(`INSTANCE`, this.instance);
      this.primitives.forEach((_errors, key) => map.set(key, _errors));
      this.objects.forEach((_errors, key) => map.set(key, _errors));
      this.iterables.forEach((_errors, key) => map.set(key, _errors));
      return this._all = map;
   }

   constructor(
      readonly primitives: ReadonlyMap<string, IValidationError<TPrimitive | Date>[]>,
      readonly objects: ReadonlyMap<string, IValidationError<object>[]>,
      readonly iterables: ReadonlyMap<string, IValidationError<TIterableValue<any>>[]>,
      readonly functions: ReadonlyMap<string, IValidationError<Function>[]>,
      readonly instance: IValidationError<T>[],
   ) {
      super(([
         ...primitives.values(),
         ...objects.values(),
         ...iterables.values(),
         ...functions.values(),
      ] as IValidationError<any>[][]).reduce((acc, errors) => acc.concat(errors), [...instance]));
   }
}
// ===============================================
//                   Getter
// ===============================================
export function getObjectMeta<T extends object>(value: T): ObjectMeta<T> {
   const error = getError(``, ``, ERRORS.meta.get);
   const isNotObject = !IS.object(value);
   if (isNotObject) throw error; // #Error#
   const builder: any = value['constructor'];
   const isPlainObject = builder === Object;
   if (isPlainObject) throw error; // #Error#
   const meta = builder[META];
   if (!meta) throw error; // #Error#

   return meta;
}


export interface IObjectFields<T extends object> {
   strings: IStringFields<T, TStringKeys<T>>;
   symbols: ISymbolFields<T, TSymbolKeys<T>>;
   primitives: ReadonlyMap<string, AField<any>>;
   iterables: ReadonlyMap<string, AIterableField<any, TIterableValue<any>>>;
   objects: ReadonlyMap<string, ObjectField<object>>;
   functions: ReadonlyMap<string, FunctionField<any>>;
   variables: IIterableFields<T, TStringKeys<T>>;
   calculated: ReadonlyMap<string, AField<any>>;
}
