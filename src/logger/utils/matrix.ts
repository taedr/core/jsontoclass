import { TMapper } from '@taedr/utils';
import { MIN_COL_WIDTH, GAP_BETWEEN_COLS, MAX_VAL_WIDTH } from './settings';

export function getMatrixConfig<T>(array: T[], fields: [string, TMapper<T, unknown>][]) {
   const rows = array.map(obj => {
      return fields.map(([_, get]) => {
         const val = get(obj);
         const format = formatValue(val);
         return format;
      });
   });

   const maxLenByCol = fields.map(([title], i) => {
      const col = rows.map(row => row[i] + ``);
      const max = [MIN_COL_WIDTH, title, ...col].reduce((max, { length }) => {
         return length > max ? length : max;
      }, Number.NEGATIVE_INFINITY);

      return max + GAP_BETWEEN_COLS;
   });

   return { rows, maxLenByCol };
}


export function formatValue(val: unknown) {
   let format = val;

   if (val instanceof Date) {
      format = val.toISOString();
   } else if (typeof val === 'object') {
      const stringed = JSON.stringify(val);

      if (stringed.length > MAX_VAL_WIDTH) {
         format = stringed.slice(0, MAX_VAL_WIDTH);
      } else {
         format = stringed;
      }
   } else if (val === '') {
      format = `''`;
   }

   return format;
}