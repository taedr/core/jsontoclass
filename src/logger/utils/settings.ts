export const GAP_BETWEEN_COLS = 4;
export const MIN_COL_WIDTH = ``.padEnd(9);
export const MAX_VAL_WIDTH = 20;
export const BOLD = 'font-weight: bold;';
export const MAX_SHOWN_BRANCHES = 30;
export const HEADER_COLORS = {
   log: `hsl(0, 100%, 30%)`,
   duration: `hsl(0, 0%, 0%)`,
   id: `hsl(280, 100%, 30%)`,
   first: `hsl(189, 100%, 30%)`
};
export const TREE_COLORS = [
   HEADER_COLORS.first,
   `hsl(152, 100%, 30%)`,
   `hsl(71, 100%, 30%)`,
   `hsl(207, 100%, 30%)`,
   `hsl(325, 100%, 30%)`,
   `hsl(39, 100%, 30%)`,
   `hsl(286, 100%, 30%)`,
   `hsl(216, 100%, 30%)`,
   `hsl(276, 100%, 30%)`,
   `hsl(355, 100%, 30%)`,
];
