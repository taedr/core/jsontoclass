import { IterableConversionTree, ObjectConversionTree } from '../../utils';
import { BOLD, TREE_COLORS, HEADER_COLORS, MAX_SHOWN_BRANCHES } from '../utils/settings';
import { TFunction, TMapper, DIVIDER } from '@taedr/utils';
import { getMatrixConfig } from '../utils/matrix';

// ===============================================
//                Model
// ===============================================
type TGroup = [string, object[], TFunction];

class Title {
   constructor(
      readonly key: string | number,
      readonly color: string
   ) {
   }
}
// ===============================================
//                Logger
// ===============================================
export function logCorruption(tree: IterableConversionTree) {
   const titles = [
      new Title(`<- JTC CORRUPTION LOG`, HEADER_COLORS.log),
      new Title(`${tree.valuesCount} in ${tree.duration.toFixed(2)} Ms`, HEADER_COLORS.duration),
      new Title(`${tree.dataKey}`, HEADER_COLORS.id),
   ];

   if (isSingleBranch(tree)) {
      const branch = tree.branches[0];
      const isSingle = tree.valuesCount === 1;
      const id = isSingle ? branch.hash : getTitle(branch);

      if (id) {
         titles.push(new Title(id, HEADER_COLORS.first));
      }

      logTree(branch, titles, -1);
   } else {
      logTree(tree, titles, -1);
   }
}
// ----------------------------------------------
//                Tree
// ----------------------------------------------
function logTree(tree: ObjectConversionTree, titles: Title[], level: number) {
   const groups = getGroups(tree, titles, level);

   if (groups.length === 0) return; // #Return#

   const pathString = '%c' + titles.map(node => node.key).join(`${DIVIDER.ARROW}%c`);
   const levelColors = titles.map(node => `color: ${node.color};`);

   if (groups.length === 1) {
      const [[title, , logger]] = groups;
      console.groupCollapsed(pathString + `${DIVIDER.ARROW}[${title.toUpperCase()}]`, ...levelColors);
      logger();
      console.groupEnd();
   } else {
      console.groupCollapsed(pathString, ...levelColors);
      for (const [title, , logger] of groups) {
         console.groupCollapsed(title);
         logger();
         console.groupEnd();
      }
      console.groupEnd();
   }
}
// ----------------------------------------------
//                Groups
// ----------------------------------------------
function getGroups(tree: ObjectConversionTree, titles: Title[], level: number) {
   const { corrupted, excluded, semantic, branches, missing } = tree;

   const merged = [...corrupted, ...semantic, ...excluded, ...missing]
   const groupsAll: TGroup[] = [
      [`Corrupted`, merged, () => {
         logToConsole(merged, [
            [`Field`, cr => cr.key],
            [`Value`, cr => cr.value],
            [`Message`, cr => cr.message],
         ]);
      }],
      [`Branches`, branches, () => {
         logBranches(tree, titles, level);
      }],
   ]

   const groups = groupsAll.filter(([, values]) => values.length);

   return groups;
}
// ----------------------------------------------
//                Branches
// ----------------------------------------------
function logBranches(tree: ObjectConversionTree, titles: Title[], level: number, isParrentFlat = false) {
   const nextLevel = level + 1;
   let showCount = tree.branches.length;

   if (showCount > MAX_SHOWN_BRANCHES) {
      showCount = MAX_SHOWN_BRANCHES;
      console.log('%c' +
         `(Too many repaired elements, only first ${showCount} will be shown.` +
         ` Use 'JTC.logRepairs' and saved 'repairTree' from result to see full log, rendering can take a while)`
         , BOLD);
   }

   for (let i = 0; i < showCount; i++) {
      const branch = tree.branches[i];
      const title = new Title(getTitle(branch), TREE_COLORS[nextLevel]);
      const isBranchflat = isSingleBranch(branch);
      const mergedTitles = isParrentFlat ? [...titles, title] : [title];

      if (isBranchflat) {
         logBranches(branch, mergedTitles, nextLevel, true)
      } else {
         logTree(branch, mergedTitles, nextLevel);
      }
   }
}
// ----------------------------------------------
//                Utils
// ----------------------------------------------
function isSingleBranch(tree: ObjectConversionTree): boolean {
   return tree.branches.length === 1 &&
      tree.excluded.length === 0 &&
      tree.semantic.length === 0 &&
      tree.corrupted.length === 0;
}


function getTitle(tree: ObjectConversionTree): string {
   const { dataKey, jsonKey, hash, isTotallyCorrupted } = tree;
   let title = isTotallyCorrupted ? `EXCLUDED` : dataKey;
   if (jsonKey && dataKey !== jsonKey) title = `${jsonKey} (${title})`;
   if (hash) title += ` |${hash}|`;
   return title;
}


export function logToConsole<T>(objects: T[], fields: [string, TMapper<T, unknown>][]) {
   const { rows, maxLenByCol } = getMatrixConfig(objects, fields);
   const header = fields.map(([title], i) => title.padEnd(maxLenByCol[i] + 2)).join(``);

   console.log('%c' + header, BOLD);

   for (const row of rows) {
      const values: unknown[] = [];

      for (let i = 0; i < row.length; i++) {
         const col = row[i];
         values.push(col);
         values.push(''.padEnd(maxLenByCol[i] - (col + ``).length));
      }

      console.log(...values);
   }
}
