import { ERRORS, IterableConversionTree, Corruption, ObjectConversionTree } from '../../circle';
import { formatValue } from '../utils/matrix';
import { DIVIDER } from '@taedr/utils';

export function getLogAsString(tree: IterableConversionTree): string {
   return getConversionTree(tree, []);
}


function getConversionTree(tree: ObjectConversionTree, prefix: string[]): string {
   const corruption = [tree.corrupted, tree.excluded, tree.missing, tree.semantic]
      .reduce((acc, group) => acc.concat(group), [])
      .sort((a, b) => a.key.localeCompare(b.key))
      .map(cr => getCorruption(cr, prefix));
   const branches = tree.branches.map(branch => {
      const { jsonKey, dataKey, isTotallyCorrupted } = branch;
      let _key = isTotallyCorrupted ? `EXCLUDED` : dataKey;

      if (!!jsonKey && jsonKey !== _key) _key = `${jsonKey} (${_key})`

      const _prefix = [...prefix, _key];
      const totalMess = `${[..._prefix, `{...}`].join(DIVIDER.ARROW)} | ${ERRORS.runtime.exclude}`;
      const totally = isTotallyCorrupted ? totalMess : ``;

      return [
         totally,
         getConversionTree(branch, _prefix),
      ].filter(Boolean).join(`\n`);
   })
   const log = [...corruption, ...branches].join(`\n`);

   return log;
}


function getCorruption({ key, value, message }: Corruption, prefix: string[]): string {
   const format = formatValue(value);
   const path = [...prefix, key, format].join(DIVIDER.ARROW);
   const full = path + ` | ` + message;

   return full;
}
