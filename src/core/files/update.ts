import { getType, IS, TDeepPartial } from '@taedr/utils';
import { AIterableField, ArrayMeta, ERRORS, IterableConversionTree, META, ObjectField, ObjectMeta } from '../../circle';
import { Converter } from './converter';

// ==================================================
//                     Model
// ==================================================
export interface IUpdateEntry<T extends object> {
   value: T;
   partial: Readonly<T | TDeepPartial<T>>;
}

export interface IUpdaterCtx<T extends object> {
   id: string;
   metas: [ObjectMeta<T>, ...ObjectMeta<T>[]];
   entries: IUpdateEntry<T>[];
}

export class UpdaterError extends Error {
   constructor(
      readonly valuesTree: IterableConversionTree,
      readonly partialsTree: IterableConversionTree,
   ) {
      super(ERRORS.runtime.update);
   }
}
// ==================================================
//                     Class
// ==================================================
export class Updater {
   constructor() {

   }
   // -----------------------------------------------
   //                   Api
   // -----------------------------------------------
   public update<T extends object>(ctx: IUpdaterCtx<T>): T[] {
      const { values, partials } = this.validateEntries(ctx);

      for (let i = 0; i < values.length; i++) {
         this.updateObject(values[i], partials[i]);
      }

      return values;
   }
   // -----------------------------------------------
   //                   Object
   // -----------------------------------------------
   private updateObject<T extends Record<string, any>>(value: T, partial: T) {
      const meta: ObjectMeta<any> = (<any>value)['constructor'][META];

      for (const [key, field] of meta.fields.strings) {
         const valueField = value[key];
         const partialField = partial[key];

         if (IS.null(partialField)) continue; // #Continue#

         if (field instanceof ObjectField) {
            this.updateObject(valueField, partialField);
         } else if (field instanceof AIterableField) {
            this.updateIterable(valueField, partialField);
         } else {
            (<any>value)[key] = partial[key];
         }
      }

      for (const [symbol] of meta.fields.symbols) {
         const updater = partial[symbol as keyof T];
         if (IS.null(updater)) continue; // #Continue#
         value[symbol as keyof T] = updater;
      }
   }
   // -----------------------------------------------
   //                   Iterable
   // -----------------------------------------------
   private updateIterable<T>(values: T, partials: T) {
      for (const key in values) {
         const valueEntry = values[key];
         const partialEntry = partials[key];
         const type = getType(valueEntry);

         if (IS.null(partialEntry)) continue; // #Continue#

         if (IS.type.object(type)) {
            this.updateObject(valueEntry as any, partialEntry);
         } else {
            values[key] = partials[key];
         }
      }
   }
   // -----------------------------------------------
   //                   Validate
   // -----------------------------------------------
   private validateEntries<T extends object>({ id, entries, metas }: IUpdaterCtx<T>) {
      const solid = metas.map(meta => new ObjectField({ meta })) as [ObjectField<T>];
      const partials = metas.map(meta => new ObjectField({ meta, isPartial: true })) as [ObjectField<T>];

      const values = new Converter().convert({
         id: `${id} - Values`,
         meta: new ArrayMeta(solid),
         values: entries.map(({ value }) => value),
         SSymbols: `SHARE`,
      });

      const partial = new Converter().convert({
         id: `${id} - Partials`,
         meta: new ArrayMeta(partials),
         values: entries.map(({ partial }) => partial),
         SSymbols: `SHARE`,
      });

      if (values.isCorrupted || partial.isCorrupted) { // #Error#
         throw new UpdaterError(values.corruption, partial.corruption);
      }

      return {
         values: values.converted.all,
         partials: partial.converted.all,
      };
   }
}
