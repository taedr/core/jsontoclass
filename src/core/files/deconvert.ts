import { IS, TNew } from '@taedr/utils';
import { ADateField, AField, AIterableField, ArrayField, BooleanField, META, ObjectField, ObjectMeta, StringField } from '../../circle';

// ==================================================
//                     Ctx
// ==================================================
export interface IDeconverterCtx<T extends object> {
   value: T;
   isDeleteEmpty?: boolean;
   leaveAsIs?: TNew<object>[];
}
// ==================================================
//                     Control
// ==================================================

export class Deconverter {
   private _isDeleteEmpty = true;
   private _leaveAsIs = new Set<TNew<object>>();

   // -----------------------------------------------
   //                   Api
   // -----------------------------------------------
   public deconvert<T extends object>(ctx: IDeconverterCtx<T>): object {
      const { value, isDeleteEmpty, leaveAsIs } = ctx;
      const meta: ObjectMeta<any> = (<any>value?.constructor)?.[META];

      this._isDeleteEmpty = isDeleteEmpty ?? true;
      this._leaveAsIs = new Set(leaveAsIs ?? []);

      if (meta) {
         return this.deconvertObject(meta, value);
      } else {
         return value;
      }
   }
   // -----------------------------------------------
   //                   Internal
   // -----------------------------------------------
   private deconvertObject<T extends object>(
      meta: ObjectMeta<T>,
      instance: T,
   ): T {
      if (this._leaveAsIs.has(meta.builder)) return instance; // #Return#
      const copy: any = {};
      for (const [key, field] of meta.fields.variables) {
         const value: any = instance[key];
         const isCanDelete = this.isCanDelete(value, field);

         if (isCanDelete) {
            continue; // #Continue#
         } else if (field instanceof ObjectField) {
            copy[key] = this.deconvertObject(field.meta, value);
         } else if (field instanceof AIterableField) {
            copy[key] = this.deconvertIterable(field as any, value);
         } else if (field instanceof ADateField) {
            copy[key] = field.deconvert(value);
         } else {
            copy[key] = value;
         }
      }

      return copy;
   }


   private deconvertIterable<T>(
      field: AIterableField<any, any>,
      iterable: T,
   ) {
      const copy: any = field instanceof ArrayField ? [] : {};

      for (const key in iterable) {
         const value = iterable[key] as any;
         const match = field.meta.getMatchingEntry(value);

         if (!match) {
            continue; // #Continue#
         } else if (match instanceof ObjectField) {
            copy[key] = this.deconvertObject(match.meta, value);
         } else if (match instanceof AIterableField) {
            copy[key] = this.deconvertIterable(match, value);
         } else if (match instanceof ADateField) {
            copy[key] = match.deconvert(value);
         } else {
            copy[key] = value;
         }
      }

      return copy;
   }


   private isCanDelete(value: any, field: AField<any>): boolean {
      if (!field.isNullable || !this._isDeleteEmpty) return false; // #Return#
      if (IS.null(value)) return true; // #Return#

      if (field instanceof BooleanField) return value === false;
      if (field instanceof StringField) return value === ``;
      if (field instanceof AIterableField) return Object.keys(value).length === 0;

      return false;
   }
}
