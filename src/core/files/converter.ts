import { EType, FN_NULL, getHrTime, getType, IS } from '@taedr/utils';
import {
   ADateField, AIterableField, AIterableMeta,
   APrimitiveField, ArrayField, ConvertResult, Corruption, ERRORS,
   IArrayFieldCtx, IterableConversionTree,
   ObjectConversionTree,
   ObjectField, ObjectMeta, SUnknownKey, TSUnknownKey,
   UnknownField, AField, IValidationError
} from '../../circle';
import { getLogAsString, logCorruption } from '../../logger';


export class Converter {
   private _isFreeze?: boolean;
   private _unknownKeys = SUnknownKey.IGNORE;
   private _symbols?: TSSymbols;

   constructor() {
   }
   // -----------------------------------------------
   //                   Api
   // -----------------------------------------------
   public convert<T>(ctx: IConvertCtx<T>): ConvertResult<T> {
      const converted: T[] = [];
      const tree = new IterableConversionTree(ctx.id);
      const result = new ConvertResult<T>(converted, tree, ctx.values);
      const field = new ArrayField(ctx);
      const unknownKeys = ctx.SUnknownKey ?? `IGNORE`;

      if (IS.null(ctx.values) && !field.isNullable) {
         tree.corrupted.push(new Corruption(ctx.values, `ARRAY`, `Origin values is empty`));
      }
      if (!ctx.values?.length) return result; // #Return#

      this._unknownKeys = SUnknownKey[unknownKeys];

      if (ctx.isFreeze) this._isFreeze = true;
      if (ctx.SSymbols === `SHARE`) this._symbols = `SHARE`;

      this.iterableConverter(field, ctx.values, converted, tree, false);

      if (IS.running.browser) {
         logCorruption(tree);
      } else if (IS.running.node) {
         const log = getLogAsString(tree);
         if (log.length) console.warn(log);
      }

      return result;
   }
   // -----------------------------------------------
   //                   Object
   // -----------------------------------------------
   private objectConverter(
      meta: ObjectMeta<any>,
      json: any,
      data: any,
      convertion: ObjectConversionTree,
      isPartial: boolean,
   ) {
      let validFields = 0;
      let isValid = false;

      for (const [key, field] of meta.fields.variables) {
         const fieldType = (<any>field.constructor)['type'];
         const jsonValue = json[key];
         const jsonType = getType(jsonValue);

         if (isValid) validFields += 1;
         isValid = true;

         if (jsonType !== fieldType) {
            const isEmpty = jsonType === EType.null;

            if (isEmpty && (isPartial || field.isNullable)) { // #Continue#
               if (isPartial && isEmpty) {
                  delete data[key];
               }
               continue;
            } else if (fieldType === EType.unknown && !isEmpty) { // #Continue#
               data[key] = jsonValue;
               continue;
            } else if (fieldType === EType.map && jsonType === EType.object) {
               // Expectable
            } else if (jsonValue instanceof Date && field instanceof ADateField) {
               // Expectable when converting instance
               data[key] = new Date(jsonValue);
               continue;
            } else {
               isValid = false;
               if (isEmpty) {
                  const view = jsonValue === null ? null : `undefined`;
                  const error = new Corruption(view, key, `Value is required`, jsonType);
                  convertion.missing.push(error);
               } else {
                  const mess = `Expected ${EType[fieldType]}, but got ${EType[jsonType]}`;
                  const error = new Corruption(jsonValue, key, mess, jsonType);
                  convertion.corrupted.push(error);
               }

               continue;
            }
         }

         const errors: IValidationError<any>[] = [];

         if (field instanceof APrimitiveField) {
            data[key] = jsonValue;

            if ( field.validators.size) {
               const validation = (<AField<any>>field).validate({ value: data[key] });
               errors.push(...validation.errors);
            }
         } else if (field instanceof ObjectField) {
            const instance = data[key] = data[key] ?? new field.meta.builder();
            const branch = new ObjectConversionTree(key);
            const isPartialObject = field.isPartial || isPartial;

            this.objectConverter(
               field.meta,
               jsonValue,
               instance,
               branch,
               isPartialObject
            );

            ObjectConversionTree.addBranch(convertion, branch);
            if (branch.isTotallyCorrupted) isValid = false;
         } else if (field instanceof AIterableField) {
            const iterable: any[] = data[key] = data[key] ?? (field instanceof ArrayField ? [] : {});
            const branch = new IterableConversionTree(key);

            this.iterableConverter(
               field,
               jsonValue,
               iterable,
               branch,
               isPartial
            );

            ObjectConversionTree.addBranch(convertion, branch);
            if (branch.isTotallyCorrupted) isValid = false;
         } else if (field instanceof ADateField) {
            data[key] = field.convert(jsonValue);

            if (data[key].toDateString() === 'Invalid Date') {
               const stub = new meta.builder();
               const error = new Corruption(jsonValue, key, ERRORS.runtime.date);
               convertion.corrupted.push(error);
               data[key] = stub[key];
               isValid = false;
            } else if (field.validators.size) {
               const validation = field.validate({ value: data[key] });
               errors.push(...validation.errors);
            }
         } else if (field instanceof UnknownField) {
            data[key] = jsonValue;
            if (field.validators.size) {
               const validation = field.validate({ value: data[key] });
               errors.push(...validation.errors);
            }
         }

         for (const { value, message } of errors) {
            const corruption = new Corruption(value, key, message, fieldType);
            convertion.semantic.push(corruption);
         }
      }

      if (isValid) validFields += 1;

      for (const [symbol, initer] of meta.fields.symbols) {
         if (IS.null(json[symbol]) && !isPartial) {
            data[symbol] = initer(data);
         } else if (this._symbols === `SHARE`) {
            data[symbol] = json[symbol]
         }
      }

      for (const [key, field] of meta.fields.calculated) {
         if (!field.validators.size) continue; // #Continue#
         const { errors } = field.validate({ value: data[key] });
         for (const { value, message } of errors) {
            const type = (<any>field)['constructor']['type'];
            const corruption = new Corruption(value, key, message, type);
            convertion.semantic.push(corruption);
         }
      }

      for (const [key, field] of meta.fields.functions) {
         if (!field.validators.size) continue; // #Continue#
         const { errors } = field.validate({ value: data[key].bind(data) });
         for (const { value, message } of errors) {
            const corruption = new Corruption(value, key, message);
            convertion.semantic.push(corruption);
         }
      }

      for (const [_, validator] of meta.validators) {
         const message = validator(data);
         if (message) {
            const corruption = new Corruption(data, `INSTANCE`, message, EType.object);
            convertion.semantic.push(corruption);
         }
      }

      if (this._unknownKeys !== FN_NULL) this._unknownKeys({ meta, json, data, convertion });
      if (this._isFreeze) Object.freeze(data);
      if (validFields === 0) convertion.isTotallyCorrupted = true;
   }
   // -----------------------------------------------
   //                   Iterable
   // -----------------------------------------------
   private iterableConverter(
      field: AIterableField<any, any>,
      json: any,
      data: any,
      convertion: IterableConversionTree,
      isPartial: boolean,
   ) {
      const timeStart = getHrTime();
      const entries = { count: 0, valid: 0 };

      const getArrayIndex = () => (data as unknown[]).length + ``;
      const getMapField = (jsonKey: string) => jsonKey;
      const getDataIndex = field instanceof ArrayField ? getArrayIndex : getMapField;
      let acceptableTypes: string | undefined;

      for (const jsonIndex in json) {
         const jsonValue = json[jsonIndex];
         const dataIndex = getDataIndex(jsonIndex);
         const match = field.meta.getMatchingEntry(jsonValue);

         entries.count += 1;

         if (!match && isPartial) { // #Continue#
            entries.valid += 1;
            data[dataIndex] = jsonValue;
            continue;
         }

         if (match === undefined) { // #Continue#
            const jsonType = getType(jsonValue);
            if (!acceptableTypes) acceptableTypes = this.getAcceptedTypes(field.meta);
            const message = `${acceptableTypes}, but got "${EType[jsonType]}"`;
            const corruption = new Corruption(jsonValue, jsonIndex, message, jsonType);
            convertion.excluded.push(corruption);
            continue;
         }

         if (match instanceof ObjectField) {
            const dataValue = new match.meta.builder();
            const branch = new ObjectConversionTree(dataIndex, jsonIndex);
            const isObjectPartial = isPartial || !!match.isPartial;

            this.objectConverter(
               match.meta,
               jsonValue,
               dataValue,
               branch,
               isObjectPartial,
            );

            for (const [_, validator] of match.validators) {
               const message = validator(data);
               if (message) {
                  const corruption = new Corruption(data, `INSTANCE`, message, EType.object);
                  convertion.semantic.push(corruption);
               }
            }

            const isShouldSave = ObjectConversionTree.addBranch(convertion, branch);

            if (!isShouldSave) continue; // #Continue#

            if (match.meta.getHash) {
               branch.hash = match.meta.getHash(dataValue);
               convertion.addHash(branch);
            }

            data[dataIndex] = dataValue;
         } else if (match instanceof AIterableField) {
            const dataValue = match instanceof ArrayField ? [] : {};
            const branch = new IterableConversionTree(dataIndex, jsonIndex);

            this.iterableConverter(
               match,
               jsonValue,
               dataValue,
               branch,
               isPartial,
            );

            const isShouldSave = ObjectConversionTree.addBranch(convertion, branch);

            if (!isShouldSave) continue; // #Continue#

            data[dataIndex] = dataValue;
         } else {
            const entryValue = data[dataIndex] = jsonValue;

            if (!IS.null(entryValue) && match.validators.size) {
               const { errors } = match.validate({ value: entryValue });
               for (const { value, message } of errors) {
                  const corruption = new Corruption(value, dataIndex, message);
                  convertion.semantic.push(corruption);
               }
            }
         }

         entries.valid += 1;
      }

      if (field.validators.size) {
         const { instance } = field.validate({ value: data, isCheckEntries: false });

         for (const { message } of instance) {
            convertion.corrupted.push(new Corruption(`[...]`, `INSTANCE`, message));
         }
      }

      convertion.duration = getHrTime() - timeStart;
      convertion.valuesCount = entries.count;
      convertion.checkHashDuplicates();
      if (this._isFreeze) Object.freeze(data);
      if (entries.valid === 0) convertion.isTotallyCorrupted = true;
   }

   private getAcceptedTypes({ entries }: AIterableMeta<any>) {
      const types = entries.map((field) => EType[(<any>field.constructor)['type']]);
      const unique = [...new Set(types)];
      const acceptableTypes = unique.join(`, `);
      const suffix = unique.length > 1 ? `s` : ``;

      return `Members can only be of "${acceptableTypes}" type${suffix}`;
   }
}
// ==================================================
//                     Ctx
// ==================================================
export type TSSymbols = 'INIT' | 'SHARE';

export interface IConvertCtx<T> extends Omit<IArrayFieldCtx<T>, 'isCalculated' | 'isNullable'> {
   id: string;
   values: any[];
   /** Determines starategy if met unknown key:
    * * `IGNORE` (default) - will check only for required keys, others will be omitted
    * * `ERROR` - will consider it as corruption
    * * `SAVE` - will save such key on converted object
    */
   SUnknownKey?: TSUnknownKey;
   SSymbols?: TSSymbols;
   /** Will deeply freeze converted values. Default `false` */
   isFreeze?: boolean;
}
