import { IS } from "@taedr/utils";
import { IValidationError, TValidator } from "../../circle";

export abstract class AField<T> {
   readonly validators: ReadonlyMap<string, TValidator<T>> = new Map<string, TValidator<T>>();
   readonly isCalculated?: true;
   readonly isNullable?: true;

   constructor({
      isNullable,
      isCalculated,
      validators,
   }: IFieldCtx<T> = {}) {
      if (isNullable) this.isNullable = true;
      if (isCalculated) this.isCalculated = true;
      if (validators?.length) this.validators = new Map(validators);
   }
   // -----------------------------------------------
   //                   Api
   // -----------------------------------------------
   public validate({ value }: IFieldValidationCtx<T>): FieldValidationResult<T> {
      const errors: IValidationError<T>[] = [];

      if (IS.null(value) && !this.isNullable) {
         errors.push({ id: `null`, message: `Value is required`, value });
      } else {
         for (const [id, validator] of this.validators) {
            const message = validator(value);
            if (message) errors.push({ id, message, value });
         }
      }

      return new FieldValidationResult(errors);
   }
}
// ===============================================
//                   Ctx
// ===============================================
export interface IFieldCtx<T> {
   isNullable?: boolean;
   isCalculated?: boolean;
   validators?: [string, TValidator<T>][];
}

export type TEntryCtx<T, F extends IFieldCtx<T>> = Omit<F, 'isCalculated'>;
// ===============================================
//                   Validation
// ===============================================
export interface IFieldValidationCtx<T> {
   value: T;
}

export class FieldValidationResult<T> {
   public get isValid() { return this.errors.length === 0; }
   public get isInvalid() { return this.errors.length > 0; }

   constructor(
      readonly errors: IValidationError<T>[],
   ) { }
}
