import { TStringKeys, TSymbolKeys } from '@taedr/utils';
import { TArrayField } from '../objects/array';
import { ADateField, TDatePrimitive } from '../objects/date';
import { TMapField } from '../objects/map';
import { ObjectField } from '../objects/object';
import { FunctionField } from '../other/function';
import { TSymbolIniter } from '../other/symbol';
import { UnknownField } from '../other/unknown';
import { BigintField } from '../primitives/bigint';
import { BooleanField } from '../primitives/boolean';
import { NumberField } from '../primitives/number';
import { StringField } from '../primitives/string';


export type TField<T> =
   T extends string ? StringField :
   T extends number ? NumberField :
   T extends boolean ? BooleanField :
   T extends bigint ? BigintField :
   T extends object ? T extends Date ? ADateField<TDatePrimitive> :
   T extends Record<string, T[TStringKeys<T>]> ? TMapField<T[TStringKeys<T>]> :
   T extends unknown[] ? TArrayField<T[number]> :
   T extends Function ? FunctionField<T> : ObjectField<T> : UnknownField;


export type TFields<T extends object> = {
   [K in keyof T]: K extends symbol ? TSymbolIniter<T, T[K]> : TField<T[K]>;
};

export interface IStringFields<T extends object, K extends TStringKeys<T>>
   extends ReadonlyMap<K, TField<T[K]>> {
   get<K extends TStringKeys<T>>(key: K): TField<T[K]>;
}

export interface ISymbolFields<T extends object, K extends TSymbolKeys<T>>
   extends ReadonlyMap<K, TSymbolIniter<T, T[K]>> {
   get<K extends TSymbolKeys<T>>(key: K): TSymbolIniter<T, T[K]>;
}

export interface IIterableFields<T extends object, K extends TStringKeys<T>>
   extends ReadonlyMap<K, TField<T[K]>> {
   get<K extends TStringKeys<T>>(key: K): TField<T[K]>;
}
