import { EType } from '@taedr/utils';
import { TValidatorEntry } from '../../circle';
import { APrimitiveField, IPrimitiveFieldCtx } from '../core/primitive';

export class BooleanField extends APrimitiveField<boolean> {
   constructor(ctx: IBooleanFieldCtx = {}) {
      super(ctx);
   }
   // -----------------------------------------------
   //                   Api
   // -----------------------------------------------

   // -----------------------------------------------
   //                   Static
   // -----------------------------------------------
   static readonly type: EType.boolean = EType.boolean;
}
// ===============================================
//                   Ctx
// ===============================================
export type TBooleanFieldValidatorMode = `TRUTHY` | `FALSY`;

export interface IBooleanFieldCtx extends IPrimitiveFieldCtx<boolean> {
}
// ===============================================
//                   Validation
// ===============================================
export function truthy(formatter?: string): TValidatorEntry<boolean> {
   return [truthy.name, value => {
      if (value === true) return;
      return formatter ?? `Should be truthy`;
   }];
}

export function falsy(formatter?: string): TValidatorEntry<boolean> {
   return [falsy.name, value => {
      if (value === false) return;
      return formatter ?? `Should be falsy`;
   }];
}
