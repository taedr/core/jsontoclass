import { EType } from '@taedr/utils';
import { TValidator, TValidatorEntry } from '../../circle';
import { APrimitiveField, IPrimitiveFieldCtx } from '../core/primitive';

export class StringField extends APrimitiveField<string> {
   constructor(ctx: IStringFieldCtx = {}) {
      super(ctx);
   }
   // -----------------------------------------------
   //                   Api
   // -----------------------------------------------

   // -----------------------------------------------
   //                   Static
   // -----------------------------------------------
   static readonly type: EType.string = EType.string;
}
// ===============================================
//                   Ctx
// ===============================================
export interface IStringFieldCtx extends IPrimitiveFieldCtx<string> {
}
// ===============================================
//                   Validation
// ===============================================
export function minLength(
   min: number,
   formatter?: (value: string, min: number) => string
): TValidatorEntry<string> {
   return [minLength.name, value => {
      const size = value.length;
      if (size >= min) return;
      return formatter ? formatter(value, min) : `Length(${size}) is less than expected(${min})`;
   }];
}

export function maxLength(
   max: number,
   formatter?: (value: string, min: number) => string
): TValidatorEntry<string> {
   return [maxLength.name, value => {
      const size = value.length;
      if (size <= max) return;
      return formatter ? formatter(value, max) : `Length(${size}) is more than expected(${max})`;
   }];
}

export function enumerable(
   id: string,
   _enum: object,
   formatter?: (value: string, _enum: object) => string
): TValidatorEntry<string> {
   return [id, value => {
      if (value in _enum) return;
      return formatter ? formatter(value, _enum) : `Not present in enum: ${Object.keys(_enum).join(', ')}`;
   }];
}

export function pattern(
   id: string,
   _pattern: RegExp,
   formatter?: (value: string, _pattern: RegExp) => string
): TValidatorEntry<string> {
   return [id, value => {
      if (_pattern.test(value)) return;
      return formatter ? formatter(value, _pattern) : `Doesn't match pattern`;
   }];
}
