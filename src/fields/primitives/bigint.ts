import { EType } from '@taedr/utils';
import { APrimitiveField, IPrimitiveFieldCtx } from '../core/primitive';

export class BigintField extends APrimitiveField<bigint> {
   constructor(ctx: IBigintFieldCtx = {}) {
      super(ctx);
   }
   // -----------------------------------------------
   //                   Api
   // -----------------------------------------------

   // -----------------------------------------------
   //                   Static
   // -----------------------------------------------
   static readonly type: EType.bigint = EType.bigint;
}
// ===============================================
//                   Ctx
// ===============================================
export interface IBigintFieldCtx extends IPrimitiveFieldCtx<bigint> {
}
