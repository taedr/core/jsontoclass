import { EType, TFunction, TMapper } from '@taedr/utils';
import { TValidator, TValidatorEntry } from '../../circle';
import { APrimitiveField, IPrimitiveFieldCtx } from '../core/primitive';

export class NumberField extends APrimitiveField<number> {
   constructor(ctx: INumberFieldCtx = {}) {
      super(ctx);
   }
   // -----------------------------------------------
   //                   Api
   // -----------------------------------------------

   // -----------------------------------------------
   //                   Static
   // -----------------------------------------------
   static readonly type: EType.number = EType.number;
}
// ===============================================
//                   Ctx
// ===============================================
export interface INumberFieldCtx extends IPrimitiveFieldCtx<number> {
}
// ===============================================
//                   Validation
// ===============================================
export function minNumber(
   min: number | bigint,
   formatter?: (value: number | bigint, min: number | bigint) => string
): TValidatorEntry<number | bigint> {
   return [minNumber.name, value => {
      if (value >= min) return;
      return formatter ? formatter(value, min) : `Smaller than expected(${min})`;
   }];
}

export function maxNumber(
   max: number | bigint,
   formatter?: (value: number | bigint, max: number | bigint) => string
): TValidatorEntry<number | bigint> {
   return [maxNumber.name, value => {
      if (value <= max) return;
      return formatter ? formatter(value, max) : `Bigger than expected(${max})`;
   }];
}
