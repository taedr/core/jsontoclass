import { EType, TStringKeys } from '@taedr/utils';
import { ArrayMeta, ObjectMeta, TArrayMeta, IFieldCtx, TEntryCtx, IMapFieldCtx, MapField } from '../../circle';
import { IUnknownFieldCtx, UnknownField } from '../other/unknown';
import { BigintField, IBigintFieldCtx } from '../primitives/bigint';
import { BooleanField, IBooleanFieldCtx } from '../primitives/boolean';
import { INumberFieldCtx, NumberField } from '../primitives/number';
import { IStringFieldCtx, StringField } from '../primitives/string';
import { AIterableField, IIterableFieldCtx } from './iterable';
import { IObjectFieldCtx, ObjectField } from './object';

export class ArrayField<T> extends AIterableField<T, T[]> {
   readonly meta: ArrayMeta<T>;

   constructor(ctx: IArrayFieldCtx<T>) {
      super(ctx);
      this.meta = ctx.meta;
   }
   // -----------------------------------------------
   //                   Api
   // -----------------------------------------------

   // -----------------------------------------------
   //                   Internal
   // -----------------------------------------------
   protected getSize(value: T[]) {
      return value.length;
   }
   // -----------------------------------------------
   //                   Static
   // -----------------------------------------------
   static readonly type: EType.array = EType.array;
}
// ===============================================
//                   Ctx
// ===============================================
export interface IArrayFieldCtx<T> extends IIterableFieldCtx<T, T[]> {
   meta: ArrayMeta<T>;
}


export interface ITypedArrayFieldCtx<T, E extends IFieldCtx<T>>
   extends Omit<IArrayFieldCtx<T>, 'meta' | 'isCalculated'> {
   entry?: TEntryCtx<T, E>;
}

// ===============================================
//                   Primitives
// ===============================================
export class NumberArrayField extends ArrayField<number> {
   constructor(ctx: ITypedArrayFieldCtx<number, INumberFieldCtx> = {}) {
      super({
         ...ctx,
         meta: new ArrayMeta([new NumberField(ctx.entry)])
      });
   }
}

export class StringArrayField extends ArrayField<string> {
   constructor(ctx: ITypedArrayFieldCtx<string, IStringFieldCtx> = {}) {
      super({
         ...ctx,
         meta: new ArrayMeta([new StringField(ctx.entry)])
      });
   }
}

export class BooleanArrayField extends ArrayField<boolean> {
   constructor(ctx: ITypedArrayFieldCtx<boolean, IBooleanFieldCtx> = {}) {
      super({
         ...ctx,
         meta: new ArrayMeta([new BooleanField(ctx.entry)])
      });
   }
}

export class BigintArrayField extends ArrayField<bigint> {
   constructor(ctx: ITypedArrayFieldCtx<bigint, IBigintFieldCtx> = {}) {
      super({
         ...ctx,
         meta: new ArrayMeta([new BigintField(ctx.entry)])
      });
   }
}

export class UnknownArrayField extends ArrayField<unknown> {
   constructor(ctx: ITypedArrayFieldCtx<unknown, IUnknownFieldCtx> = {}) {
      super({
         ...ctx,
         meta: new ArrayMeta([new UnknownField(ctx.entry)])
      });
   }
}
// ===============================================
//                   Objects
// ===============================================
export interface IObjectArrayFieldCtx<T extends object> extends Omit<IArrayFieldCtx<T>, 'meta'> {
   meta: ObjectMeta<T>;
   entry?: Omit<IObjectFieldCtx<T>, 'meta' | 'isCalculated'>;
}


export class ObjectArrayField<T extends object> extends ArrayField<T> {
   constructor(ctx: IObjectArrayFieldCtx<T>) {
      const _ctx: IObjectFieldCtx<T> = { meta: null as any, ...ctx.entry ?? {} };
      const descriptor = Object.getOwnPropertyDescriptor(ctx, `meta`) as PropertyDescriptor
      Object.defineProperty(_ctx, `meta`, descriptor);
      Object.defineProperty(ctx, `meta`, { value: null });

      super({
         ...ctx,
         meta: new ArrayMeta([
            new ObjectField(_ctx)
         ])
      });
   }
}
// ===============================================
//                   Map
// ===============================================
export interface IMapArrayFieldCtx<T> extends ITypedArrayFieldCtx<Record<string, T>, IMapFieldCtx<T>> {
   entry: TEntryCtx<Record<string, T>, IMapFieldCtx<T>>;
}


export class MapArrayField<T> extends ArrayField<Record<string, T>> {
   constructor(ctx: IMapArrayFieldCtx<T>) {
      super({
         ...ctx,
         meta: new ArrayMeta([new MapField(ctx.entry)])
      });
   }
}
// ===============================================
//                   Dimensional
// ===============================================
export interface IDimensionalArrayFieldCtx<T> extends Omit<IArrayFieldCtx<T[]>, 'meta'> {
   meta: TArrayMeta<T>;
   entry?: Omit<IArrayFieldCtx<T>, 'meta' | 'isCalculated'>;
}


export class DimensionalArrayField<T> extends ArrayField<T[]>{
   constructor(ctx: IDimensionalArrayFieldCtx<T>) {
      super({
         ...ctx,
         meta: new ArrayMeta([
            new ArrayField({
               meta: ctx.meta as any,
               ...ctx.entry
            })
         ])
      });
   }
}
// ===============================================
//                   Type
// ===============================================
export type TArrayField<T> =
   T extends string ? StringArrayField :
   T extends number ? NumberArrayField :
   T extends boolean ? BooleanArrayField :
   T extends bigint ? BigintArrayField :
   T extends object ? T extends unknown[] ? DimensionalArrayField<T[number]> :
   T extends Record<string, TStringKeys<T>> ? MapArrayField<T[TStringKeys<T>]> :
   ObjectArrayField<T> : ArrayField<T>;
