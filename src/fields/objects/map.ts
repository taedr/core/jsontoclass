import { EType, TStringKeys } from '@taedr/utils';
import { MapMeta, ObjectMeta, TMapMeta, ArrayField, IArrayFieldCtx } from '../../circle';
import { TEntryCtx, IFieldCtx } from '../core/field';
import { IUnknownFieldCtx, UnknownField } from '../other/unknown';
import { BigintField, IBigintFieldCtx } from '../primitives/bigint';
import { BooleanField, IBooleanFieldCtx } from '../primitives/boolean';
import { INumberFieldCtx, NumberField } from '../primitives/number';
import { IStringFieldCtx, StringField } from '../primitives/string';
import { AIterableField, IIterableFieldCtx } from './iterable';
import { IObjectFieldCtx, ObjectField } from './object';

export class MapField<T> extends AIterableField<T, Record<string, T>> {
   readonly meta: MapMeta<T>;

   constructor(ctx: IMapFieldCtx<T>) {
      super(ctx);
      this.meta = ctx.meta;
   }
   // -----------------------------------------------
   //                   Api
   // -----------------------------------------------

   // -----------------------------------------------
   //                   Internal
   // -----------------------------------------------
   protected getSize(value: Record<string, T>) {
      return Object.keys(value).length;
   }
   // -----------------------------------------------
   //                   Static
   // -----------------------------------------------
   static readonly type: EType.map = EType.map;
}
// ===============================================
//                   Ctx
// ===============================================
export interface IMapFieldCtx<T> extends IIterableFieldCtx<T, Record<string, T>> {
   meta: MapMeta<T>;
}


export interface ITypedMapFieldCtx<T, E extends IFieldCtx<T>>
   extends Omit<IMapFieldCtx<T>, 'meta'> {
   entry?: TEntryCtx<T, E>;
}

// ===============================================
//                   Primitives
// ===============================================
export class NumberMapField extends MapField<number> {
   constructor(ctx: ITypedMapFieldCtx<number, INumberFieldCtx> = {}) {
      super({
         ...ctx,
         meta: new MapMeta([new NumberField(ctx.entry)])
      });
   }
}

export class StringMapField extends MapField<string> {
   constructor(ctx: ITypedMapFieldCtx<string, IStringFieldCtx> = {}) {
      super({
         ...ctx,
         meta: new MapMeta([new StringField(ctx.entry)])
      });
   }
}

export class BooleanMapField extends MapField<boolean> {
   constructor(ctx: ITypedMapFieldCtx<boolean, IBooleanFieldCtx> = {}) {
      super({
         ...ctx,
         meta: new MapMeta([new BooleanField(ctx.entry)])
      });
   }
}

export class BigintMapField extends MapField<bigint> {
   constructor(ctx: ITypedMapFieldCtx<bigint, IBigintFieldCtx> = {}) {
      super({
         ...ctx,
         meta: new MapMeta([new BigintField(ctx.entry)])
      });
   }
}

export class UnknownMapField extends MapField<unknown> {
   constructor(ctx: ITypedMapFieldCtx<unknown, IUnknownFieldCtx> = {}) {
      super({
         ...ctx,
         meta: new MapMeta([new UnknownField(ctx.entry)])
      });
   }
}
// ===============================================
//                   Objects
// ===============================================
export interface IObjectMapFieldCtx<T extends object> extends Omit<IMapFieldCtx<T>, 'meta'> {
   meta: ObjectMeta<T>;
   entry?: Omit<IObjectFieldCtx<T>, 'meta' | 'isCalculated'>;
}


export class ObjectMapField<T extends object> extends MapField<T> {
   constructor(ctx: IObjectMapFieldCtx<T>) {
      const _ctx: IObjectFieldCtx<T> = { meta: null as any, ...ctx.entry ?? {} };
      const descriptor = Object.getOwnPropertyDescriptor(ctx, `meta`) as PropertyDescriptor
      Object.defineProperty(_ctx, `meta`, descriptor);
      Object.defineProperty(ctx, `meta`, { value: null });

      super({
         ...ctx,
         meta: new MapMeta([
            new ObjectField(_ctx)
         ])
      });
   }
}
// ===============================================
//                   Array
// ===============================================
export interface IArrayMapFieldCtx<T> extends ITypedMapFieldCtx<T[], IArrayFieldCtx<T>> {
   entry: TEntryCtx<T[], IArrayFieldCtx<T>>;
}


export class ArrayMapField<T> extends MapField<T[]> {
   constructor(ctx: IArrayMapFieldCtx<T>) {
      super({
         ...ctx,
         meta: new MapMeta([new ArrayField(ctx.entry)])
      });
   }
}
// ===============================================
//                   Dimensional
// ===============================================
export interface IDimensionalMapFieldCtx<T> extends Omit<IMapFieldCtx<Record<string, T>>, 'meta'> {
   meta: TMapMeta<T>;
   entry?: Omit<IMapFieldCtx<T>, 'meta' | 'isCalculated'>;
}


export class DimensionalMapField<T> extends MapField<Record<string, T>>{
   constructor(ctx: IDimensionalMapFieldCtx<T>) {
      super({
         ...ctx,
         meta: new MapMeta([
            new MapField({
               meta: ctx.meta as any,
               ...ctx.entry
            })
         ])
      });
   }
}
// ===============================================
//                   Objects
// ===============================================
export type TMapField<T> =
   T extends string ? StringMapField :
   T extends number ? NumberMapField :
   T extends boolean ? BooleanMapField :
   T extends bigint ? BigintMapField :
   T extends object ? T extends unknown[] ? ArrayMapField<T[number]> :
   T extends Record<string, TStringKeys<T>> ? MapField<T[TStringKeys<T>]> :
   ObjectMapField<T> : MapField<T>;
