import { EType } from '@taedr/utils';
import { TValidator, TValidatorEntry } from '../../circle';
import { AField, IFieldCtx } from '../core/field';


// ===============================================
//                   Core
// ===============================================
export type TDatePrimitive = string | number;

export abstract class ADateField<T extends TDatePrimitive> extends AField<Date> {
   constructor(ctx: IDateFieldCtx<T> = {}) {
      super(ctx);
   }
   // -----------------------------------------------
   //                   Abstract
   // -----------------------------------------------
   public abstract convert(value: T): Date;
   public abstract deconvert(value: Date): T;
   // -----------------------------------------------
   //                   Api
   // -----------------------------------------------

}
// -----------------------------------------------
//                   Ctx
// -----------------------------------------------
export interface IDateFieldCtx<T extends TDatePrimitive> extends IFieldCtx<Date> {
}
// -----------------------------------------------
//                   Validation
// -----------------------------------------------
export function minDate(
   min: Date,
   formatter?: (value: Date, min: Date) => string
): TValidatorEntry<Date> {
   return [minDate.name, value => {
      if (+value > +min) return;
      return formatter ? formatter(value, min) : `Earlier than ${min.toISOString()}`;
   }];
}

export function maxDate(
   max: Date,
   formatter?: (value: Date, max: Date) => string
): TValidatorEntry<Date> {
   return [maxDate.name, value => {
      if (+value < +max) return;
      return formatter ? formatter(value, max) : `Later than ${max.toISOString()}`;
   }];
}
// ===============================================
//                   Millis
// ===============================================
export class MillisDateField extends ADateField<number> {
   public convert(millis: number) {
      return new Date(millis);
   }

   public deconvert(date: Date) {
      return +date;
   }
   // -----------------------------------------------
   //                   Static
   // -----------------------------------------------
   static readonly type: EType.number = EType.number;
}
// ===============================================
//                   Seconds
// ===============================================
export class SecondsDateField extends ADateField<number> {
   constructor(ctx: IDateFieldCtx<number> = {}) {
      super(ctx);
   }

   public convert(seconds: number) {
      return new Date(seconds * 1000);
   }

   public deconvert(date: Date) {
      return +date / 1000;
   }
   // -----------------------------------------------
   //                   Static
   // -----------------------------------------------
   static readonly type: EType.number = EType.number;
}
// ===============================================
//                   String
// ===============================================
export class StringDateField extends ADateField<string> {
   readonly deconvert: (value: Date) => string;

   constructor(ctx: IStringDateFieldCtx) {
      super(ctx);
      this.deconvert = ctx.deconvert;
   }

   public convert(value: string) {
      return new Date(value);
   }
   // -----------------------------------------------
   //                   Static
   // -----------------------------------------------
   static readonly type: EType.string = EType.string;
}
// -----------------------------------------------
//                   Ctx
// -----------------------------------------------
export interface IStringDateFieldCtx extends IDateFieldCtx<string> {
   deconvert(value: Date): string;
}
