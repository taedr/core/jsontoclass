import { IS } from '@taedr/utils';
import { AIterableMeta, IValidationError, ObjectValidationResult, TIterableValue, TValidator, TValidatorEntry } from '../../circle';
import { AField, FieldValidationResult, IFieldCtx, IFieldValidationCtx } from '../core/field';


export abstract class AIterableField<T, I extends TIterableValue<T>> extends AField<I> {
   readonly meta: AIterableMeta<T>;

   constructor(ctx: IIterableFieldCtx<T, I>) {
      super(ctx);
      this.meta = ctx.meta;
   }
   // -----------------------------------------------
   //                   Abstract
   // -----------------------------------------------
   protected abstract getSize(value: I): number;
   // -----------------------------------------------
   //                   Api
   // -----------------------------------------------
   public validate(ctx: IIterableValidationCtx<T, I>): IterableValidationResult<T, I> {
      const {
         value,
         isCheckEntries = true,
         isCheckInstance = true,
      } = ctx;
      const entries = new Map<string, IValidationError<any>[]>();
      const instance: IValidationError<any>[] = [];

      if (isCheckInstance) {
         const { errors } = super.validate(ctx);
         instance.push(...errors);
      }

      if (isCheckEntries) {
         for (const key in value) {
            const entry = value[key] as any as T;
            const field = this.meta.getMatchingEntry(entry);
            if (IS.null(field)) continue; // #Continue#
            const result = field.validate({ value: entry });

            if (
               result instanceof ObjectValidationResult ||
               result instanceof IterableValidationResult
            ) {
               for (const [_path, _errors] of result.all) {
                  if (_errors.length) entries.set(`${key}.${_path}`, _errors);
               }
            } else {
               if (result.errors.length) entries.set(key, result.errors);
            }
         }
      }

      return new IterableValidationResult(entries, instance);;
   }
}
// ===============================================
//                   Ctx
// ===============================================
export interface IIterableFieldCtx<T, I extends TIterableValue<T>> extends IFieldCtx<I> {
   meta: AIterableMeta<T>;
}
// ===============================================
//                   Validation
// ===============================================
export interface IIterableValidationCtx<T, I extends TIterableValue<T>> extends IFieldValidationCtx<I> {
   isCheckInstance?: boolean;
   isCheckEntries?: boolean;
}

export class IterableValidationResult<T, I extends TIterableValue<T>> extends FieldValidationResult<I> {
   private _all?: ReadonlyMap<string, IValidationError<any>[]>;

   public get all(): ReadonlyMap<string, IValidationError<I>[]> {
      if (!IS.null(this._all)) return this._all; // #Return#
      const all = new Map<string, IValidationError<any>[]>();
      if (this.instance.length) all.set(`INSTANCE`, this.instance);
      this.entries.forEach((erros, key) => all.set(key, erros));
      return this._all = all;
   }

   constructor(
      readonly entries: ReadonlyMap<string, IValidationError<any>[]>,
      readonly instance: IValidationError<I>[],
   ) {
      super([...entries.values()].reduce((acc, errors) => acc.concat(errors), [...instance]));
   }
}

export function minSize<T extends Iterable<unknown>>(
   min: number,
   formatter?: (value: T, min: number) => string
): TValidatorEntry<T> {
   return [minSize.name, value => {
      const size = [...value].length;
      if (size >= min) return;
      return formatter ? formatter(value, min) : `Size(${size}) is less than expected(${min})`;
   }];
}

export function maxSize<T extends Iterable<unknown>>(
   max: number,
   formatter?: (value: T, max: number) => string
): TValidatorEntry<T> {
   return [maxSize.name, value => {
      const size = [...value].length;
      if (size <= max) return;
      return formatter ? formatter(value, max) : `Size(${size}) is more than expected(${max})`;
   }];
}
