import { EType } from '@taedr/utils';
import { IObjectValidationCtx, ObjectMeta, ObjectValidationResult } from '../../circle';
import { AField, IFieldCtx } from '../core/field';

export class ObjectField<T extends object> extends AField<T> {
   readonly isPartial?: true;
   readonly meta: ObjectMeta<T>;

   public get isValidatable(): boolean {
      if (this.validators.size) return true;
      for (const [_, field] of this.meta.fields.strings) {
         if (field.validators.size) return true;
      }
      return false;
   }

   constructor(ctx: IObjectFieldCtx<T>) {
      super(ctx);
      const descriptor = Object.getOwnPropertyDescriptor(ctx, `meta`) as PropertyDescriptor
      this.meta = null as any;
      Object.defineProperty(this, `meta`, descriptor);

      if (ctx.isPartial) this.isPartial = true;
   }
   // -----------------------------------------------
   //                   Api
   // -----------------------------------------------
   public validate(ctx: IObjectValidationCtx<T>): ObjectValidationResult<T> {
      const { errors } = super.validate(ctx);
      const result = this.meta.validate(ctx);

      if (!errors.length) return result; // #Return#

      return new ObjectValidationResult(
         result.primitives,
         result.objects,
         result.iterables,
         result.functions,
         [...result.instance, ...errors]
      );
   }
   // -----------------------------------------------
   //                   Static
   // -----------------------------------------------
   static readonly type: EType.object = EType.object;
}
// ===============================================
//                   Ctx
// ===============================================
export interface IObjectFieldCtx<T extends object> extends IFieldCtx<T> {
   meta: ObjectMeta<T>;
   isPartial?: boolean;
}
