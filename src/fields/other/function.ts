import { EType } from '@taedr/utils';
import { AField, IFieldCtx } from '../core/field';

export class FunctionField<T extends Function> extends AField<T> {
   constructor(ctx: IFunctionCtx<T> = {}) {
      super(ctx);
   }
   // -----------------------------------------------
   //                   Api
   // -----------------------------------------------
   // -----------------------------------------------
   //                   Static
   // -----------------------------------------------
   static readonly type: EType.function = EType.function;
}
// ===============================================
//                   Ctx
// ===============================================
export interface IFunctionCtx<T extends Function> extends IFieldCtx<T> {
}
