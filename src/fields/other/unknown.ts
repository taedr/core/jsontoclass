import { EType } from '@taedr/utils';
import { AField, IFieldCtx } from '../core/field';

export class UnknownField extends AField<any> {
   constructor(ctx: IUnknownFieldCtx = {}) {
      super(ctx);
   }
   // -----------------------------------------------
   //                   Api
   // -----------------------------------------------

   // -----------------------------------------------
   //                   Static
   // -----------------------------------------------
   static readonly type: EType.unknown = EType.unknown;
}
// ===============================================
//                   Ctx
// ===============================================
export interface IUnknownFieldCtx extends IFieldCtx<any> {
}
