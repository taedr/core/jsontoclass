export type TSymbolIniter<I extends object, O> = (instance: I) => O;
