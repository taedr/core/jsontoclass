## [Documentation](https://taedr.gitbook.io/jsontoclass)

## Overview
JsonToClass (JTC) is a JavaScript library for data validation.
JTC main purpose is to ensure that expectable and received data are aligned fully. Library provides functionality for converting JS objects into class instances with syntax and semantic validation and gives full description in case of problems. 

## When to use?
JTC can be very helpful when working with data sources beyond control, like server API or local storage on client side and client request on server side.

## Example

### Sweet
```typescript
import { JTC, ObjectMeta, NumberField, StringField, BooleanField, ObjectArrayMeta } from '@taedr/jsontoclass';

class User {
   id: number;
   name: string;
   isAdmin: boolean;
}

const USER_META = new ObjectMeta({
   builder: User,
   fields: {
      id: new NumberField(),
      name: new StringField(),
      isAdmin: new BooleanField(),
   }
});

const users = [
   { id: 1, name: `Vasya`, isAdmin: false },
   { id: 2, name: `Petya`, isAdmin: false },
   { id: 3, name: `Masha`, isAdmin: true },
];

const result = JTC.convert({
   id: `Users`,
   meta: new ObjectArrayMeta({ meta: USER_META }),
   values: users,
});

console.log(result.converted.all);
```
### Hardcore
```typescript
import { 
   BooleanField, FunctionField, JTC, NumberField, ObjectArrayField, ObjectArrayMeta, 
   ObjectField, ObjectMeta, StringArrayField, StringField, VALIDATORS 
} from '@taedr/jsontoclass';

class Address {
   country: string;
   isConfirmed: boolean;
}

const ADDRESS_META = new ObjectMeta({
   builder: Address,
   fields: {
      country: new StringField(),
      isConfirmed: new BooleanField({
         validators: [
            VALIDATORS.truthy(),
         ]
      })
   }
});

class Location {
   title: string;
   lat: number;
   lng: number;
}

const LOCATION_META = new ObjectMeta({
   builder: Location,
   fields: {
      title: new StringField(),
      lat: new NumberField({
         validators: [
            VALIDATORS.min.number(-90),
            VALIDATORS.max.number(90),
         ]
      }),
      lng: new NumberField({
         validators: [
            VALIDATORS.min.number(-180),
            VALIDATORS.max.number(180),
         ]
      }),
   }
});

const APP = Symbol(`APP`);

class App {
   isSelected = false;
}

class User {
   name: string;
   age: number;
   email: string;
   notes: string[];
   address: Address;
   locations: Location[];
   referal: User | null;
   [APP]: App;

   get short() { return `${this.name} - ${this.age} - ${this.email}`; }

   sayHi() {
      console.log(`Hi! My name is ${this.name}.`);
   }
}

const USER_META = new ObjectMeta<User>({
   builder: User,
   fields: {
      name: new StringField({
         validators: [
            VALIDATORS.max.length(20)
         ]
      }),
      age: new NumberField({
         validators: [
            VALIDATORS.min.number(18, () => `Adults allowed ONLY`),
         ]
      }),
      email: new StringField({
         validators: [
            VALIDATORS.pattern(`email`, /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)
         ]
      }),
      notes: new StringArrayField(),
      address: new ObjectField({ meta: ADDRESS_META }),
      locations: new ObjectArrayField({ meta: LOCATION_META }),
      sayHi: new FunctionField(),
      short: new StringField({ isCalculated: true }),
      referal: new ObjectField({
         isNullable: true,
         get meta() { return USER_META; }
      }),
      [APP]: () => new App(),
   }
});

const user_1 = {
   name: `Andrii`,
   age: 26,
   email: `taedr.js@gmail.com`,
   notes: [`jsontoclass`, `humble`, `creator`],
   address: {
      country: `Ukraine`,
      isConfirmed: true,

   },
   locations: [
      {
         title: `Chornomorsk`,
         lat: 46.2952,
         lng: 30.6481,
      }
   ],
   referal: null,
};

const user_2 = {
   name: `Mashsa`,
   age: 10,
   email: `email`,
   notes: [`Some`, 12,],
   address: {
      country: `Russia`,
      isConfirmed: false,

   },
   locations: [
      {
         title: `Omsk`,
      },
      {
         lat: 256,
         lng: true
      },
      {
         lat: `78`,
         lng: -1111111
      }
   ],
   referal: user_1,
};

const values = [
   user_1,
   user_2,
];

const result = JTC.convert<User>({
   id: `Users`,
   meta: new ObjectArrayMeta({ meta: USER_META }),
   values,
   isFreeze: true,
   validators: [
      VALIDATORS.max.size(1)
   ]
});

const log = JTC.log.asString(result.corruption);

/* INSTANCE -> [...] | Size(2) is more than expected(1)
   1 -> age -> 10 | Adults allowed ONLY
   1 -> email -> email | Doesn't match pattern
   1 -> notes -> 1 -> 12 | Members can only be of "string" type, but got "number"
   1 -> address -> isConfirmed -> false | Should be truthy
   1 -> locations -> 0 -> lat -> undefined | Value is required
   1 -> locations -> 0 -> lng -> undefined | Value is required
   1 -> locations -> 1 -> lat -> 256 | Bigger than expected(90)
   1 -> locations -> 1 -> lng -> true | Expected number, but got boolean
   1 -> locations -> 1 -> title -> undefined | Value is required
   1 -> locations -> 2 -> lat -> 78 | Expected number, but got string
   1 -> locations -> 2 -> lng -> -1111111 | Smaller than expected(-180)
   1 -> locations -> 2 -> title -> undefined | Value is required
   */
console.log(log);
console.log(result.converted.all);

for (const user of result.converted.all) {
   user.sayHi();
}
```
