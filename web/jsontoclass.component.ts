import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { JTC, NumberField, ObjectArrayMeta, ObjectMeta, StringField } from '@taedr/jsontoclass';
import { example } from './example';

@Component({
   selector: 'app-jsontoclass',
   templateUrl: './jsontoclass.component.html',
   styleUrls: ['./jsontoclass.component.scss'],
   changeDetection: ChangeDetectionStrategy.OnPush
})
export class JsontoclassComponent implements OnInit {

   constructor() { }

   ngOnInit(): void {
      // example();
      test();



   }

}

function test() {
   class User {
      constructor(
         public name: string,
         public age: number,
      ) { }
   }

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         name: new StringField(),
         age: new NumberField(),
      }
   });

   const users = [
      { name: `Vasya`, age: 34 },
      { name: `Masha`, age: 21 },
   ];

   const result = JTC.convert({
      id: `Users`,
      meta: new ObjectArrayMeta({
         meta: USER_META,
      }),
      values: users,
   });

   const deconverted = result.converted.all.map(value => JTC.deconvert({ value }));
   const _deconverted = users.map(value => JTC.deconvert({ value }));

   console.log({ USER_META, result, deconverted, _deconverted })

}
