import { BooleanField, FunctionField, NumberField, ObjectField, ObjectMeta, StringField } from "@taedr/jsontoclass";
import { Address, ADDRESS_META } from "./address";

export class User {
   public get info() {
      return `${this.name} - ${this.age} - ${this.isAlive ? `alive` : `dead`}`;
   }

   constructor(
      public name: string,
      public age: number,
      public isAlive: boolean,
      public address: Address,
   ) { }

   public sayHi() {
      console.log(`Hi! My name is ${this.name}`);
   }
}

export const USER_META = new ObjectMeta({
   builder: User,
   fields: {
      name: new StringField(),
      age: new NumberField({ isNullable: true }),
      isAlive: new BooleanField(),
      sayHi: new FunctionField<() => void>(),
      info: new StringField({ isCalculated: true }),
      address: new ObjectField({ meta: ADDRESS_META }),
   }
});
