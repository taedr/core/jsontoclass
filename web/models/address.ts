import { ObjectMeta, StringField } from "@taedr/jsontoclass";

export class Address {
   constructor(
      public country: string,
      public city: string,
   ) {

   }
}

export const ADDRESS_META = new ObjectMeta({
   builder: Address,
   fields: {
      country: new StringField(),
      city: new StringField(),
   }
});
