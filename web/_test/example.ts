import { ArrayMeta, JTC, ObjectArrayMeta, ObjectField } from "@taedr/jsontoclass";
import { Note } from "./models/note";
import { Create, CREATE_META, Delete, DELETE_META, EActionType, Update, UPDATE_META } from "./models/type";
import { APP, USER_META } from "./models/user";



export function example() {
  const vasya = {
    id: `1`,
    name: `Vasya`,
    // age: 12,
    isAlive: true,
    dob: Date.now(),
    address: {
      country: `Ukraine`,
      city: `Odessa`,
      matrix: [
        [1, 2],
        [3, 4]
      ]
    },
    nicknames: [`SUPER_VASYA`],
    notes: [
      {
        text: `Some`,
        date: `2021-06-09T06:49:32.482Z`
      }
    ]
  };

  const petya = {
    id: `2`,
    name: `Petya`,
    age: 10,
    isAlive: false,
    dob: Date.now(),
    address: {
      country: `Ukraine`,
      city: `Chernomorsk`,
      matrix: [
        [1, 2],
        [3, 4]
      ]
    },
    nicknames: [`PetRoVicHHH11`],
    notes: [],
    referal: vasya,
    aaa: ``,
    reg: 1,
  }

  const values = [
    vasya,
    petya,
    // vasya
  ];


  const errors= USER_META.fields.strings.get(`age`).validate({ value: 5 })

  const result = JTC.convert({
    id: `Users`,
    meta: new ObjectArrayMeta({ meta: USER_META,  }),
    values,
    // isFreeze: true,

  });

  const [user] = result.converted.all;

  result.origin.duplicates


  console.log(result);
  // user.name = `AAA`;
  // user.notes.push(new Note(``, new Date()))

  user[APP].isSelected = true;

//   const result_user = JTC.convert({
//     id: `Users`,
//     meta: new ObjectArrayMeta({ meta: USER_META }),
//     values: [user],
//     SSymbols: `SHARE`,
//   });

//   const [user_share] = result_user.converted.all;

  // console.log({
  //   user,
  //   user_share,
  //   isSameRef: user_share === user,
  //   isSameSymbol: user_share[APP] === user[APP]
  // })

  // console.log(result.converted.all);
  // console.log(result);
  // result.converted.all[0].sayHi();
  // result.converted.all[0].address.sayFrom();

  // console.log(user[APP]);



  const result_1 = JTC.convert({
    id: `Users`,
    meta: new ArrayMeta([
      new ObjectField({ meta: CREATE_META }),
      new ObjectField({ meta: UPDATE_META }),
      new ObjectField({ meta: DELETE_META }),
    ] as [
        ObjectField<Create | Update | Delete>,
        ObjectField<Create | Update | Delete>,
        ObjectField<Create | Update | Delete>,
      ]),
    values: [
      { type: EActionType.create },
      { type: EActionType.delete },
      { type: EActionType.create },
      { type: EActionType.update },
      { type: EActionType.delete },
      { type: EActionType.update },
    ],
  });

  // console.log(result_1.converted.all)
}
