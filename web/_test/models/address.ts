import { DimensionalArrayField, FunctionField, NumberArrayMeta, ObjectMeta, StringField } from "@taedr/jsontoclass";

export class Address {
  constructor(
    public country: string,
    public city: string,
    public matrix: number[][],
  ) {}

  public sayFromA() {
    console.log(`Hi! I'm from ${this.country} - ${this.city}`);
  }
}

export const ADDRESS_META = new ObjectMeta({
  builder: Address,
  fields: {
    country: new StringField(),
    city: new StringField(),
    sayFromA: new FunctionField<() => void>(),
    matrix: new DimensionalArrayField({
      meta: new NumberArrayMeta(),
    })
  }
});
