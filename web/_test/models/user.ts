import { BooleanField, FunctionField, MillisDateField, NumberField, ObjectArrayField, ObjectArrayMeta, ObjectField, ObjectMeta, StringArrayField, StringDateField, StringField } from "@taedr/jsontoclass";
import { Address, ADDRESS_META } from "./address";
import { Note, NOTE_META } from "./note";

export const APP = Symbol(`APP`);

export class App {
  public isSelected = false;

  constructor(
    public user: User,
  ) { }
}

export class User {
  public get short() {
    const mess = this.isAlive ? `alive` : `dead`;
    return `${this.name} is ${mess}!`;
  }

  constructor(
    public id: string,
    public name: string,
    public isAlive: boolean,
    public dob: Date,
    public address: Address,
    public nicknames: string[],
    public notes: Note[],
    public referal: User,
    public age?: number,
  ) { }

  public sayHi() {
    console.log(`Hi! My name is ${this.name}`);
  }

  readonly [APP]: App;
}

export const USER_META = new ObjectMeta<User>({
  builder: User,
  fields: {
    id: new StringField(),
    name: new StringField({
      maxLength: 3,
      minLength: 2,
    }),
    age: new NumberField({
      isNullable: true,
      min: 18,
      max: 120,
    }),
    isAlive: new BooleanField({
      mode: `FALSY`
    }),
    dob: new MillisDateField(),
    address: new ObjectField({ meta: ADDRESS_META }),
    nicknames: new StringArrayField(),
    notes: new ObjectArrayField({ meta: NOTE_META }),
    referal: new ObjectField({
      get meta(): ObjectMeta<User> { return USER_META; },
      isNullable: true,
    }),
    short: new StringField({ isCalculated: true }),
    sayHi: new FunctionField(),
    [APP]: user => new App(user)
  },
  getHash: ({ id }) => id,
});
