import { FunctionField, MillisDateField, ObjectMeta, StringDateField, StringField } from "@taedr/jsontoclass";

export class Note {
  constructor(
    public text: string,
    public date: Date,
  ){}

  public test() {

  }
}

export const NOTE_META = new ObjectMeta({
  builder: Note,
  fields: {
    text: new StringField(),
    date: new StringDateField({
      deconvert: date => date.toISOString(),
    }),
    test: new FunctionField<() => void>(),
  }
})
