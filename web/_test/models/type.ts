import { ObjectMeta, StringField } from "@taedr/jsontoclass";

export enum EActionType {
  create = `create`,
  update = `update`,
  delete = `delete`
}

export class Create {
  readonly type: EActionType.create = EActionType.create;
}

export const CREATE_META = new ObjectMeta({
  builder: Create,
  fields: {
    type: new StringField(),
  },
  isOfType: ({ type }) => type === EActionType.create,
});


export class Update {
  readonly type: EActionType.update = EActionType.update;
}

export const UPDATE_META = new ObjectMeta({
  builder: Update,
  fields: {
    type: new StringField(),
  },
  isOfType: ({ type }) => type === EActionType.update,
});

export class Delete {
  readonly type: EActionType.delete = EActionType.delete;
}

export const DELETE_META = new ObjectMeta({
  builder: Delete,
  fields: {
    type: new StringField(),
  },
  isOfType: ({ type }) => type === EActionType.delete,
});
