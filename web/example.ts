import { JTC, ObjectArrayMeta } from "@taedr/jsontoclass";
import { Address } from "./models/address";
import { User, USER_META } from "./models/user";

export function example() {
   const vasya = {
      name: `Vasya`,
      isAlive: true,
      address: {
         country:    1,
         city: `Odessa`,
      }
   };

   const masha = {
      name: 'Masha',
      age: null,
      isAlive: false,
      address: {
         country: `Ukraine`,
      },
   };

   const values = [
      vasya,
      masha
   ];

   const result = JTC.convert({
      id: `User`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values,
   });

   const [_vasya, _masha] = result.converted.all;

   console.log(_vasya, _masha);
}
