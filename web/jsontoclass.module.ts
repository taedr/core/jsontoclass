import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JsontoclassComponent } from './jsontoclass.component';
import { JsontoclassRoutingModule } from './jsontoclass.routing';


@NgModule({
   imports: [
      CommonModule,
      JsontoclassRoutingModule,
   ],
   declarations: [
      JsontoclassComponent,
   ],
})
export class JsontoclassModule { }
 