import { ERRORS, getError, UpdaterError, JTC, ObjectMeta, NumberField, StringField, ObjectField, ObjectArrayField, ObjectArrayMeta } from '@taedr/jsontoclass';



it(`Update Error`, () => {
   class Note {
      public text = ``;
      public date = 0;
   }

   const NOTE_META = new ObjectMeta({
      builder: Note,
      fields: {
         date: new NumberField(),
         text: new StringField(),
      }
   });

   class Coords {
      public lat = 0;
      public lng = 0;
   }

   const COORDS_META = new ObjectMeta({
      builder: Coords,
      fields: {
         lat: new NumberField(),
         lng: new NumberField(),
      }
   });

   class User {
      public id = 0;
      public name = ``;
      public notes: Note[] = [];
      public coords = new Coords();
   }

   const USER_META = new ObjectMeta({
      builder: User,
      getHash: user => user.id + ``,
      fields: {
         id: new NumberField(),
         name: new StringField(),
         coords: new ObjectField({ meta: COORDS_META }),
         notes: new ObjectArrayField({ meta: NOTE_META })
      }
   });


   const json = {
      id: 1,
      name: `Vasya`,
      coords: {
         lat: 1,
         lng: 1
      },
      notes: [
         { text: `1`, date: 1 },
         { text: `2`, date: 2 },
         { text: `3`, date: 3 },
      ],
   };

   const result = JTC.convert({
      id: `Corrupted User`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values: [json],
   });
   const [value] = result.converted.all;
   const partial: any = {
      name: `Ola`,
      coords: {
         lat: `2`,
      },
      notes: [
         { text: `AZ` },
         null,
         { date: `4` },
      ]
   }



   const id = `Update Corrupted User`;

   try {
      JTC.update({
         id,
         metas: [USER_META],
         entries: [{ value, partial }],
      });
   } catch (e) {
      const error: UpdaterError = e;

      JTC.log.asString(error.valuesTree);     //?
      JTC.log.asString(error.partialsTree);   //?
   }


   expect(() => {
      JTC.update({
         id,
         metas: [USER_META],
         entries: [{ value, partial }],
      });
   }).toThrow(ERRORS.runtime.update);
});
