import { IUpdateEntry, JTC, NumberField, ObjectArrayField, ObjectField, ObjectMeta, StringField } from '@taedr/jsontoclass';



it(`Update Plain`, () => {
   class Note {
      public text = ``;
      public date = 0;
   }

   const NOTE_META = new ObjectMeta({
      builder: Note,
      fields: {
         date: new NumberField(),
         text: new StringField(),
      }
   });

   class Coords {
      public lat = 0;
      public lng = 0;
   }

   const COORDS_META = new ObjectMeta({
      builder: Coords,
      fields: {
         lat: new NumberField(),
         lng: new NumberField(),
      }
   });

   class User {
      public id = 0;
      public name = ``;
      public notes: Note[] = [];
      public coords = new Coords();
   }

   const USER_META = new ObjectMeta({
      builder: User,
      getHash: user => user.id + ``,
      fields: {
         id: new NumberField(),
         name: new StringField(),
         coords: new ObjectField({ meta: COORDS_META }),
         notes: new ObjectArrayField({ meta: NOTE_META })
      }
   });


   const value = {
      id: 1,
      name: `Vasya`,
      coords: {
         lat: 1,
         lng: 1
      },
      notes: [
         { text: `1`, date: 1 },
         { text: `2`, date: 2 },
         { text: `3`, date: 3 },
      ],
   };

   const partial = {
      name: `Ola`,
      coords: {
         lat: 2,
      },
      notes: [
         { text: `AZ` },
         null,
         { date: 4 },
      ]
   };

   const entries: IUpdateEntry<User>[] = [
      { value, partial }
   ];

   const [updated] = JTC.update({
      id: `Update Valid User`,
      metas: [USER_META],
      entries
   });

   updated  //?

   expect(updated).toBeInstanceOf(User);

   expect(updated.id).toBe(value.id);
   expect(updated.name).toBe(partial.name);
   expect(updated.coords.lat).toBe(partial.coords.lat);
   expect(updated.coords.lng).toBe(value.coords.lng);

   expect(updated.notes).not.toBe(value.notes);
   expect(updated.notes).not.toBe(partial.notes);

   expect(updated.notes[0].text).toBe(partial.notes[0].text);
   expect(updated.notes[0].date).toBe(value.notes[0].date);
   expect(updated.notes[1].text).toBe(value.notes[1].text);
   expect(updated.notes[1].date).toBe(value.notes[1].date);
   expect(updated.notes[2].text).toBe(value.notes[2].text);
   expect(updated.notes[2].date).toBe(partial.notes[2].date);
});
