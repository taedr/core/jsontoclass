import { IUpdateEntry, JTC, NumberField, ObjectArrayField, ObjectField, ObjectMeta, StringField } from '@taedr/jsontoclass';
import { FN_COPY, FN_PACK } from '@taedr/utils';



it(`Update Speed`, () => {

   class Note {
      public text = ``;
      public date = 0;
   }

   const NOTE_META = new ObjectMeta({
      builder: Note,
      fields: {
         date: new NumberField(),
         text: new StringField(),
      }
   });

   class Coords {
      public lat = 0;
      public lng = 0;
   }

   const COORDS_META = new ObjectMeta({
      builder: Coords,
      fields: {
         lat: new NumberField(),
         lng: new NumberField(),
      }
   });

   class User {
      public id = 0;
      public name = ``;
      public notes: Note[] = [];
      public coords = new Coords();
   }

   const USER_META = new ObjectMeta({
      builder: User,
      getHash: user => user.id + ``,
      fields: {
         id: new NumberField(),
         name: new StringField(),
         coords: new ObjectField({ meta: COORDS_META }),
         notes: new ObjectArrayField({ meta: NOTE_META })
      }
   });

   const _entries = FN_PACK(100).map(i => {
      return {
         value: {
            id: i,
            name: `Vasya`,
            coords: {
               lat: 1,
               lng: 1
            },
            notes: [
               { text: `1`, date: 1 },
            ],
         },
         partial: {
            id: i,
            name: 'Katya',
            coords: {
               lat: 66,
               lng: 77
            },
            notes: [
               { text: 'AAA', date: 54 }
            ],
         },
      };
   })

   const [first, second] = JTC.update({
      id: `Update Corrupted User`,
      metas: [USER_META],
      entries: _entries,
   });

   first    //?
   second   //?
});
