
import { ArrayField, ArrayMeta, BooleanField, FunctionField, JTC, MapField, MapMeta, NumberField, ObjectField, ObjectMeta, StringDateField, StringField } from '@taedr/jsontoclass';
import { EType, IMap } from '@taedr/utils';

it(`Deconvert - Plain`, () => {
   class Note {
      public text: string;
      public date: number;
   }

   const NOTE_META = new ObjectMeta({
      builder: Note,
      fields: {
         text: new StringField({ isNullable: true }),
         date: new NumberField({ isNullable: true }),
      }
   });

   class Coords {
      public lat: number;
      public lng: number;
   }

   const COORDS_META = new ObjectMeta({
      builder: Coords,
      fields: {
         lat: new NumberField({ isNullable: true }),
         lng: new NumberField({ isNullable: true }),
      }
   });

   const HANDLER = Symbol(`HANDLER`);

   class User {
      public id: number;
      public name: string;
      public isAdmin: boolean;
      public date: Date;
      public coords: Coords;
      public cheers: string[] = [];
      public notes: Note[] = [];
      public beers: Record<string, number> = {};

      public get short() { return `${this.id} - ${this.name}`; }

      public sayHi() {
         console.log(`Hi!`);
      }

      public [HANDLER]: number;
   }

   const USER_META = new ObjectMeta<User>({
      builder: User,
      fields: {
         id: new NumberField(),
         name: new StringField(),
         isAdmin: new BooleanField(),
         date: new StringDateField({ deconvert: date => date.toISOString() }),
         coords: new ObjectField({
            meta: COORDS_META,
         }),
         cheers: new ArrayField({
            meta: new ArrayMeta([new StringField()]),
            isNullable: true,
         }),
         notes: new ArrayField({
            meta: new ArrayMeta([
               new ObjectField({ meta: NOTE_META })
            ]),
         }),
         beers: new MapField({
            meta: new MapMeta([new NumberField()]),
            isNullable: true,
         }),
         short: new StringField({ isCalculated: true }),
         sayHi: new FunctionField(),
         [HANDLER]: ({ id }: User) => id,
      },
   });

   const json = {
      id: 1,
      name: `Vasya`,
      date: `2020-09-05T05:56:19.805Z`,
      isAdmin: false,
      coords: {
         lat: 1,
      },
      beers: {
         vine: 12,
      },
      notes: [
         { text: `1`, date: 1 },
         { date: 2 },
         { text: `3` },
      ],
   };

   const result = JTC.convert({
      id: `Deconvert Plain User`,
      meta: new ArrayMeta([
         new ObjectField({ meta: USER_META })
      ]),
      values: [json]
   });

   const [user] = result.converted.all;

   const deconvert = {
      instance: JTC.deconvert({ value: user }),
      plain: JTC.deconvert({ value: json }),
      null: JTC.deconvert({ value: null }),
   }

   user        //?
   deconvert   //?
   JTC.log.asString(result.corruption) //?

   USER_META //?

   expect(result.isCorrupted).toBeFalsy();

   expect(user).toBeInstanceOf(User);
   expect(deconvert.instance).toEqual(json);
   expect(deconvert.plain).toEqual(deconvert.instance);
   expect(deconvert.null).toBeNull();
});
