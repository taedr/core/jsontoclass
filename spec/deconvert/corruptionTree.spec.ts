import { ArrayMeta, JTC, NumberField, ObjectField, ObjectMeta, StringField, getObjectMeta, META, ITERABLE_CONVERSION_META, ArrayField, ObjectArrayMeta } from "@taedr/jsontoclass";


it(`Deconvert - Corruption Tree`, () => {
   class User {
      public name: string;
      public age: number;
      public child: User;
   }

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         name: new StringField(),
         age: new NumberField(),
         child: new ObjectField<User>({
            isNullable: true,
            get meta() { return USER_META; }
         }),
      }
   });

   const USERS_META = new ArrayMeta([
      new ObjectField({ meta: USER_META })
   ]);

   const json = [
      {
         name: `aaa`,
      },
      {
         name: `bbb`,
         age: 1,
         role: `one`
      },
      {
         name: `ccc`,
         child: {
            name: 23,
            age: 1,
            role: `one`
         },
      },
      {
         name: `ddd`,
         age: 2
      }
   ]


   const result = JTC.convert({
      id: `Corruption tree`,
      meta: USERS_META,
      values: json,
   });

   const treeJson = JTC.deconvert({
      value: result.corruption,
   });

   const [tree] = JTC.convert({
      id: `Tree check`,
      meta: new ObjectArrayMeta({ meta: ITERABLE_CONVERSION_META }),
      values: [treeJson],
   }).converted.all;

   const resultLog = JTC.log.asString(result.corruption);
   const deconvertLog = JTC.log.asString(tree);

   result.corruption    //?
   tree           //?
   treeJson       //?
   resultLog      //?
   deconvertLog   //?

   expect(tree).toEqual(result.corruption);
   expect(resultLog).toEqual(deconvertLog);
});
