
import { NumberArrayField, ObjectArrayField, JTC, NumberField, ObjectField, StringField, TFields, ObjectMeta, BooleanField, ArrayField, ObjectArrayMeta, MapField, NumberMapMeta, MillisDateField, StringMapField, NumberMapField, FunctionField, UnknownField } from '@taedr/jsontoclass';
import { TDeepPartial, FN_GET_ID, FN_PACK, EType } from '@taedr/utils';

class Note {
   public text: string;
   public date: Date;
}

const NOTE_META = new ObjectMeta({
   builder: Note,
   fields: {
      text: new StringField(),
      date: new MillisDateField(),
   }
});

class Coords {
   public lng: number;
   public lat: number;
   public something: unknown;
}

const COORDS_META = new ObjectMeta({
   builder: Coords,
   fields: {
      lng: new NumberField(),
      lat: new NumberField(),
      something: new UnknownField(),
   }
});

const REFERAL = Symbol(`REFERAL`);

class User {
   public id: number;
   public name: string;
   public age: number;
   public isDead: boolean;
   public coords: Coords;
   public notes: Note[];
   public marks: Record<string, number>;

   public get isAlive() { return !this.isDead; }

   readonly [REFERAL]: User;

   public sayHi(greeting: string) {
      const state = this.isDead ? `walking corps` : `radiant soul`;
      const message = `Hi! My name is ${this.name}. I'm ${state}. ${greeting}`;
      console.log(message);
      return message.length;
   }
}

function hiValidator(fn: (value: string) => number) {
   const greeting = `HOI`;
   const len = fn(greeting);
   if (len === greeting.length) return;
   return `It's awaken! (${greeting})`;
}

const USER_META = new ObjectMeta<User>({
   builder: User,
   fields: {
      id: new NumberField(),
      name: new StringField(),
      age: new NumberField(),
      isDead: new BooleanField(),
      isAlive: new BooleanField({ isCalculated: true }),
      coords: new ObjectField({ meta: COORDS_META }),
      marks: new NumberMapField(),
      notes: new ObjectArrayField({ meta: NOTE_META }),
      sayHi: new FunctionField({
         validators: [
            [hiValidator.name, hiValidator]
         ]
      }),
      [REFERAL]: user => new User()
   }
});


it(`Object`, () => {
   const json = {
      id: 1,
      name: `Vasya`,
      age: 25,
      isDead: false,
      coords: {
         lng: 90,
         lat: 180
      },
      notes: [],
      marks: {}
   };

   const result = JTC.convert({
      id: ``,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values: FN_PACK(100).map(() => ( {
         id: 1,
         name: `Vasya`,
         age: 25,
         isDead: false,
         coords: {
            lng: 90,
            lat: 180
         },
         notes: [],
         marks: {}
      }))
   })

   const [user] = result.converted.all;  //?

   result.corruption.duration //?

   JTC.log.asString(result.corruption) //?

   USER_META.validate({
      value: user
   }) //?


   USER_META //?
});
