
import { BigintField, IValidationError, JTC, MillisDateField, NumberField, ObjectArrayMeta, ObjectMeta, VALIDATORS } from '@taedr/jsontoclass';

it(`Validator - Number`, () => {
   class User {
      public id: number;
      public age: number;
      public dob: Date;
      public uuid: bigint;
   }

   const eventValidatorId = `even`;
   function evenValidator(value: number) {
      if (value % 2 === 0) return; // #Return#
      return `Not even`;
   }

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         age: new NumberField({
            validators: [
               VALIDATORS.min.number(18),
               VALIDATORS.max.number(130),
            ]
         }),
         id: new NumberField({
            validators: [
               [eventValidatorId, evenValidator]
            ]
         }),
         dob: new MillisDateField({
            validators: [
               VALIDATORS.min.date(new Date(100)),
               VALIDATORS.max.date(new Date(1000)),
            ]
         }),
         uuid: new BigintField({
            validators: [
               VALIDATORS.min.number(1000),
               VALIDATORS.max.number(1000000),
            ]
         })
      },
   });

   const users = [
      {
         id: 2,
         age: 45,
         dob: 900,
         uuid: BigInt(90000),
      },
      {
         id: 3,
         age: 12,
         dob: 2000,
         uuid: BigInt(200),
      },
      {
         id: 5,
         age: 145,
         dob: 15,
         uuid: BigInt(10000000),
      },
   ];


   const result = JTC.convert({
      id: `No unknown keys in User`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values: users,
   });

   const [first, second] = result.converted.all;
   const errors = USER_META.validate({ value: second });
   const log = JTC.log.asString(result.corruption);
   const expected = [
      `1 -> age -> 12 | Smaller than expected(18)`,
      `1 -> dob -> 1970-01-01T00:00:02.000Z | Later than 1970-01-01T00:00:01.000Z`,
      `1 -> id -> 3 | Not even`,
      `1 -> uuid -> 200 | Smaller than expected(1000)`,
      `2 -> age -> 145 | Bigger than expected(130)`,
      `2 -> dob -> 1970-01-01T00:00:00.015Z | Earlier than 1970-01-01T00:00:00.100Z`,
      `2 -> id -> 5 | Not even`,
      `2 -> uuid -> 10000000 | Bigger than expected(1000000)`,
   ].join(`\n`);

   log      //?
   errors   //?
   first    //?
   second   //?

   expect(log).toEqual(expected);
});
