
import { JTC, NumberField, ObjectArrayMeta, ObjectMeta, TValidatorEntry, VALIDATORS } from '@taedr/jsontoclass';

it(`Validator - Iterable`, () => {
   class User {
      public id: number;
   }

   const usersValidator: TValidatorEntry<User[]> = [`Sum`, (users: User[]) => {
      const sum = users.reduce((acc, user) => acc + user.id, 0);
      if (sum === 10) return; // #Return#
      return `Not valid id sum (${sum})`;
   }];

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         id: new NumberField(),
      },
   });

   const result_1 = JTC.convert<User>({
      id: `No unknown keys in User`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      validators: [
         VALIDATORS.min.size(3),
         VALIDATORS.max.size(12),
         usersValidator
      ],
      values: [
         {
            id: 1,
         },
         {
            id: 3,
         },
      ],
   });

   const result_2 = JTC.convert<User>({
      id: `No unknown keys in User`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      validators: [
         VALIDATORS.min.size(0),
         VALIDATORS.max.size(1),
         usersValidator
      ],
      values: [
         {
            id: 7,
         },
         {
            id: 3,
         },
      ],
   });

   const log_1 = JTC.log.asString(result_1.corruption);
   const log_2 = JTC.log.asString(result_2.corruption);
   const expected_1 = [
      `INSTANCE -> [...] | Size(2) is less than expected(3)`,
      `INSTANCE -> [...] | Not valid id sum (4)`,
   ].join(`\n`);
   const expected_2 = [
      `INSTANCE -> [...] | Size(2) is more than expected(1)`,
   ].join(`\n`);

   log_1 //?
   log_2 //?

   expect(log_1).toEqual(expected_1);
   expect(log_2).toEqual(expected_2);
});
