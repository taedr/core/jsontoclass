
import { JTC, ObjectArrayMeta, ObjectMeta, StringField, TValidatorEntry, VALIDATORS } from '@taedr/jsontoclass';

it(`Validator - String`, () => {
   enum ERole {
      user = 'user',
      superUser = 'superUser',
      admin = 'admin',
   }

   class User {
      public name: string;
      public role: ERole;
      public email: string;
      public title: string;

      public get short() { return `My name is ${this.name}`; }
   }

   const titleValidator: TValidatorEntry<string> = [`title`, (title: string) => {
      if (title.startsWith(`Y`) && title.endsWith(`Z`)) return; // #Return#
      return `Not fit for start and end letters`;
   }];

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         name: new StringField({
            isNullable: true,
            validators: [
               VALIDATORS.min.length(3),
               VALIDATORS.max.length(10),
            ]
         }),
         title: new StringField({
            validators: [titleValidator]
         }),
         role: new StringField({
            validators: [
               VALIDATORS.enum(`role`, ERole)
            ]
         }),
         email: new StringField({
            validators: [
               VALIDATORS.pattern(`email`, /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/)
            ]
         }),
         short: new StringField({
            isCalculated: true,
            validators: [
               VALIDATORS.max.length(10),
            ]
         })
      },
   });

   const users = [
      {
         name: `Vaasfasfasfasfasfasfs`,
         email: ``,
         role: ``,
         title: `Vasyan`
      },
      {
         name: `Masha`,
         email: `masha@gmail.com`,
         role: ERole.admin,
         title: `YAaaaZZzzZ`
      },
      {
         email: `hoi@gmail.com`,
         role: ERole.superUser,
         title: `Hoi`
      }
   ];


   const result = JTC.convert({
      id: `No unknown keys in User`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values: users,
   });

   const [first, second] = result.converted.all;
   const errors = USER_META.validate({ value: first });
   const log = JTC.log.asString(result.corruption);
   const expected = [
      `0 -> email -> '' | Doesn't match pattern`,
      `0 -> name -> Vaasfasfasfasfasfasfs | Length(21) is more than expected(10)`,
      `0 -> role -> '' | Not present in enum: user, superUser, admin`,
      `0 -> short -> My name is Vaasfasfasfasfasfasfs | Length(32) is more than expected(10)`,
      `0 -> title -> Vasyan | Not fit for start and end letters`,
      `1 -> short -> My name is Masha | Length(16) is more than expected(10)`,
      `2 -> short -> My name is undefined | Length(20) is more than expected(10)`,
      `2 -> title -> Hoi | Not fit for start and end letters`,
   ].join(`\n`);

   log      //?
   errors   //?
   first    //?
   second   //?

   expect(log).toEqual(expected);

});
