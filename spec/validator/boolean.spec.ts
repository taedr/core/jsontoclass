
import { BooleanField, JTC, ObjectArrayMeta, ObjectMeta, VALIDATORS } from '@taedr/jsontoclass';

it(`Validator - Boolean`, () => {
   class User {
      public isAdmin: boolean;
      public isAlive: boolean;
   }

   const truthy = `Voobsheto ja trebuu PRAVDI!!1`;
   const falsy = `Ti doljen SOLGAT...`;
   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         isAdmin: new BooleanField({ validators: [VALIDATORS.truthy(truthy)] }),
         isAlive: new BooleanField({ validators: [VALIDATORS.falsy(falsy)] }),
      },
   });

   const users = [
      {
         isAdmin: false,
         isAlive: false,
      },
      {
         isAdmin: true,
         isAlive: true,
      },
   ];


   const result = JTC.convert({
      id: `No unknown keys in User`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values: users,
   });

   const [first, second] = result.converted.all;
   const errors = USER_META.validate({ value: first });
   const log = JTC.log.asString(result.corruption);
   const expected = [
      `0 -> isAdmin -> false | ${truthy}`,
      `1 -> isAlive -> true | ${falsy}`,
   ].join(`\n`);

   log      //?
   errors   //?
   first    //?
   second   //?

   expect(log).toEqual(expected);

});
