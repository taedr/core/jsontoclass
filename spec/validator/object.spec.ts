
import { IValidationError, JTC, NumberField, ObjectArrayMeta, ObjectMeta, StringField } from '@taedr/jsontoclass';

it(`Validator - Object`, () => {
   class User {
      public id: number;
      public name: string;
      public age: number;
   }

   function userValidator(user: User) {
      const { id, age, name } = user;
      const diff = age - id;
      const len = name.length;
      if (diff === len) return; // #Return#
      return `(${diff} !== ${len}) Magic is not working :(`;
   }

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         id: new NumberField(),
         name: new StringField(),
         age: new NumberField()
      },
      validators: [
         [userValidator.name, userValidator]
      ]
   });

   const users = [
      {
         id: 1,
         name: `Vasya`,
         age: 45,
      },
      {
         id: 3,
         name: `Evdokia`,
         age: 10,
      },
   ];


   const result = JTC.convert({
      id: `No unknown keys in User`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values: users,
   });

   const [first, second] = result.converted.all;
   const errors = USER_META.validate({ value: first });
   const log = JTC.log.asString(result.corruption);
   const expected = [
      `0 -> INSTANCE -> {...} | (44 !== 5) Magic is not working :(`
   ].join(`\n`);

   log      //?
   errors   //?
   first    //?
   second   //?

   expect(log).toEqual(expected);

});
