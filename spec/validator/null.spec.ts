
import { JTC, ObjectArrayMeta, ObjectMeta, StringField } from '@taedr/jsontoclass';

it(`Validator - Null`, () => {
   class User {
      public id: string;
      public name: string;
   }

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         id: new StringField(),
         name: new StringField({
            isNullable: true,
         }),
      },
   });

   const users: any[] = [
      {
         id: `1`,
         name: `Petya`,
      },
      {
         id: `2`,
      },
      {
         name: `Masha`
      },
      {
         id: null,
         name: null,
      }
   ];


   const result = JTC.convert({
      id: `User`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values: users,
   });

   const results = users.map(value => USER_META.validate({ value }));
   const firstError = results[2].errors[0];
   const log = JTC.log.asString(result.corruption);
   const expected = [
      `2 -> id -> undefined | Value is required`,
      `3 -> id -> null | Value is required`
   ].join(`\n`);

   log         //?
   results     //?
   firstError  //?

   expect(log).toEqual(expected);
   expect(results[0].isValid).toBeTruthy();
   expect(results[1].isValid).toBeTruthy();
   expect(results[2].isValid).toBeFalsy();
   expect(firstError.id).toEqual(`null`);
   expect(firstError.value).toBeUndefined();
   expect(firstError.message).toEqual(`Value is required`);
});
