
import {
   FunctionField,
   NumberField, ObjectMeta, StringArrayField, StringField, VALIDATORS
} from '@taedr/jsontoclass';



it(`Test`, () => {
   const number = new NumberField({
      // isNullable: true,
      validators: [
         VALIDATORS.min.number(0),
         VALIDATORS.max.number(10),
      ]
   });

   number //?
   number.validate({ value: null }) //?
   number.validators.has(VALIDATORS.max.number.name) //?

   class User {
      public get short() { return `${this.name} - ${this.age}`; }

      constructor(
         public name: string,
         public age: number,
         public notes: string[],
      ) { }

      public sayHi() {
         console.log(`Hi! My name is ${this.name}`);
      }
   }

   const USER_META = new ObjectMeta<User>({
      builder: User,
      fields: {
         name: new StringField(),
         age: new NumberField(),
         notes: new StringArrayField(),
         short: new StringField({ isCalculated: true }),
         sayHi: new FunctionField(),
      }
   });

   // USER_META.fields.//?
});
