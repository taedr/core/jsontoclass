import { ArrayMeta, JTC, MillisDateField, ObjectArrayMeta, ObjectField, ObjectMeta, SecondsDateField, StringDateField, StringField } from '@taedr/jsontoclass';
import { MONTH_MS } from '@taedr/utils';



it(`Mix`, () => {
   class User {
      public name: string;
      public ms: Date;
      public sec: Date;
      public str: Date;
      public nullable: Date;
   }

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         name: new StringField(),
         ms: new MillisDateField(),
         sec: new SecondsDateField(),
         str: new StringDateField({
            deconvert: date => date.toUTCString(),
         }),
         nullable: new StringDateField({
            deconvert: date => date.toUTCString(),
            isNullable: true
         }),
      }
   });

   const msDate = Date.now();
   const strDate = `Mon, 06 Feb 1995 10:31:20 GMT`;
   const secDate = Math.round(Date.now() / 1000);

   const json = {
      name: 'one',
      ms: msDate,
      sec: secDate,
      str: strDate,
   }

   const result = JTC.convert({
      id: `Dates`,
      meta: new ArrayMeta([
         new ObjectField({ meta: USER_META })
      ]),
      values: [json],
   });

   const [user] = result.converted.all

   const deconverted = JTC.deconvert({
      value: user,
   });


   user //?

   expect(result.isCorrupted).toBeFalsy();
   expect(+user.ms).toEqual(msDate);
   expect(+user.sec).toEqual(secDate * 1000);
   expect(user.str.toUTCString()).toEqual(strDate);
   expect(user.nullable).toBeUndefined();
   expect(deconverted['ms']).toEqual(msDate);
   expect(deconverted['sec']).toEqual(secDate);
   expect(deconverted['str']).toEqual(strDate);
   expect(deconverted['nullable']).toBeUndefined();
});


it(`Nest`, () => {
   class GrandChild {
      public dob = new Date();
   }

   const GRAND_CHILD_META = new ObjectMeta({
      builder: GrandChild,
      fields: {
         dob: new StringDateField({
            deconvert: date => date.toUTCString(),
         }),
      }
   });

   class Child {
      public grandChild = new GrandChild();
      public dob = new Date();
   }

   const CHILD_META = new ObjectMeta({
      builder: Child,
      fields: {
         grandChild: new ObjectField({
            meta: GRAND_CHILD_META,
         }),
         dob: new SecondsDateField(),
      }
   });

   class Parent {
      public dob = new Date();
      public child = new Child();
   }

   const PARRENT_META = new ObjectMeta({
      builder: Parent,
      fields: {
         child: new ObjectField({
            meta: CHILD_META,
         }),
         dob: new MillisDateField(),
      }
   });

   const msDate = Date.now() - MONTH_MS;
   const secDate = +msDate / 1000;
   const strDate = new Date(msDate).toUTCString();

   const json = {
      dob: msDate,
      child: {
         dob: secDate,
         grandChild: {
            dob: strDate,
         }
      }
   }

   const result = JTC.convert({
      id: `Dates Nest`,
      meta: new ArrayMeta([
         new ObjectField({ meta: PARRENT_META, })
      ]),
      values: [json],
   });

   const [user] = result.converted.all;
   const deconverted = JTC.deconvert({
      value: user,
   });

   user        //?
   deconverted //?

   expect(result.isCorrupted).toBeFalsy();
   expect(+user.dob).toEqual(msDate);
   expect(+user.child.dob).toEqual(secDate * 1000);
   expect(user.child.grandChild.dob.toUTCString()).toEqual(strDate);
   expect(deconverted['dob']).toEqual(msDate);
   expect(deconverted['child']['dob']).toEqual(secDate);
   expect(deconverted['child']['grandChild']['dob']).toEqual(strDate);
});

it(`Instance`, () => {
   class User {
      public name: string;
      public date: Date;
   }

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         name: new StringField(),
         date: new MillisDateField(),
      }
   });

   const json = {
      name: 'Vasya',
      date: Date.now()
   }

   const result = JTC.convert({
      id: `Dates`,
      meta: new ArrayMeta([
         new ObjectField({ meta: USER_META })
      ]),
      values: [json],
   });

   const [user] = result.converted.all;

   const result_1 = JTC.convert({
      id: `Dates Instance`,
      meta: new ArrayMeta([
         new ObjectField({ meta: USER_META })
      ]),
      values: [user],
   });

   const [instance] = result_1.converted.all;

   user     //?
   instance //?

   expect(result.isCorrupted).toBeFalsy();
   expect(result_1.isCorrupted).toBeFalsy();
   expect(instance.date).toEqual(user.date);
   expect(instance.date).not.toBe(user.date);
});
