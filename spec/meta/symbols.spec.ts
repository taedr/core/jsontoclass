
import { JTC, NumberField, ObjectArrayMeta, ObjectMeta, StringField, TFields } from '@taedr/jsontoclass';
import { EType } from '@taedr/utils';

class Handler {
   public isMarked?: true;

   constructor(
      public id: number,
   ) { }


}

const HANDLER = Symbol(`HANDLER`);

class User {
   public id: number;
   public name: string;
}

const USER_FIELDS: TFields<User> = {
   id: new NumberField(),
   name: new StringField()
};

const USER_META = new ObjectMeta({
   builder: User,
   fields: USER_FIELDS
});

class InitableUser extends User {
   public [HANDLER]: Handler;
}

const INITABLE_USER_META = new ObjectMeta<InitableUser>({
   builder: InitableUser,
   fields: {
      ...USER_FIELDS,
      [HANDLER]: user => new Handler(user.id),
   },
});

const json = [
   { id: 1, name: `Vasya` },
   { id: 2, name: `Masha` },
   { id: 3, name: `Petya` },
   { id: 4, name: `Vitala` },
   { id: 5, name: `Fedor` },
   { id: 6, name: `Maksim` },
   { id: 7, name: `Kiril` },
];

it(`Plain`, () => {
   const result = JTC.convert({
      id: `Plain User Init`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values: json
   });
   const users = result.converted.all;

   result   //?
   users     //?

   expect(result.isCorrupted).toBeFalsy();

   for (let i = 0; i < users.length; i++) {
      const user = users[i];
      expect(user).toBeInstanceOf(User);
      expect(user.id).toBe(json[i].id);
      expect(user.name).toBe(json[i].name);
      expect(user[HANDLER]).toBeUndefined();
   }
});

it(`Modified`, () => {
   const result = JTC.convert({
      id: `Modified User Init`,
      meta: new ObjectArrayMeta({ meta: INITABLE_USER_META }),
      values: json,
   });
   const users = result.converted.all;

   result   //?
   users     //?

   expect(result.isCorrupted).toBeFalsy();

   for (let i = 0; i < users.length; i++) {
      const user = users[i];
      expect(user).toBeInstanceOf(User);
      expect(user.id).toBe(json[i].id);
      expect(user.name).toBe(json[i].name);
      expect(user[HANDLER]).toBeInstanceOf(Handler);
      expect(user[HANDLER].id).toBe(json[i].id);
   }
});

it(`Update`, () => {
   const result_1 = JTC.convert({
      id: `Update User Init`,
      meta: new ObjectArrayMeta({ meta: INITABLE_USER_META }),
      values: json,
   });
   const users = result_1.converted.all;

   result_1   //?
   users     //?

   expect(result_1.isCorrupted).toBeFalsy();

   for (const user of users) {
      user[HANDLER].isMarked = true;
   }

   const result_2 = JTC.convert({
      id: `Update User Init`,
      meta: new ObjectArrayMeta({ meta: INITABLE_USER_META }),
      values: json,
   });

   const updated = JTC.update({
      id: `Update`,
      metas: [INITABLE_USER_META],
      entries: users.map((user, i) => ({
         value: result_2.converted.all[i],
         partial: user
      }))
   });

   users    //?
   updated  //?

   for (let i = 0; i < updated.length; i++) {
      const user = updated[i];
      expect(user).toBeInstanceOf(User);
      expect(user.id).toBe(json[i].id);
      expect(user.name).toBe(json[i].name);
      expect(user[HANDLER]).toBeInstanceOf(Handler);
      expect(user[HANDLER].isMarked).toBeTruthy();
   }
});

it(`SHARE`, () => {
   const result_1 = JTC.convert({
      id: `Update User Init`,
      meta: new ObjectArrayMeta({ meta: INITABLE_USER_META }),
      values: json,
   });

   const users_1 = result_1.converted.all;

   const result_2 = JTC.convert({
      id: `Update User Init`,
      meta: new ObjectArrayMeta({ meta: INITABLE_USER_META }),
      values: users_1,
      SSymbols: `SHARE`
   });

   const users_2 = result_2.converted.all;

   expect(result_1.isCorrupted).toBeFalsy();
   expect(result_2.isCorrupted).toBeFalsy();

   for (let i = 0; i < json.length; i++) {
      const user_1 = users_1[i];
      const user_2 = users_2[i];
      expect(user_1[HANDLER]).toBe(user_2[HANDLER]);
   }
});
