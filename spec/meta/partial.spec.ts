
import { JTC, NumberField, ObjectArrayField, ObjectArrayMeta, ObjectField, ObjectMeta, StringField } from '@taedr/jsontoclass';

class User {
   public id: number;
   public name: string;
   public age: number;
}

const USER_META = new ObjectMeta({
   builder: User,
   fields: {
      id: new NumberField(),
      name: new StringField(),
      age: new NumberField(),
   }
});

class Update {
   public data: User;
}

const UPDATE_META = new ObjectMeta({
   builder: Update,
   fields: {
      data: new ObjectField({
         meta: USER_META,
         isPartial: true,
      })
   }
});

const values = [
   {
      data: {
         id: 1
      },
   },
   {
      data: {
         name: `Masha`
      },
   },
   {
      data: {
         age: 23
      },
   },
   {
      data: {
         id: 3,
         name: `Vitala`
      },
   },
   {
      data: {
         id: 2,
         age: 45
      },
   },
   {
      data: {
         id: 1,
         name: `Kiril`,
         age: 76
      },
   },
   {
      data: {
         name: `Maksim`,
         age: 33
      },
   },
];

const expected = [
   [`name`, `age`],
   [`id`, `age`],
   [`id, name`],
   [`age`],
   [`name`],
   [`id`],
   [],
];

it(`Object`, () => {
   const result = JTC.convert({
      id: `Patial User Data`,
      meta: new ObjectArrayMeta({ meta: UPDATE_META }),
      values,
   });

   const updates = result.converted.all;

   UPDATE_META.fields //?

   updates     //?

   JTC.log.asString(result.corruption) //?

   expect(result.isCorrupted).toBeFalsy();

   for (let i = 0; i < updates.length; i++) {
      const update = updates[i];

      for (const key of expected[i]) {
         expect(update).not.toHaveProperty(key);
      }

      expect(update).toBeInstanceOf(Update);
      expect(update).toBeInstanceOf(Update);
      expect(update).toEqual(values[i]);
   }
});


it(`Array`, () => {
   class Responce {
      public values: Update[] = [];
   }


   const RESPONCE_META = new ObjectMeta({
      builder: Responce,
      fields: {
         values: new ObjectArrayField({ meta: UPDATE_META })
      }
   });

   const result = JTC.convert({
      id: `Patial User Data`,
      meta: new ObjectArrayMeta({ meta: RESPONCE_META }),
      values: [{ values }],
   });

   const [updates] = result.converted.all;

   updates     //?

   JTC.log.asString(result.corruption) //?

   expect(result.isCorrupted).toBeFalsy();

   for (let i = 0; i < updates.values.length; i++) {
      const update = updates.values[i];

      for (const key of expected[i]) {
         expect(update).not.toHaveProperty(key);
      }

      expect(update).toBeInstanceOf(Update);
      expect(update).toBeInstanceOf(Update);
      expect(update).toEqual(values[i]);
   }
});
