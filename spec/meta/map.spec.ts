
import { JTC, NumberField, ObjectArrayMeta, ObjectMapField, ObjectMeta, StringField, StringMapField } from '@taedr/jsontoclass';
import { IMap } from '@taedr/utils';

class Note {
   public text: string;
   public date: number;
}

const NOTE_META = new ObjectMeta({
   builder: Note,
   fields: {
      text: new StringField(),
      date: new NumberField(),
   }
});

class User {
   public id = 0;
   public notes: Record<string, Note> = {};
   public texts: Record<string, string> = {};
}

const USER_META = new ObjectMeta({
   builder: User,
   fields: {
      id: new NumberField(),
      notes: new ObjectMapField({ meta: NOTE_META }),
      texts: new StringMapField()
   }
});

const users = [
   {
      id: 1,
      notes: {
         a: { text: `acd`, date: 12 },
         b: { text: `ret`, date: 14 },
      },
      texts: {
         one: `Axa`,
         two: `Rela`,
      }
   },
   {
      id: 2,
      notes: {
         a: { text: `acd`, date: 12, xero: 12 },
         b: { text: `ret`, date: 14 },
         c: null,
         d: { ner: 1, }
      },
      texts: {
         one: `Axa`,
         two: `Rela`,
      }
   },
];

it(`Convert`, () => {
   const result = JTC.convert({
      id: `No unknown keys in User`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values: users,
   });

   result.converted.all[0] //?
   result.converted.all[1] //?


   JTC.log.asString(result.corruption) //?

   const [first] = result.converted.all;

   for (const key in users[0].notes) {
      const _note = users[0].notes[key];
      const note = first.notes[key];
      expect(_note).toEqual(note);
   }
});


it(`Deconvert`, () => {
   const result = JTC.convert({
      id: `Deconvert`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values: users,
   });

   result.converted.all //?

   const deconvert_instance = JTC.deconvert({
      value: result.converted.all[0],
   });


   const deconvert_plain = JTC.deconvert({
      value: users[0] as any,
   });

   deconvert_instance   //?
   deconvert_plain      //?

   expect(deconvert_instance).toEqual(users[0]);
   expect(deconvert_plain).toEqual(users[0]);
});



it(`Copy`, () => {
   const result = JTC.convert({
      id: `Origin`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values: users,
   });

   result.converted.all //?

   const copy = JTC.convert({
      id: `Copy`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values: result.converted.all,
   });

   copy.converted.all[0] //?

   expect(result.converted.all).toEqual(copy.converted.all);
});
