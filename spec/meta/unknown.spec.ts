import { JTC, UnknownMapField, UnknownArrayField, NumberField, ObjectArrayMeta, ObjectMeta, UnknownField } from '@taedr/jsontoclass';


it(`Object`, async () => {
   class User {
      public id = 0;
      public garbage: unknown;
   }

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         id: new NumberField(),
         garbage: new UnknownField(),
      }
   });

   const values = [
      {
         id: 1,
         garbage: [null]
      },
      {
         id: 2,
         garbage: true
      },
      {
         id: 3,
         garbage: 111
      },
      {
         id: 4,
         garbage: `aaa`
      }
   ];


   const result = JTC.convert({
      id: `Object`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values
   });

   result.converted.all //?

   JTC.log.asString(result.corruption) //?

   expect(result.isCorrupted).toBeFalsy();
   expect(result.converted.all).toEqual(values);
});

it(`Iterables`, async () => {
   class User {
      public id = 0;
      public junk: Record<string, unknown> = {};
      public garbage: unknown[] = [];
   }

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         id: new NumberField(),
         junk: new UnknownMapField(),
         garbage: new UnknownArrayField(),
      }
   });

   const values = [{
      id: 1,
      junk: {
         1: null,
         2: ``,
         3: true,
         4: 0,
         5: {},
      },
      garbage: [null, ``, true, 0, {}]
   }];


   const result = JTC.convert({
      id: `Iterables`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values
   });


   result.converted.all //?

   JTC.log.asString(result.corruption) //?

   expect(result.isCorrupted).toBeFalsy();
   expect(result.converted.all).toEqual(values);
});
