
import { ArrayMeta, JTC, NumberField, ObjectArrayMeta, ObjectField, ObjectMeta, StringField, TFields } from '@taedr/jsontoclass';


class User {
   public id: number;
   public name: string;
   public age: number;
}


const USER_FIELDS: TFields<User> = {
   id: new NumberField(),
   age: new NumberField({ isNullable: true }),
   name: new StringField({ isNullable: true })
}

const USER_META = new ObjectMeta({
   builder: User,
   fields: USER_FIELDS
});

class SuperUser extends User {
   public role = ``;
}

const SUPER_USER_META = new ObjectMeta({
   builder: SuperUser,
   fields: {
      ...USER_FIELDS,
      age: new NumberField({ isNullable: false }),
      role: new StringField()
   }
});

const users = [
   {
      id: 1,
      name: `Vasya`,
      age: 23,
   },
   {
      id: 2,
      name: `Vasya`,
      age: null,
   },
   {
      id: 3,
      name: `Vasya`,
   },
];

const superUsers = [
   {
      id: 4,
      name: `Vasya`,
      role: `admin`
   },
   {
      id: 5,
      name: `Vasya`,
      role: `admin`,
      age: 20
   }
];

const mixed = [...users, ...superUsers];


it(`Inheritance`, () => {
   const result = JTC.convert({
      id: `No unknown keys in User`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values: users,
   });

   result.converted.all //?

   expect(USER_META.fields.strings.get(`age`).isNullable).toBeTruthy();
   expect(USER_META.fields.strings.get(`name`).isNullable).toBeTruthy();
   expect(result.isCorrupted).toBeFalsy();
   expect(result.converted.all[0].age).toEqual(users[0].age);
   expect(result.converted.all[1].age).toBeUndefined();
   expect(result.converted.all[2].age).toBeUndefined();
});

it(`Inheritance`, () => {
   const result = JTC.convert({
      id: `No unknown keys in User`,
      meta: new ArrayMeta([
         new ObjectField({ meta: USER_META }),
         new ObjectField({ meta: SUPER_USER_META }),
      ]),
      values: mixed,
   });

   USER_META         //?
   SUPER_USER_META   //?

   result.converted.all //?

   const Elog = `3 -> age -> undefined | Value is required`;
   const log = JTC.log.asString(result.corruption) //?

   expect(USER_META.fields.strings.get(`age`).isNullable).toBeTruthy();
   expect(SUPER_USER_META.fields.strings.get(`age`).isNullable).toBeFalsy();
   expect(SUPER_USER_META.fields.strings.get(`name`).isNullable).toBeTruthy();
   expect(result.isCorrupted).toBeTruthy();
   expect(log).toEqual(Elog);
});


it(`Member`, () => {
   const usersWithNulls = [null, ...users, null];

   const result = JTC.convert({
      id: `No unknown keys in User`,
      meta: new ObjectArrayMeta({ meta: USER_META, isNullable: true }),
      values: usersWithNulls
   });

   result.converted.all //?

   expect(result.isCorrupted).toBeFalsy();
   // expect(result.converted.all).toEqual(usersWithNulls);/*  */
});
