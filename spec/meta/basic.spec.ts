import { ArrayField, ArrayMeta, JTC, MapField, MapMeta, NumberField, ObjectField, ObjectMeta, SecondsDateField, StringField, TFields, UnknownField } from '@taedr/jsontoclass';
import { EType } from '@taedr/utils';


const HANDLER = Symbol(`HANDLER`);

class Note {
   public text: string;
   public date: number;
}

const NOTE_META = new ObjectMeta({
   builder: Note,
   fields: {
      text: new StringField(),
      date: new NumberField(),
   }
});

class Address {
   public country: string;
   public city: string;
   public street: string;
}

const ADDRESS_META = new ObjectMeta({
   builder: Address,
   fields: {
      country: new StringField(),
      city: new StringField(),
      street: new StringField(),
   }
});

class User {
   public id: number;
   public name: string;
   public dob: Date;
   public address: Address;
   public notes: Note[] = [];
   public scores: Record<string, number> = {};
   public something: unknown;

   public [HANDLER] = 1;
}

const USER_FIELDS: TFields<User> = {
   id: new NumberField(),
   name: new StringField({ isNullable: true }),
   dob: new SecondsDateField(),
   address: new ObjectField({ meta: ADDRESS_META }),
   notes: new ArrayField({
      meta: new ArrayMeta([
         new ObjectField({ meta: NOTE_META })
      ]),
   }),
   scores: new MapField({
      meta: new MapMeta([new NumberField()]),
   }),
   something: new UnknownField(),
   [HANDLER]: user => 1,
}

const USER_META = new ObjectMeta({
   builder: User,
   fields: USER_FIELDS,
});

it(`User`, () => {
   expect(USER_META.builder).toEqual(User);
   expect(USER_META.id).toEqual(User.name);

   expect(USER_META.fields.strings.size).toEqual(7);
   expect(USER_META.fields.strings.get(`name`).isNullable).toBeTruthy();
   expect(USER_META.fields.symbols.size).toEqual(1);
});

it(`Address`, () => {
   const ADDRESS_META = JTC.getMeta(new Address());
   expect(ADDRESS_META.fields.strings.size).toEqual(3);
});


it(`Note`, () => {
   const NOTE_META = JTC.getMeta(new Note());
   expect(NOTE_META.fields.strings.size).toEqual(2);
});


it(`Super User`, () => {
   class SuperUser extends User {
      public role: string;
   }

   const SUPER_USER_META = new ObjectMeta({
      builder: SuperUser,
      fields: {
         ...USER_FIELDS,
         role: new StringField(),
      }
   });

   expect(SUPER_USER_META.builder).toEqual(SuperUser);
   expect(SUPER_USER_META.id).toEqual(SuperUser.name);
   expect(SUPER_USER_META.parrent).toEqual(USER_META);
   expect(SUPER_USER_META.fields.strings.size).toEqual(8);
});
