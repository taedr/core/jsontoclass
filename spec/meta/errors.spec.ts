import { getError, ERRORS, ArrayMeta, MapMeta, StringField, NumberField, NumberArrayField } from '@taedr/jsontoclass';
import { JTC, ObjectMeta } from '@taedr/jsontoclass';


it(`Not filled iterable`, () => {
   expect(
      () => new ArrayMeta([] as any)
   ).toThrowError(
      getError(``, ``, ERRORS.meta.example)
   );

   expect(
      () => new MapMeta([] as any)
   ).toThrowError(
      getError(``, ``, ERRORS.meta.example)
   );
});


it(`Duplicate`, () => {
   class User {
      public id: string;
   }

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         id: new StringField()
      }
   });

   expect(
      () => new ObjectMeta({
         builder: User,
         fields: {
            id: new StringField()
         }
      })
   ).toThrowError(
      getError(User.name, ``, ERRORS.meta.duplicate)
   );
});

it(`Getter`, () => {
   const values = [1, `2`, true, {}, [], null, undefined, () => { }];

   for (const value of values) {
      expect(
         () => JTC.getMeta(value)
      ).toThrowError(
         getError(``, ``, ERRORS.meta.get)
      );
   }
});

it(`Js native builder`, () => {
   const customs = [
      Array, Object, Function, String,
      Number, Boolean, BigInt, Symbol
   ] as any;

   for (const builder of customs) {
      expect(
         () => new ObjectMeta({ builder, fields: {} })
      ).toThrowError(
         getError(ObjectMeta.name, builder.name, ERRORS.meta.custom)
      );
   }
});

it(`Multi inner iterable`, () => {
   expect(
      () => new ArrayMeta([
         new NumberArrayField(),
         new NumberArrayField(),
      ])
   ).toThrowError(
      getError(``, ``, ERRORS.meta.innerIterable)
   );

   expect(
      () => new MapMeta([
         new NumberArrayField(),
         new NumberArrayField(),
      ])
   ).toThrowError(
      getError(``, ``, ERRORS.meta.innerIterable)
   );
});

it(`Parrent meta`, () => {
   class Id {
      public id: string;
   }

   class User extends Id {
      public name: string;
   }

   expect(
      () => new ObjectMeta({
         builder: User,
         fields: {
            id: new StringField(),
            name: new StringField(),
         }
      })
   ).toThrowError(
      getError(User.name, Id.name, ERRORS.meta.parrent)
   );
});
