
import { JTC, Corruption, ObjectMeta, NumberField, StringField, ObjectArrayMeta, ERRORS } from '@taedr/jsontoclass';

class User {
   public id: number;
   public name: string;
}

const USER_META = new ObjectMeta({
   builder: User,
   getHash: ({ id }) => id + ``,
   fields: {
      id: new NumberField(),
      name: new StringField()
   }
});

it(`Duplicate`, () => {
   const json = [
      { id: 1, name: `Vasya` },
      { id: 2, name: `Masha` },
      { id: 3, name: `Petya` },
      { id: 3, name: `Vitala` },
      { id: 2, name: `Fedor` },
      { id: 1, name: `Maksim` },
      { id: 1, name: `Kiril` },
   ];

   const result = JTC.convert({
      id: `Duplicate User hash check`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values: json,
   });
   const users = result.converted.all;
   const semantic = result.corruption.semantic;
   const message = ERRORS.runtime.hashDuplicates;

   const indexes: number[][] = [
      [0, 5, 6],
      [1, 4],
      [2, 3]
   ];

   const errors = [
      new Corruption(`1`, indexes[0].join(`, `), message),
      new Corruption(`2`, indexes[1].join(`, `), message),
      new Corruption(`3`, indexes[2].join(`, `), message),
   ];

   result   //?
   users     //?

   expect(result.isCorrupted).toBeTruthy();
   expect(semantic).toHaveLength(3);

   for (let i = 0; i < semantic.length; i++) {
      const got = semantic[i];
      const expected = errors[i];

      expect(got.key).toBe(expected.key);
      expect(got.value).toBe(expected.value);
      expect(got.message).toBe(expected.message);
   }

   for (let i = 0; i < users.length; i++) {
      const user = users[i];
      expect(user).toBeInstanceOf(User);
      expect(user).toEqual(json[i]);
   }

   const jsonDuplicatesMap = result.converted.duplicates;
   const dataDuplicatesMap = result.origin.duplicates;

   jsonDuplicatesMap //?
   dataDuplicatesMap //?

   expect(jsonDuplicatesMap).toEqual(dataDuplicatesMap);

   for (const keys of indexes) {
      const jsonValues = keys.map(key => json[key]);
      const dataValues = keys.map(key => users[key]);
      const hash = dataValues[0].id + ``;
      const jsonDuplicates = jsonDuplicatesMap.get(hash).map(({ value }) => value);
      const dataDuplicates = dataDuplicatesMap.get(hash).map(({ value }) => value);

      expect(jsonValues).toEqual(jsonDuplicates);
      expect(jsonValues).toEqual(dataValues);
      expect(dataValues).toEqual(dataDuplicates);
   }
});


it(`Different indexes`, () => {
   const json = [
      { id: 1, name: `Vasya` },
      null,
      { id: 2, name: `Masha` },
      null,
      { id: 3, name: `Petya` },
      { id: 3, name: `Vitala` },
      { id: 2, name: `Fedor` },
      1,
      { id: 1, name: `Maksim` },
      2,
      { id: 1, name: `Kiril` },
   ];

   const result = JTC.convert({
      id: `Different indexes`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values: json,
   });
   const users = result.converted.all;
   const semantic = result.corruption.semantic;
   const message = ERRORS.runtime.hashDuplicates;

   const errors = [
      new Corruption(`1`, `0, 8(5), 10(6)`, message),
      new Corruption(`2`, `2(1), 6(4)`, message),
      new Corruption(`3`, `4(2), 5(3)`, message),
   ];

   result   //?
   users     //?

   expect(result.isCorrupted).toBeTruthy();
   expect(semantic).toHaveLength(3);

   for (let i = 0; i < semantic.length; i++) {
      const got = semantic[i];
      const expected = errors[i];

      expect(got.key).toBe(expected.key);
      expect(got.value).toBe(expected.value);
      expect(got.message).toBe(expected.message);
   }
});
