import { ArrayMeta, JTC, ObjectField } from '@taedr/jsontoclass';
import { FN_PACK } from '@taedr/utils';
import { ivan } from '../utils/json/ivan';
import { zoi } from '../utils/json/zoi';
import { SUPER_USER_META } from '../utils/models/superUser/user';
import { USER_META } from '../utils/models/user/user';


it(`Convert Speed Large`, () => {
   const values = FN_PACK(1000).map(i => {
      return i % 2 === 0 ? ivan : zoi;
   });

   function convert() {
      const result = JTC.convert({
         id: `Convert Speed Large`,
         meta: new ArrayMeta([
            new ObjectField({ meta: SUPER_USER_META }),
            new ObjectField({ meta: USER_META }),
         ]),
         values,
      });

      // result.isCorrupted   //?
      // result.converted     //?
      // result.corruption.duration //?
      // JTC.log.asString(result.corruption) //?

      expect(result.isCorrupted).toBeFalsy();
   }

   for (let i = 0; i < 3; i++) {
      convert() /*?.*/
   }

   /*  Σ 1308.567ms, μ 436.189ms, ⋀ 410.766ms, ⋁ 460.138ms */
   /*  Σ 1186.286ms, μ 395.429ms, ⋀ 382.349ms, ⋁ 411.588ms */
});
