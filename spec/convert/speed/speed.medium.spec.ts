import { ArrayField, ArrayMeta, BooleanField, JTC, NumberField, ObjectField, ObjectMeta, StringField } from '@taedr/jsontoclass';
import { FN_PACK } from '@taedr/utils';


it(`Convert Speed Medium`, () => {
   class Note {
      public text: string;
      public date: number;
   }


   const NOTE_META = new ObjectMeta({
      builder: Note,
      fields: {
         date: new NumberField(),
         text: new StringField()
      }
   });

   class Adress {
      public country: string;
      public state: string;
      public city: string;
      public street: string;
      public zip: string;
   }

   const ADDRESS_META = new ObjectMeta({
      builder: Adress,
      fields: {
         country: new StringField(),
         state: new StringField(),
         city: new StringField(),
         street: new StringField(),
         zip: new StringField(),
      }
   });

   class User {
      public id: number;
      public name: string;
      public age: number;
      public dob: number;
      public email: string;
      public phone: string;
      public isDied: boolean;
      public address: Adress;
      public notes: Note[];
   }

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         id: new NumberField(),
         name: new StringField(),
         age: new NumberField(),
         dob: new NumberField(),
         email: new StringField(),
         phone: new StringField(),
         isDied: new BooleanField(),
         address: new ObjectField({
            meta: ADDRESS_META,
         }),
         notes: new ArrayField({
            meta: new ArrayMeta([
               new ObjectField({ meta: NOTE_META })
            ]),
         }),
      }
   });

   const values = FN_PACK(1000).map(i => {
      return {
         id: i,
         name: 'Katya',
         age: 10,
         dob: 20,
         email: `sm@gmail.com`,
         phone: `3809455512321`,
         isDied: true,
         address: {
            country: `Ukraine`,
            state: `Odesska obl`,
            city: `Odessa`,
            street: `Pushkinska`,
            zip: `239035`
         },
         notes: [
            { text: `${i}`, date: i },
            { text: `${i}`, date: i },
            { text: `${i}`, date: i },
         ]
      };
   });

   function convert() {
      const result = JTC.convert({
         id: `Convert Speed Medium`,
         meta: new ArrayMeta([
            new ObjectField({ meta: USER_META })
         ]),
         values,
      });

      // result.isCorrupted   //?
      // result.converted     //?
      // result.corruption.duration //?

      expect(result.isCorrupted).toBeFalsy();
   }


   for (let i = 0; i < 3; i++) {
      convert(); /*?.*/
   }

   /* Σ 547.779ms, μ 182.593ms, ⋀ 167.913ms, ⋁ 201.514ms */
});
