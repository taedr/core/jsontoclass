import { ArrayMeta, JTC, NumberField, ObjectField, ObjectMeta, StringField } from '@taedr/jsontoclass';
import { FN_PACK } from '@taedr/utils';


it(`Convert Speed Small`, () => {
   class User {
      public age: number;
      public dob: number;
      public name: string;
      public email: string;

   }

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         age: new NumberField(),
         dob: new NumberField(),
         name: new StringField(),
         email: new StringField(),
      }
   });

   const values = FN_PACK(1000).map(i => {
      return {
         name: 'Katya',
         age: 10,
         dob: 20,
         email: `sm@gmail.com`,
      };
   });

   function convert() {
      const result = JTC.convert({
         id: `Convert Speed Small`,
         meta: new ArrayMeta([
            new ObjectField({ meta: USER_META })
         ]),
         values,
      });

      // result.isCorrupted   //?
      // result.converted     //?
      // result.corruption.duration //?

      expect(result.isCorrupted).toBeFalsy();
   }

   for (let i = 0; i < 10; i++) {
      convert(); /*?.*/
   }

   /* Σ 343.718ms, μ 34.372ms, ⋀ 29.713ms, ⋁ 43.813ms */
});
