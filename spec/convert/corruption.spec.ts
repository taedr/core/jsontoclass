import { ArrayMeta, Corruption, ERRORS, JTC, NumberField, ObjectField, ObjectMeta, StringField, TFields } from '@taedr/jsontoclass';
import { FN_GET_ID } from '@taedr/utils';

class User {
   readonly id = '';
   readonly name = '';
}

const USER_FIELDS: TFields<User> = {
   id: new StringField(),
   name: new StringField()
}



const USER_META = new ObjectMeta({
   builder: User,
   getHash: ({ id }) => id,
   fields: USER_FIELDS,
});



class SuperUser extends User {
   readonly age = 0;
}


const SUPER_USER_FIELDS: TFields<SuperUser> = {
   ...USER_FIELDS,
   age: new NumberField()
};

const SUPER_USER_META = new ObjectMeta({
   builder: SuperUser,
   fields: SUPER_USER_FIELDS,
});

class ExtraUser extends SuperUser {
   readonly role = ``;
}

const EXTRA_USER_META = new ObjectMeta({
   builder: ExtraUser,
   fields: {
      ...SUPER_USER_FIELDS,
      role: new StringField()
   }
});

EXTRA_USER_META //?
const json: any = [
   {
      id: `1`,
      name: `aaa`,
      age: 12,
   },
   {
      id: `3`,
      name: `bbb`,
   },
   null,
   {
      id: `2`,
      name: `ccc`,
      age: 14,
      role: `aaa`
   },
   {},
   1,
   {
      id: `1`,
      name: `ddd`,
   },
   `2`,
   {
      id: `2`,
      name: `eee`,
      age: 13
   },
   {
      yyqwedqwd: 12,
      dassdad: 13
   },
];

const operationId = `User Corruption`;
const result = JTC.convert({
   id: operationId,
   meta: new ArrayMeta([
      new ObjectField({ meta: USER_META }),
      new ObjectField({ meta: SUPER_USER_META }),
      new ObjectField({ meta: EXTRA_USER_META }),
   ]),
   values: json,
});
const tree = result.corruption;
const users = result.converted.all;

const validJson = [0, 1, 3, 6, 8].map(i => json[i]);
const toExcludeJson = [2, 4, 5, 7, 9].map(i => json[i]);
const instancesOrder = [SuperUser, User, ExtraUser, User, SuperUser];
const usersOrder = users.map(user => user['constructor']);

result.converted.all //?
users       //?
validJson   //?
JTC.log.asString(tree) //?

tree

it(`Same instances`, () => {
   expect(users).toEqual(validJson);
   expect(usersOrder).toEqual(instancesOrder);
   expect(users).toHaveLength(validJson.length);
});


it(`Corruption`, () => {
   expect(result.isCorrupted).toBeTruthy();
   expect(tree.dataKey).toBe(operationId);
   expect(tree.corrupted).toHaveLength(0);
   expect(tree.branches).toHaveLength(2);
   expect(tree.missing).toHaveLength(0);
   expect(tree.excluded).toHaveLength(3);
   expect(tree.semantic).toHaveLength(2);
});


it(`Duplicated ids`, () => {
   expect(tree.semantic[0]).toEqual(new Corruption(`1`, `0, 6(3)`, ERRORS.runtime.hashDuplicates));
   expect(tree.semantic[1]).toEqual(new Corruption(`2`, `3(2), 8(4)`, ERRORS.runtime.hashDuplicates));
});
