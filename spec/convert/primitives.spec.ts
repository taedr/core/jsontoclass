import { AField, ArrayMeta, BigintField, BooleanField, JTC, NumberField, StringField, UnknownField } from '@taedr/jsontoclass';
import { TPrimitive } from '@taedr/utils';



it(`Number`, async () => {
   const values = [1, 2, 3, 4, 5];
   const result = JTC.convert({
      id: `Number`,
      meta: new ArrayMeta([new NumberField()]),
      values
   });

   result.converted.all //?

   expect(result.isCorrupted).toBeFalsy();
   expect(result.converted.all).toEqual(values);
});

it(`String`, async () => {
   const values = [`1`, `2`, `3`, `4`, `5`];
   const result = JTC.convert({
      id: `String`,
      meta: new ArrayMeta([new StringField()]),
      values
   });

   result.converted.all //?

   expect(result.isCorrupted).toBeFalsy();
   expect(result.converted.all).toEqual(values);
});


it(`Boolean`, async () => {
   const values = [true, false, true, false, true];
   const result = JTC.convert({
      id: `Boolean`,
      meta: new ArrayMeta([new BooleanField()]),
      values
   });

   result.converted.all //?

   expect(result.isCorrupted).toBeFalsy();
   expect(result.converted.all).toEqual(values);
});


it(`BigInt`, async () => {
   const values = [1, 2, 3, 4, 5].map(value => BigInt(value));
   const result = JTC.convert({
      id: `BigInt`,
      meta: new ArrayMeta([new BigintField()]),
      values
   });

   result.converted.all //?

   expect(result.isCorrupted).toBeFalsy();
   expect(result.converted.all).toEqual(values);
});


it(`Unknown`, async () => {
   const values = [1, `2`, 3, true, `4`, 5, false];
   const result = JTC.convert({
      id: `Misc`,
      meta: new ArrayMeta([new UnknownField()]),
      values
   });

   result.converted.all //?

   expect(result.isCorrupted).toBeFalsy();
   expect(result.converted.all).toEqual(values);
});


it(`Misc`, async () => {
   const values = [1, `2`, 3, true, `4`, 5, false];
   const result = JTC.convert({
      id: `Misc`,
      meta: new ArrayMeta([
         new StringField(),
         new NumberField(),
         new BooleanField()
      ] as [AField<TPrimitive>, AField<TPrimitive>, AField<TPrimitive>]),
      values
   });

   result.converted.all //?

   expect(result.isCorrupted).toBeFalsy();
   expect(result.converted.all).toEqual(values);
});
