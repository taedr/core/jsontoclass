
import { ArrayField, ArrayMeta, JTC, NumberField, ObjectField, ObjectMeta, StringField } from '@taedr/jsontoclass';
import { TNew } from '@taedr/utils';

class Note {
   public text = ``;
}

const NOTE_META = new ObjectMeta({
   builder: Note,
   fields: {
      text: new StringField()
   }
});


class Coords {
   public lat = 0;
   public lng = 0;
}

const COORDS_META = new ObjectMeta({
   builder: Coords,
   fields: {
      lat: new NumberField(),
      lng: new NumberField(),
   }
});

class User {
   public id = 0;
   public name = ``;
   public notes: Note[] = [];
   public coords = new Coords();
}

const USER_META = new ObjectMeta({
   builder: User,
   fields: {
      id: new NumberField(),
      name: new StringField(),
      coords: new ObjectField({ meta: COORDS_META }),
      notes: new ArrayField({
         meta: new ArrayMeta([
            new ObjectField({ meta: NOTE_META })
         ]),
      }),
   }
});

const USER_ARRAY_META = new ArrayMeta([
   new ObjectField({ meta: USER_META })
]);

const json = [
   { id: 1, name: `Vasya`, notes: [{ text: `1` }], coords: { lat: 1, lng: 1 } },
   { id: 2, name: `Masha`, notes: [{ text: `2` }], coords: { lat: 2, lng: 2 } },
   { id: 3, name: `Petya`, notes: [{ text: `3` }], coords: { lat: 3, lng: 3 } },
   { id: 4, name: `Vitala`, notes: [{ text: `4` }], coords: { lat: 4, lng: 4 } },
   { id: 5, name: `Fedor`, notes: [{ text: `5` }], coords: { lat: 5, lng: 5 } },
   { id: 6, name: `Maksim`, notes: [{ text: `6` }], coords: { lat: 6, lng: 6 } },
   { id: 7, name: `Kiril`, notes: [{ text: `7` }], coords: { lat: 7, lng: 7 } },
];

it(`Plain`, () => {
   const result = JTC.convert({
      id: `No User Freeze`,
      meta: USER_ARRAY_META,
      values: json,
   });
   const users = result.converted.all;

   result   //?
   users    //?

   expect(result.isCorrupted).toBeFalsy();

   for (let i = 0; i < users.length; i++) {
      const user = users[i];
      expect(user).toBeInstanceOf(User);
      expect(user).toEqual(json[i]);

      user.id = 1;
      user.name = `1`;
      user.coords.lat = 1;
      user.notes[0].text = `1`

      expect(user.id).toBe(1);
      expect(user.name).toBe(`1`);
      expect(user.coords.lat).toBe(1);
      expect(user.notes[0].text).toBe(`1`);
   }

   const user = new User();
   users.push(user)
   expect(users[users.length - 1]).toBe(user);
});

it(`Freeze`, () => {
   const result = JTC.convert({
      id: `User Freeze`,
      meta: USER_ARRAY_META,
      values: json,
      isFreeze: true,
   });
   const users = result.converted.all;
   const getError = (key: string, builder: TNew<object>) => `Cannot assign to read only property '${key}' of object '#<${builder.name}>'`;
   const getArrayError = (index: number) => `Cannot add property ${index}, object is not extensible`;

   result   //?
   users    //?

   expect(result.isCorrupted).toBeFalsy();

   for (let i = 0; i < users.length; i++) {
      const user = users[i];
      const note = new Note();

      expect(user).toBeInstanceOf(User);
      expect(user).toEqual(json[i]);

      expect(() => user.id = 1).toThrowError(getError(`id`, User));
      expect(() => user.name = `1`).toThrowError(getError(`name`, User));
      expect(() => user.coords.lat = 1).toThrowError(getError(`lat`, Coords));
      expect(() => user.notes[0].text = `1`).toThrowError(getError(`text`, Note));
      expect(() => user.notes.push(note)).toThrowError(getArrayError(1));
      expect(() => user.notes.pop()).toThrowError();
      expect(() => user.notes[3] = note).toThrowError();

      expect(user).toEqual(json[i]);
   }

   const user = new User();

   expect(() => users.push(user)).toThrowError(getArrayError(7));
   expect(() => users.pop()).toThrowError();
});
