
import { ArrayMeta, JTC, NumberField, ObjectField, ObjectMeta, StringField } from '@taedr/jsontoclass';

class User {
   constructor(
      public id: number,
      public name: string,
   ) { }
}

const USER_META = new ObjectMeta({
   builder: User,
   fields: {
      id: new NumberField(),
      name: new StringField()
   }
});

const USERS_META = new ArrayMeta([
   new ObjectField({ meta: USER_META })
]);

const json = {
   id: 1,
   name: `Vasya`,
   age: 23,
}

it(`Valid`, () => {
   const result = JTC.convert({
      id: `No unknown keys in User`,
      meta: USERS_META,
      values: [{
         id: json.id,
         name: json.name
      }],
   });

   const [user] = result.converted.all;

   result   //?
   user     //?

   expect(result.isCorrupted).toBeFalsy();
   expect(user).toBeInstanceOf(User);
   expect(user.id).toBe(json.id);
   expect(user.name).toBe(json.name);
});

it(`ERROR`, () => {
   const result = JTC.convert({
      id: `Error on unknown keys in User`,
      meta: USERS_META,
      values: [json],
      SUnknownKey: 'ERROR'
   });

   const [user] = result.converted.all;
   const [branch] = result.corruption.branches;
   const [excluded] = branch.excluded;

   user     //?
   excluded //?

   JTC.log.asString(result.corruption) //?

   expect(result.isCorrupted).toBeTruthy();
   expect(result.corruption.branches).toHaveLength(1);
   expect(branch.excluded).toHaveLength(1);
   expect(excluded.value).toBe(json.age);
   expect(excluded.key).toBe(`age`);
   expect(excluded.message).toBe(`Unknown key`);
   expect(user.id).toBe(json.id);
   expect(user.name).toBe(json.name);
   expect(user['age']).toBeUndefined();
});

it(`IGNORE`, () => {
   const result = JTC.convert({
      id: `Ignore unknown keys in User`,
      meta: USERS_META,
      values: [json],
      SUnknownKey: 'IGNORE'
   });
   const [user] = result.converted.all;

   user     //?

   expect(result.isCorrupted).toBeFalsy();
   expect(result.corruption.branches).toHaveLength(0);
   expect(user.id).toBe(json.id);
   expect(user.name).toBe(json.name);
   expect(user['age']).toBeUndefined();
});


it(`SAVE`, () => {
   const result = JTC.convert({
      id: `Save unknown keys in User`,
      meta: USERS_META,
      values: [json],
      SUnknownKey: 'SAVE',
   });

   const [user] = result.converted.all;

   user     //?

   expect(result.isCorrupted).toBeFalsy();
   expect(result.corruption.branches).toHaveLength(0);
   expect(user.id).toBe(json.id);
   expect(user.name).toBe(json.name);
   expect(user['age']).toBe(json.age);
});
