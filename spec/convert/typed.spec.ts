
import { ArrayMeta, JTC, NumberField, ObjectField, ObjectMeta, StringField } from '@taedr/jsontoclass';

it(`Convert - typed`, () => {
   enum EActionType {
      DELETE,
      UPDATE,
      ADD,
   }

   class User {
      public id: number;
      public name: string;
      public age: number;
   }

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         id: new NumberField(),
         name: new StringField(),
         age: new NumberField(),
      }
   });

   class Update {
      public type: EActionType.UPDATE = EActionType.UPDATE;
      public data: User;
   }

   const UPDATE_META = new ObjectMeta({
      builder: Update,
      isOfType: ({ type }) => type === EActionType.UPDATE,
      fields: {
         type: new NumberField(),
         data: new ObjectField({
            meta: USER_META,
            isPartial: true,
         })
      }
   });

   class Add {
      public type: EActionType.ADD = EActionType.ADD;
      public data: User;
   }

   const ADD_META = new ObjectMeta({
      builder: Add,
      isOfType: ({ type }) => type === EActionType.ADD,
      fields: {
         type: new NumberField(),
         data: new ObjectField({ meta: USER_META })
      }
   });

   class Delete {
      public type: EActionType.DELETE = EActionType.DELETE;
      public id: number;
   }

   const DELETE_META = new ObjectMeta({
      builder: Delete,
      isOfType: ({ type }) => type === EActionType.DELETE,
      fields: {
         type: new NumberField(),
         id: new NumberField(),
      }
   });

   const ACTIONS_META = new ArrayMeta([
      new ObjectField({ meta: UPDATE_META }),
      new ObjectField({ meta: ADD_META }),
      new ObjectField({ meta: DELETE_META }),
   ] as [
         ObjectField<Update | Add | Delete>,
         ObjectField<Update | Add | Delete>,
         ObjectField<Update | Add | Delete>,
      ]);

   const values = [
      {
         type: EActionType.DELETE,
         id: 1,
      },
      {
         type: EActionType.UPDATE,
         data: {
            name: `Masha`
         },
      },
      {
         type: EActionType.ADD,
         data: {
            id: 4,
            name: `Evdokia`,
            age: 23
         },
      },
      {
         type: EActionType.UPDATE,
         data: {
            id: 3,
            name: `Vitala`
         },
      },
      {
         type: EActionType.ADD,
         data: {
            id: 2,
            name: `Inragim`,
            age: 45
         },
      },
      {
         type: EActionType.DELETE,
         id: 1,
      },

   ];

   const builderByType = {
      [EActionType.ADD]: Add,
      [EActionType.DELETE]: Delete,
      [EActionType.UPDATE]: Update,
   }

   const result = JTC.convert({
      id: `Patial User Data`,
      meta: ACTIONS_META,
      values,
   });

   const actions = result.converted.all;

   console.log(actions);

   expect(result.isCorrupted).toBeFalsy();

   for (let i = 0; i < actions.length; i++) {
      const action = actions[i];

      expect(action).toBeInstanceOf(builderByType[action.type]);
      expect(action).toEqual(values[i]);
   }
});
