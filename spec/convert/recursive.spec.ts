import { JTC, NumberField, ObjectArrayField, ObjectArrayMeta, ObjectField, ObjectMeta, StringField } from '@taedr/jsontoclass';



it(`Object`, () => {
   class User {
      public id: number;
      public name: string;
      public referal: User | null;
   }

   const USER_META = new ObjectMeta<User>({
      builder: User,
      fields: {
         id: new NumberField(),
         name: new StringField(),
         referal: new ObjectField({
            get meta() { return USER_META },
            isNullable: true,
         })
      }
   }) as ObjectMeta<User>;

   const values = [
      {
         id: 0,
         name: `Vasya`,
         referal: {
            id: 1,
            name: `Masha`
         }
      }
   ];

   USER_META.fields.strings.get(`referal`) //?

   const result = JTC.convert({
      id: ``,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values,
   });

   JTC.log.asString(result.corruption) //?

   expect(result.isCorrupted).toBeFalsy();
   expect(result.converted.all).toEqual(values);
});

it(`Array`, () => {
   class User {
      public id: number;
      public name: string;
      public referals: User[];
   }

   const USER_META = new ObjectMeta<User>({
      builder: User,
      fields: {
         id: new NumberField(),
         name: new StringField(),
         referals: new ObjectArrayField({
            get meta() { return USER_META },
         })
      }
   });

   const values = [
      {
         id: 0,
         name: `Vasya`,
         referals: [{
            id: 1,
            name: `Masha`,
            referals: []
         }]
      }
   ];

   const result = JTC.convert({
      id: ``,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values,
   });

   JTC.log.asString(result.corruption) //?

   expect(result.isCorrupted).toBeFalsy();
   expect(result.converted.all).toEqual(values);
});
