import { ArrayMeta, JTC } from '@taedr/jsontoclass';
import { ivan } from './utils/json/ivan';
import { zoi } from './utils/json/zoi';
import { SuperAddress } from './utils/models/superUser/address';
import { SUPER_USER_ENTRY, SUPER_USER_META } from './utils/models/superUser/user';
import { Address } from './utils/models/user/address';
import { Signin } from './utils/models/user/signin';
import { Social } from './utils/models/user/social';
import { USER_ENTRY, USER_META } from './utils/models/user/user';


it(`Convert`, () => {
   const values = [ivan, zoi];

   const result = JTC.convert({
      id: `Convertion Smaple`,
      values,
      meta: new ArrayMeta([USER_ENTRY, SUPER_USER_ENTRY]),
   });

   const { corruption, origin, converted, isCorrupted } = result;
   const [_ivan, _zoi] = converted.all;

   _ivan //?
   _zoi  //?

   // USER_META.fields.all.get(`signins`) //?

   SUPER_USER_META //?
   JTC.log.asString(corruption) //?

   SUPER_USER_META.fields //?

   expect(isCorrupted).toBeFalsy();

   expect(converted.all).toHaveLength(2);
   expect(converted.valid).toHaveLength(2);
   expect(converted.corrupted).toHaveLength(0);

   expect(origin.all).toHaveLength(converted.all.length);
   expect(origin.all).toHaveLength(converted.valid.length);
   expect(origin.corrupted).toHaveLength(0);
   expect(origin.excluded).toHaveLength(0);

   expect(_ivan).toBeInstanceOf(USER_META.builder);
   expect(_zoi).toBeInstanceOf(SUPER_USER_META.builder);

   expect(corruption.valuesCount).toEqual(2);

   expect(_ivan.address).toBeInstanceOf(Address);
   expect(_ivan.social).toBeInstanceOf(Social);

   for (const signin of _ivan.signins) {
      expect(signin).toBeInstanceOf(Signin);
   }

   expect(_zoi.address).toBeInstanceOf(SuperAddress);
});
