
import { ArrayMapField, DimensionalArrayField, DimensionalArrayMeta, JTC, MapArrayField, NumberField, ObjectArrayMeta, ObjectMeta, StringArrayMeta, StringField, StringMapMeta } from '@taedr/jsontoclass';

it(`Object`, () => {
   class User {
      public id: number;
      public name: string;
   }

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         id: new NumberField(),
         name: new StringField()
      },
   });

   const misc = [
      [
         {
            id: 1,
            name: `Vasya`,
         },
         {
            id: 2,
            name: `Kolyan`,
         },
      ],
      [
         {
            id: 3,
            name: `Glasha`,
         },
         {
            id: 4,
            name: `Evdokia`,
         },
      ]
   ];

   const result = JTC.convert({
      id: `No unknown keys in User`,
      meta: new DimensionalArrayMeta({
         meta: new ObjectArrayMeta({ meta: USER_META })
      }),
      values: misc,
   });

   result.converted.all //?

   JTC.log.asString(result.corruption) //?

   expect(result.isCorrupted).toBeFalsy();
   expect(result.converted.all).toEqual(misc);
});

it(`Primitives`, () => {
   const values = [
      [
         [`1`, `2`],
         [`3`, `4`],
      ],
      [
         [`5`, `6`],
         [`7`, `8`],
      ],
   ];

   const result = JTC.convert({
      id: ``,
      meta: new DimensionalArrayMeta({
         meta: new DimensionalArrayMeta({
            meta: new StringArrayMeta()
         })
      }),
      values,
   })

   result.converted.all //?

   expect(result.isCorrupted).toBeFalsy();
   expect(result.converted.all).toEqual(values);
});

it(`Nested`, () => {
   class Test {
      public map: Record<string, Array<string>>;
      public array: Record<string, string>[];
   }


   const TEST_META = new ObjectMeta({
      builder: Test,
      fields: {
         map: new ArrayMapField({
            entry: {
               meta: new StringArrayMeta()
            },
         }),
         array: new MapArrayField({
            entry: {
               meta: new StringMapMeta(),
            }
         })
      }
   });


   const values = [
      {
         map: {
            one: [`1`, `2`],
            two: [`3`, `4`],
         },
         array: [
            {
               one: `1`,
               two: `2`
            },
            {
               tree: `3`,
               four: `4`
            }
         ]
      },
   ];

   const result = JTC.convert({
      id: ``,
      meta: new ObjectArrayMeta({ meta: TEST_META }),
      values,
   });
   const [value] = result.converted.all;

   value //?

   JTC.log.asString(result.corruption) //?

   expect(result.isCorrupted).toBeFalsy();
   expect(value).toEqual(values[0]);
});
