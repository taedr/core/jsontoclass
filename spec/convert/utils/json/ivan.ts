import { DAY_MS, YEAR_MS } from '@taedr/utils';

export const ivan = {
   type: `user`,
   id: `11111111`,
   name: `Ivan`,
   gender: `male`,
   isPremium: false,
   dob: Date.now() - YEAR_MS * 23,
   address: {
      country: `Ukraine`,
      city: `Odessa`,
      street: `Arnautskaa`
   },
   social: {
      email: `ivan@gmail.com`,
   },
   signins: [
      {
         date: Date.now() - DAY_MS * 1,
      },
      {
         date: Date.now() - DAY_MS * 5,
         geo: {
            latitude: 34,
            longitude: 98
         }
      },
   ]
};