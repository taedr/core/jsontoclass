import { DAY_MS, YEAR_MS } from '@taedr/utils';
import { ivan } from './ivan';

export const zoi = {
   type: `super`,
   id: `22222222`,
   name: `Zoi`,
   gender: `female`,
   isPremium: true,
   dob: Date.now() - YEAR_MS * 18,
   referal: ivan,
   role: `admin`,
   temp: {},
   address: {
      country: `Ukraine`,
      city: `Odessa`,
      street: `Derebasovska`,
      zipcode: 78001,
   },
   social: {
      email: `zoi@mail.ru`,
      linkedIn: `https://www.linkedin.com/in/zoi`
   },
   tasks: {
      "Bring new user": true,
      "Ban bad people": false,
   },
   signins: [
      {
         ip: `192.168.1.1`,
         date: Date.now() - DAY_MS * 1,
      },
      {
         date: Date.now() - DAY_MS * 5,
         geo: {
            latitude: 34,
            longitude: 98
         }
      },
   ]
};
