import { ArrayMeta, JTC, MillisDateField, ObjectField, ObjectMeta, TFields } from '@taedr/jsontoclass';
import { Geo, GEO_META } from './geo';

export class Signin {
   public date: Date;
   public geo: Geo | null;
}


export const SIGNIN_FIELDS: TFields<Signin> = {
   date: new MillisDateField(),
   geo: new ObjectField({
      meta: GEO_META,
      isNullable: true,
   }),
};


export const SIGNIN_META = new ObjectMeta({
   builder: Signin,
   fields: SIGNIN_FIELDS,
});
