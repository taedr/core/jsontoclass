import { NumberField, ObjectMeta } from '@taedr/jsontoclass';

export class Geo {
   public latitude = 0;
   public longitude = 0;
}


export const GEO_META = new ObjectMeta({
   builder: Geo,
   fields: {
      latitude: new NumberField({
         min: -90,
         max: 90,
      }),
      longitude: new NumberField({
         min: -180,
         max: 180
      }),
   },
});
