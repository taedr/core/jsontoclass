import { BooleanField, FunctionField, MillisDateField, NumberField, ObjectArrayField, ObjectField, ObjectMeta, StringField, TFields } from '@taedr/jsontoclass';
import { YEAR_MS } from '@taedr/utils';
import { Address, ADDRESS_META } from './address';
import { Signin, SIGNIN_META } from './signin';
import { Social, SOCIAL_META } from './social';

export enum EUser {
   user = `user`,
   super = `super`,
}


export enum EGender {
   male = `male`,
   female = `female`,
   unknown = `unknown`,
}


export class User {
   public type: EUser;
   public id: string;
   public name: string;
   public isPremium: boolean;
   public gender: EGender;
   public address: Address;
   public social: Social;
   public signins: Signin[];
   public dob?: Date;
   public referal?: User;

   public get age(): number { return Math.floor((Date.now() - +this.dob) / YEAR_MS); }
   // -----------------------------------------------
   //                   Api
   // -----------------------------------------------
   public sayHi() {
      const age = this.age;
      const prefix = `Hi! My name is ${this.name}, I`;

      if (isNaN(age)) {
         return prefix + ` don't know how old am I...`;
      } else {
         return prefix + `'m ${age} years old.`;
      }
   }
}

export const USER_FIELDS: TFields<User> = {
   type: new StringField({ enum: EUser }),
   id: new StringField(),
   name: new StringField(),
   isPremium: new BooleanField(),
   gender: new StringField({ enum: EGender }),
   address: new ObjectField({ meta: ADDRESS_META }),
   social: new ObjectField({ meta: SOCIAL_META }),
   signins: new ObjectArrayField({ meta: SIGNIN_META }),
   dob: new MillisDateField({ isNullable: true }),
   age: new NumberField({ isCalculated: true }),
   sayHi: new FunctionField(),
   referal: new ObjectField({
      isNullable: true,
      get meta() { return USER_META; }
   }),
};

export const USER_META = new ObjectMeta({
   builder: User,
   isOfType: ({ type }) => type === EUser.user,
   fields: USER_FIELDS,
});

export const USER_ENTRY = new ObjectField({ get meta() { return USER_META } });
