import { ObjectMeta, StringField, TFields } from '@taedr/jsontoclass';

export class Social {
   public email = ``;
}

export const SOCIAL_FIELDS: TFields<Social> = {
   email: new StringField(),
};


export const SOCIAL_META = new ObjectMeta({
   builder: Social,
   fields: SOCIAL_FIELDS,
});
