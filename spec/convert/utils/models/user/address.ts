import { ObjectMeta, StringField, TFields } from '@taedr/jsontoclass';

export class Address {
   public country: string;
   public city: string;
   public street: string;
}


export const ADDRESS_FIELDS: TFields<Address> = {
   city: new StringField(),
   country: new StringField(),
   street: new StringField(),
};

export const ADDRESS_META = new ObjectMeta({
   builder: Address,
   fields: ADDRESS_FIELDS,
});
