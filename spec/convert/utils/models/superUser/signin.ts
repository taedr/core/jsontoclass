import { ObjectMeta, StringField } from '@taedr/jsontoclass';
import { Signin, SIGNIN_FIELDS } from '../user/signin';

export class SuperSignin extends Signin {
   public ip: string;
}


export const SUPER_SIGNIN_META = new ObjectMeta({
   builder: SuperSignin,
   fields: {
      ...SIGNIN_FIELDS,
      ip: new StringField(),
   },
});
