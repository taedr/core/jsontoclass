import { NumberField, ObjectMeta } from '@taedr/jsontoclass';
import { Address, ADDRESS_FIELDS } from '../user/address';

export class SuperAddress extends Address {
   public zipcode: number;
}


export const SUPER_ADDRESS_META = new ObjectMeta({
   builder: SuperAddress,
   fields: {
      ...ADDRESS_FIELDS,
      zipcode: new NumberField(),
   },
});
