import { User, EUser, USER_FIELDS } from '../user/user';
import { BooleanField, BooleanMapField, MapField, MapMeta, ObjectField, ObjectMeta, StringField, UnknownField } from '@taedr/jsontoclass';
import { SuperAddress, SUPER_ADDRESS_META } from './address';
import { SuperSocial, SUPER_SOCIAL_META } from './social';

export enum ERole {
   admin = `admin`,
   manager = `manager`,
   editor = `editor`,
}


export class SuperUser extends User {
   public type: EUser;
   public role: ERole;
   public address = new SuperAddress();
   public social = new SuperSocial();
   public tasks: Record<string, boolean> = {};
   public temp: unknown;
}


export const SUPER_USER_META = new ObjectMeta({
   builder: SuperUser,
   isOfType: ({ type }) => type === EUser.super,
   fields: {
      ...USER_FIELDS,
      role: new StringField({ enum: ERole }),
      address: new ObjectField({ meta: SUPER_ADDRESS_META }),
      social: new ObjectField({ meta: SUPER_SOCIAL_META }),
      temp: new UnknownField(),
      tasks: new BooleanMapField(),
   }
});

export const SUPER_USER_ENTRY = new ObjectField({ meta: SUPER_USER_META });
