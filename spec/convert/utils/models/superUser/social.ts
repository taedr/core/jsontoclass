import { ObjectMeta, StringField } from '@taedr/jsontoclass';
import { Social, SOCIAL_FIELDS } from '../user/social';

export class SuperSocial extends Social {
   public linkedIn: string;
}

export const SUPER_SOCIAL_META = new ObjectMeta({
   builder: SuperSocial,
   fields: {
      ...SOCIAL_FIELDS,
      linkedIn: new StringField({
         pattern: /https:\/\/www.linkedin.com\/in\/[\w,-]+/,
      }),
   }
});
