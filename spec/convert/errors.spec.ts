import { JTC, ERRORS, ObjectMeta, ArrayMeta, StringField, NumberField, ObjectField } from '@taedr/jsontoclass';


it(`Exclude`, () => {
   class User {
      public id: string;
      public age: number
   }

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         id: new StringField(),
         age: new NumberField()
      }
   });

   const json = {
      aa: `1111`,
      cc: 10
   };

   const result = JTC.convert({
      id: ``,
      meta: new ArrayMeta([
         new ObjectField({ meta: USER_META })
      ]),
      values: [json],
   });
   const { isCorrupted, tree } = result;

   tree //?

   //    const { message, key } = tree.branches[0]


   //    expect(isCorrupted).toBeTruthy();
   //    expect(message).toEqual(ERRORS.runtime.exclude);
   //    expect(key).toEqual(`0`);
});
