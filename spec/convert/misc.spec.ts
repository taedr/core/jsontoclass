
import { AField, ArrayMeta, BooleanField, JTC, NumberField, ObjectField, ObjectMeta, StringField, TFields } from '@taedr/jsontoclass';


class User {
   public id = 0;
   public name = ``;
}

const USER_FIELDS: TFields<User> = {
   id: new NumberField(),
   name: new StringField()
};


const USER_META = new ObjectMeta({
   builder: User,
   fields: USER_FIELDS,
});

class SuperUser extends User {
   public role = ``;
}

const SUPER_USER_META = new ObjectMeta({
   builder: SuperUser,
   fields: {
      ...USER_FIELDS,
      role: new StringField()
   },
});

const misc = [
   1,
   {
      id: 1,
      name: `Vasya`,
   },
   null,
   `AAAA`,
   {
      id: 5,
      name: `Vasya`,
      role: `admin`,
   },
   true
];


it(`All`, () => {
   const result = JTC.convert({
      id: `No unknown keys in User`,
      meta: new ArrayMeta([
         new ObjectField({ meta: USER_META, isNullable: true }),
         new ObjectField({ meta: SUPER_USER_META }),
         new StringField(),
         new NumberField(),
         new BooleanField(),
      ] as [
            AField<User | SuperUser | string | number | boolean>,
            ...AField<User | SuperUser | string | number | boolean>[],
         ]),
      values: misc,
   });

   result.converted.all //?

   expect(result.isCorrupted).toBeFalsy();
   expect(result.converted.all).toEqual(misc);
});
