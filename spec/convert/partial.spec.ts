
import { ArrayMeta, JTC, NumberField, ObjectField, ObjectMeta, StringField } from '@taedr/jsontoclass';

class User {
   constructor(
      public id: number,
      public name: string,
      public age: number
   ) { }
}

const USER_META = new ObjectMeta({
   builder: User,
   fields: {
      id: new NumberField(),
      name: new StringField(),
      age: new NumberField()
   }
});

const json = [
   { id: 1 },
   { name: `Masha` },
   { age: 23 },
   { id: 3, name: `Vitala` },
   { id: 2, age: 45 },
   { name: `Maksim`, age: 33 },
   { id: 1, name: `Kiril`, age: 76 },
];

it(`Plain`, () => {
   const result = JTC.convert({
      id: `Full User Data`,
      meta: new ArrayMeta([
         new ObjectField({ meta: USER_META })
      ]),
      values: json,
   });
   const users = result.converted.all;
   const expected = [...USER_META.fields.strings.keys()];

   result   //?
   users    //?
   expected //?

   expect(result.isCorrupted).toBeTruthy();

   for (let i = 0; i < users.length; i++) {
      const user = users[i];

      for (const key of expected) {
         expect(user).toHaveProperty(key);
      }

      expect(user).toBeInstanceOf(User);
      expect(user).toEqual(json[i]);
   }
});


it(`Partial`, () => {
   const result = JTC.convert({
      id: `Patial User Data`,
      meta: new ArrayMeta([
         new ObjectField({ meta: USER_META, isPartial: true })
      ]),
      values: json,
   });
   const users = result.converted.all;
   const expected = [
      [`name`, `age`],
      [`id`, `age`],
      [`id, name`],
      [`age`],
      [`name`],
      [`id`],
      [],
   ];

   result   //?
   users     //?

   expect(result.isCorrupted).toBeFalsy();

   for (let i = 0; i < users.length; i++) {
      const user = users[i];

      for (const key of expected[i]) {
         expect(user).not.toHaveProperty(key);
      }

      expect(user).toBeInstanceOf(User);
      expect(user).toEqual(json[i]);
   }
});
