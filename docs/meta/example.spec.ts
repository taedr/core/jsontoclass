import { NumberField, NumberMapField, ObjectArrayField, ObjectField, ObjectMeta, StringField } from '@taedr/jsontoclass';


it(`Doc - Meta - Example`, () => {
   class Note {
      public text: string;
      public date: number;
   }

   const NOTE_META = new ObjectMeta({
      builder: Note,
      fields: {
         date: new NumberField(),
         text: new StringField()
      }
   })


   class Address {
      public country: string;
      public city: string;
      public street: string;
   }

   const ADDRESS_META = new ObjectMeta({
      builder: Address,
      fields: {
         city: new StringField(),
         country: new StringField(),
         street: new StringField(),
      }
   })

   class User {
      public address: Address;
      public notes: Note[] = [];
      public marks: Record<string, number> = {};

      constructor(
         public id: number,
         public name: string,
      ) { }

   }

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         id: new NumberField(),
         name: new StringField(),
         address: new ObjectField({ meta: ADDRESS_META }),
         marks: new NumberMapField(),
         notes: new ObjectArrayField({ meta: NOTE_META })
      }
   });


   console.log(USER_META);
});
