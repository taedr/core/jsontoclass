import { ERRORS, JTC, ObjectArrayMeta, ObjectMeta, StringField } from "@taedr/jsontoclass";


class User {
   public id: string;
   public name: string;
}

const USER_META = new ObjectMeta<User>({
   builder: User,
   getHash: ({ id }) => id,
   fields: {
      id: new StringField(),
      name: new StringField(),
   }
});

it(`Valid`, () => {
   const values = [
      {
         id: `1`,
         name: `Vasya`,
      },
      {
         id: `2`,
         name: `Masha`,
      },
   ];

   const result = JTC.convert({
      id: `Get Hash - Valid`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values
   });

   const users = result.converted.all;

   console.log(users);

   expect(result.isCorrupted).toBeFalsy();
   expect(result.converted.all).toEqual(values);
});

it(`Corrupted`, () => {
   const values = [
      {
         id: `1`,
         name: `Vasya`,
      },
      {
         id: `2`,
         name: `Masha`,
      },
      {
         id: `1`,
         name: `Petya`,
      },
   ];

   const result = JTC.convert({
      id: `Get Hash - Corrupted`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values
   });

   const users = result.converted.all;

   console.log(JTC.log.asString(result.corruption)); // 0, 2 -> 1 | Duplicated id
   console.log(users);
   console.log(result.origin.duplicates);    //
   console.log(result.converted.duplicates); //


   expect(result.isCorrupted).toBeTruthy();
   expect(result.converted.all).toEqual(values);
   expect(JTC.log.asString(result.corruption)).toEqual(`0, 2 -> 1 | ${ERRORS.runtime.hashDuplicates}`);
});
