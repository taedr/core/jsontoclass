import { NumberField, ObjectField, ObjectMeta, StringField } from '@taedr/jsontoclass';


it(`Doc - Meta - Is of Type`, () => {
   class Id {
      public id: number;
   }

   const ID_META = new ObjectMeta({
      builder: Id,
      fields: {
         id: new NumberField(),
      }
   })

   class User extends Id {
      public age: number;
      public name: string;
   }

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         id: new NumberField(),
         age: new NumberField(),
         name: new StringField(),
      }
   });

   enum EAction {
      Create,
      Update,
      Delete,
   }


   class Create {
      public type = EAction.Create;
      public data: User;
   }

   const CREATE_META = new ObjectMeta({
      builder: Create,
      isOfType: ({ type }) => type === EAction.Create,
      fields: {
         type: new NumberField(),
         data: new ObjectField({ meta: USER_META })
      }
   });


   class Update {
      public type = EAction.Update;
      public data: User;
   }

   const UPDATE_META = new ObjectMeta({
      builder: Update,
      isOfType: ({ type }) => type === EAction.Update,
      fields: {
         type: new NumberField(),
         data: new ObjectField({ meta: USER_META, isPartial: true })
      }
   });

   class Delete {
      public type = EAction.Delete;
      public data = new Id();
   }

   const DELETE_META = new ObjectMeta({
      builder: Delete,
      isOfType: ({ type }) => type === EAction.Delete,
      fields: {
         type: new NumberField(),
         data: new ObjectField({ meta: ID_META })
      }
   });

   const values = [
      {
         type: EAction.Create,
         data: {
            id: 1,
            name: `Vasya`,
            age: 45,
         }
      },
      {
         type: EAction.Update,
         data: {
            id: 1,
            age: 12,
         }
      }
   ]


   console.log(USER_META);

})
