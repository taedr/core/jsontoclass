import { AField, IFieldCtx, JTC, NumberField, ObjectArrayMeta, ObjectMeta, StringField } from "@taedr/jsontoclass"

const APP_INFO = Symbol(`APP_INFO`);

class AppInfo {
   public isSelected: boolean;

   constructor(
      public id: number,
   ) { }
}

class User {
   public id: number;
   public name: string;

   public [APP_INFO]: AppInfo;
}

const USER_META = new ObjectMeta<User>({
   builder: User,
   fields: {
      id: new NumberField(),
      name: new StringField(),
      [APP_INFO]: ({ id }) => new AppInfo(id),
   }
});

const values = [
   {
      id: 1,
      name: `Vasya`,
   },
   {
      id: 2,
      name: `Masha`,
   },
];


it(`INIT`, () => {
   const result = JTC.convert({
      id: `Symbols`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values
   });

   const users = result.converted.all;

   console.log(users);

   console.log(users[0][APP_INFO].id === 1); // true
   console.log(users[1][APP_INFO].id === 2); // true
});

it(`SHARE`, () => {
   const result_1 = JTC.convert({
      id: `Symbols Share 1`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values,
   });

   const users_1 = result_1.converted.all;

   const result_2 = JTC.convert({
      id: `Symbols Share 1`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values: users_1,
      SSymbols: `SHARE`
   });

   const users_2 = result_2.converted.all;

   expect(result_1.isCorrupted).toBeFalsy();
   expect(result_2.isCorrupted).toBeFalsy();

   for (let i = 0; i < values.length; i++) {
      const user_1 = users_1[i];
      const user_2 = users_2[i];
      console.log(user_1[APP_INFO] === user_2[APP_INFO]); // true

      expect(user_1[APP_INFO]).toBe(user_2[APP_INFO]);
   }
});
