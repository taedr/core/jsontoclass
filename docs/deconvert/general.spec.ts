import { FunctionField, JTC, NumberField, ObjectArrayMeta, ObjectMeta, StringDateField, StringField } from "@taedr/jsontoclass";

it(`Doc - Deconvert - General`, () => {
   const APP_INFO = Symbol(`APP_INFO`);

   class AppInfo {
      public isSelected = false;
   }

   class User {
      public id: number;
      public name: string;
      public dob: Date;

      public get short() { return `${this.id} - ${this.name}`; }

      public sayHi() {
         console.log(`Hi`);
      }

      public [APP_INFO]: AppInfo;
   }

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         id: new NumberField(),
         name: new StringField(),
         dob: new StringDateField({ deconvert: date => date.toISOString() }),
         short: new StringField({ isCalculated: true }),
         sayHi: new FunctionField(),
         [APP_INFO]: () => new AppInfo(),
      }
   });

   const values = [
      {
         id: 1,
         name: `Vasya`,
         dob: `2021-04-11T15:38:20.371Z`,
      },
   ];

   const [user] = JTC.convert({
      id: `Deconvert`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values
   }).converted.all;


   const deconverted = JTC.deconvert({ value: user });

   console.log(user);
   console.log(deconverted);
   console.log(JSON.stringify(values[0]) === JSON.stringify(deconverted)); // true

   expect(deconverted).toEqual(values[0]);
});
