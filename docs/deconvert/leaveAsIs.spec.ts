import { JTC, NumberField, ObjectArrayMeta, ObjectField, ObjectMeta, StringField } from "@taedr/jsontoclass";


it(`Doc - Deconvert - Leave as is`, () => {
   class Coords {
      public lat: number;
      public lng: number;
   }

   const COORDS_META = new ObjectMeta({
      builder: Coords,
      fields: {
         lat: new NumberField(),
         lng: new NumberField(),
      }
   });

   class User {
      public id: number;
      public name: string;
      public coords: Coords;
   }

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         id: new NumberField(),
         name: new StringField({ isNullable: true }),
         coords: new ObjectField({ meta: COORDS_META }),
      }
   });

   const values = [
      {
         id: 1,
         name: `Vasya`,
         coords: {
            lng: 0,
            lat: 0,
         }
      },
   ];

   const result = JTC.convert({
      id: `Deconvert`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values,
   });
   const [user] = result.converted.all;
   const deconverted = JTC.deconvert({ value: user, leaveAsIs: [Coords] });

   console.log(user);
   console.log(deconverted);

   expect(deconverted['coords']).toBe(user.coords);
});
