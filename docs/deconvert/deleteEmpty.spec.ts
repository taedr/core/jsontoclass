import { BooleanField, JTC, NumberArrayField, NumberField, ObjectArrayMeta, ObjectMeta, StringField } from "@taedr/jsontoclass";


it(`Doc - Deconvert - Delete empty`, () => {
   class User {
      public id: number;
      public name: string;
      public isAlive: boolean;
      public marks: number[] = [];
   }

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         id: new NumberField(),
         name: new StringField({ isNullable: true }),
         isAlive: new BooleanField({ isNullable: true }),
         marks: new NumberArrayField({ isNullable: true }),
      }
   });


   const values = [
      {
         id: 1,
         name: `Vasya`,
         isAlive: false,
      },
      {
         id: 2,
         name: ``,
         isAlive: true,
         marks: [1]
      },
      {
         id: 3,
         name: `Masha`,
         marks: [],
         isAlive: null,
      }
   ];


   const result = JTC.convert({
      id: `Deconvert`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values,
   });
   const users = result.converted.all;
   const deconverted = users.map(value => JTC.deconvert({ value, isDeleteEmpty: true }));

   console.log(users);
   console.log(deconverted);

   expect(deconverted[0]['isAlive']).toBeUndefined();
   expect(deconverted[1]['name']).toBeUndefined();
   expect(deconverted[2]['marks']).toBeUndefined();
   expect(deconverted[2]['isAlive']).toBeUndefined();
});
