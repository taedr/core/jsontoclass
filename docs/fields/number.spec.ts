import { BigintField, JTC, NumberField, ObjectArrayMeta, ObjectMeta, VALIDATORS } from "@taedr/jsontoclass";

it(`Creation`, () => {
   class User {
      public id: bigint;
      public age: number;
   }

   const ageField = new NumberField();
   const idField = new BigintField();

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         id: idField,
         age: ageField
      }
   });

   const values = [
      {
         id: BigInt(1),
         age: 43
      },
      {
         id: 12,
         age: ``
      }
   ];

   const result = JTC.convert({
      id: `Number`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values,
   });

   const log = JTC.log.asString(result.corruption);

   console.log(result.converted.all);

   /* 1 (EXCLUDED) -> {...} | Validation failed for all fields
      1 (EXCLUDED) -> age -> '' | Expected number, but got string */
   console.log(log);

});


it(`Min/Max`, () => {
   class User {
      public id: bigint;
      public age: number;
   }

   const ageField = new NumberField({
      validators: [
         VALIDATORS.min.number(18),
         VALIDATORS.max.number(100),
      ]
   });
   const idField = new BigintField({
      validators: [
         VALIDATORS.min.number(0),
         VALIDATORS.max.number(1000),
      ]
   });

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         id: idField,
         age: ageField
      }
   });

   const values = [
      {
         id: BigInt(1),
         age: 22
      },
      {
         id: BigInt(-1),
         age: 222
      },
      {
         id: BigInt(1001),
         age: 11
      }
   ];

   const result = JTC.convert({
      id: `Min/Max`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values,
   });

   const validation = ageField.validate({ value: 1 });
   const log = JTC.log.asString(result.corruption);

   console.log(result.converted.all);

   /* 1 -> age -> 222 | Bigger than expected(100)
      1 -> id -> -1 | Smaller than expected(0)
      2 -> age -> 11 | Smaller than expected(18)
      2 -> id -> 1001 | Bigger than expected(1000) */
   console.log(log);
   /* [ { id: 'minNumber', message: 'Smaller than 18', value: 1 } ] */
   console.log(validation.errors);
   /* true */
   console.log(validation.isInvalid);

   expect(log).toEqual([
      `1 -> age -> 222 | Bigger than expected(100)`,
      `1 -> id -> -1 | Smaller than expected(0)`,
      `2 -> age -> 11 | Smaller than expected(18)`,
      `2 -> id -> 1001 | Bigger than expected(1000)`
   ].join(`\n`));
   expect(validation.errors).toEqual([{ id: 'minNumber', message: 'Smaller than expected(18)', value: 1 }]);
   expect(result.converted.all).toEqual(values);
   expect(result.converted.valid).toEqual([0].map(i => values[i]));
   expect(result.converted.corrupted).toEqual([1, 2].map(i => values[i]));

});
