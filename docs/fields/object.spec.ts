import { JTC, ObjectArrayMeta, ObjectField, ObjectMeta, StringField } from "@taedr/jsontoclass";

it(`Creation`, () => {
   class User {
      public id: string;
      public address: Address;
   }

   class Address {
      public country: string;
   }

   const ADDRESS_META = new ObjectMeta({
      builder: Address,
      fields: {
         country: new StringField(),
      },
   });

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         id: new StringField(),
         address: new ObjectField({ meta: ADDRESS_META })
      }
   });

   const values = [
      {
         id: `1`,
         address: {
            country: `Ukraine`
         }
      },
      {
         id: `2`,
         address: 111
      }
   ]

   const result = JTC.convert({
      id: `Creation`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values,
   });
   const log = JTC.log.asString(result.corruption);

   /* 1 -> address -> 111 | Expected object, but got number */
   console.log(log);

   expect(result.isCorrupted).toBeTruthy();
   expect(log).toEqual(`1 -> address -> 111 | Expected object, but got number`);
});
