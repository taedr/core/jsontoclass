import { JTC, ObjectArrayMeta, ObjectMeta, StringField, UnknownField } from "@taedr/jsontoclass";

it(`Creation`, () => {
   class User {
      public id: string;
      public something: unknown;
   }

   const somethingField = new UnknownField();

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         id: new StringField(),
         something: somethingField,
      }
   });

   const values = [
      {
         id: `1`,
      },
      {
         id: `2`,
         something: 4
      },
      {
         id: `3`,
         something: { one: 1 }
      },
   ];

   const result = JTC.convert({
      id: `Boolean Truthy`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values,
   });

   const log = JTC.log.asString(result.corruption);

   console.log(result.converted.all);

   /* 0 -> something -> undefined | Value is required*/
   console.log(log);

   expect(log).toEqual([
      `0 -> something -> undefined | Value is required`
   ].join(`\n`));
   expect(result.converted.all).toEqual(values);
   expect(result.converted.valid).toEqual([1, 2].map(i => values[i]));
   expect(result.converted.corrupted).toEqual([0].map(i => values[i]));

});
