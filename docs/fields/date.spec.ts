import { JTC, MillisDateField, ObjectArrayMeta, ObjectMeta, SecondsDateField, StringDateField, VALIDATORS } from "@taedr/jsontoclass";

it(`Creation`, () => {
   class User {
      public dob: Date;
   }

   const date = `2021-06-07T15:12:29.140Z`;
   const max = `2021-06-07T15:12:30.140Z`;
   const min = `2021-06-07T15:12:28.140Z`;

   const dobField = new StringDateField({
      deconvert: _date => _date.toISOString(),
      validators: [
         VALIDATORS.min.date(new Date(min)),
         VALIDATORS.max.date(new Date(max)),
      ]
   });

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         dob: dobField
      }
   });

   const values = [
      {
         dob: date,
      },
      {
         dob: `aaacccc`
      },
      {
         dob: 12
      },
      {
         dob: new Date(+(new Date(max)) + 1000).toISOString(),
      },
      {
         dob: new Date(+(new Date(min)) - 1000).toISOString(),
      }
   ];

   const result = JTC.convert({
      id: `Date Strings`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values,
   });

   const laterThanMax = new Date(new Date(max).getTime() + 1)
   const validation = dobField.validate({ value: laterThanMax });
   const log = JTC.log.asString(result.corruption);
   const [user] = result.converted.all;
   const deconverted = JTC.deconvert({ value: user });

   console.log(user);
   /* { dob: '2021-06-07T15:12:29.140Z' } */
   console.log(deconverted);

   /* 1 (EXCLUDED) -> {...} | Validation failed for all fields
      1 (EXCLUDED) -> dob -> aaacccc | Can't create Date from such value
      2 (EXCLUDED) -> {...} | Validation failed for all fields
      2 (EXCLUDED) -> dob -> 12 | Expected string, but got number
      3 (1) -> dob -> 2021-06-07T15:12:31.140Z | Later than 2021-06-07T15:12:30.140Z
      4 (2) -> dob -> 2021-06-07T15:12:27.140Z | Earlier than 2021-06-07T15:12:28.140Z*/
   console.log(log);
   /* [ {
         id: 'maxDate',
         message: 'Later than 2021-06-07T15:12:30.140Z',
         value: Mon Jun 07 2021 18:12:30 GMT+0300 (Восточная Европа, летнее время)
      } ] */
   console.log(validation.errors);
   /* true */
   console.log(validation.isInvalid);

   result.converted.valid //?

   expect(log).toEqual([
      `1 (EXCLUDED) -> {...} | Validation failed for all fields`,
      `1 (EXCLUDED) -> dob -> aaacccc | Can't create Date from such value`,
      `2 (EXCLUDED) -> {...} | Validation failed for all fields`,
      `2 (EXCLUDED) -> dob -> 12 | Expected string, but got number`,
      `3 (1) -> dob -> 2021-06-07T15:12:31.140Z | Later than 2021-06-07T15:12:30.140Z`,
      `4 (2) -> dob -> 2021-06-07T15:12:27.140Z | Earlier than 2021-06-07T15:12:28.140Z`,
   ].join(`\n`));
   expect(validation.errors).toEqual([{
      id: 'maxDate',
      message: 'Later than 2021-06-07T15:12:30.140Z',
      value: laterThanMax
   }]);
   expect(deconverted).toEqual(values[0]);
   expect(result.origin.all[0]).toEqual(deconverted);
   expect(result.origin.corrupted).toEqual([1, 2, 3, 4].map(i => values[i]));
});

it(`Millis`, () => {
   class User {
      public dob: Date;
   }

   const date = new Date(`2021-06-07T15:12:29.140Z`).getTime();
   const max = date + 1000;
   const min = date - 1000;

   const dobField = new MillisDateField({
      validators: [
         VALIDATORS.min.date(new Date(min)),
         VALIDATORS.max.date(new Date(max)),
      ]
   });

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         dob: dobField
      }
   });

   const values = [
      {
         dob: date,
      },
      {
         dob: `aaacccc`
      },
      {
         dob: new Date(max + 1).getTime(),
      },
      {
         dob: new Date(min - 1).getTime(),
      }
   ];

   const result = JTC.convert({
      id: `Date Millis`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values,
   });

   const laterThanMax = new Date(max * 1000 + 1);
   const validation = dobField.validate({ value: laterThanMax });
   const log = JTC.log.asString(result.corruption);
   const [user] = result.converted.all;
   const deconverted = JTC.deconvert({ value: user });

   console.log(user);
   /* { dob: 1623078749140 } */
   console.log(deconverted);

   /* 1 (EXCLUDED) -> {...} | Validation failed for all fields
      1 (EXCLUDED) -> dob -> aaacccc | Expected number, but got string
      2 (1) -> dob -> 2021-06-07T15:12:30.141Z | Later than 2021-06-07T15:12:30.140Z
      3 (2) -> dob -> 2021-06-07T15:12:28.139Z | Earlier than 2021-06-07T15:12:28.140Z*/
   console.log(log);
   /* [ {   id: 'maxDate',
            message: 'Later than 2021-06-07T15:12:30.140Z',
            value: Wed Apr 27 53403 19:22:20 GMT+0300 (Восточная Европа, летнее время) } ] */
   console.log(validation.errors);
   /* true */
   console.log(validation.isInvalid);

   result.converted.valid //?

   expect(log).toEqual([
      `1 (EXCLUDED) -> {...} | Validation failed for all fields`,
      `1 (EXCLUDED) -> dob -> aaacccc | Expected number, but got string`,
      `2 (1) -> dob -> 2021-06-07T15:12:30.141Z | Later than 2021-06-07T15:12:30.140Z`,
      `3 (2) -> dob -> 2021-06-07T15:12:28.139Z | Earlier than 2021-06-07T15:12:28.140Z`,
   ].join(`\n`));
   expect(validation.errors).toEqual([{ id: 'maxDate', message: 'Later than 2021-06-07T15:12:30.140Z', value: laterThanMax }]);
   expect(deconverted).toEqual(values[0]);
   expect(result.origin.corrupted).toEqual([1, 2, 3].map(i => values[i]));
});

it(`Seconds`, () => {
   class User {
      public dob: Date;
   }

   const date = Math.floor(new Date(`2021-06-07T15:12:29.140Z`).getTime() / 1000);
   const max = date + 1;
   const min = date - 1;

   const dobField = new SecondsDateField({
      validators: [
         VALIDATORS.min.date(new Date(min * 1000)),
         VALIDATORS.max.date(new Date(max * 1000)),
      ]
   });

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         dob: dobField
      }
   });

   const values = [
      {
         dob: date,
      },
      {
         dob: `aaacccc`
      },
      {
         dob: new Date(max + 1).getTime(),
      },
      {
         dob: new Date(min - 1).getTime(),
      }
   ];

   const result = JTC.convert({
      id: `Date Seconds`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values,
   });

   const laterThanMax = new Date(max * 1000 + 1);
   const validation = dobField.validate({ value: laterThanMax });
   const log = JTC.log.asString(result.corruption);
   const [user] = result.converted.all;
   const deconverted = JTC.deconvert({ value: user });

   console.log(user);
   /* { dob: 1623078749 } */
   console.log(deconverted);

   /* 1 (EXCLUDED) -> {...} | Validation failed for all fields
      1 (EXCLUDED) -> dob -> aaacccc | Expected number, but got string
      2 (1) -> dob -> 1970-01-19T18:51:20.000Z | Later than 1970-01-01T00:27:03.079Z
      3 (2) -> dob -> 1970-01-19T18:51:16.000Z | Later than 1970-01-01T00:27:03.079Z*/
   console.log(log);
   /* [ { id: 'maxDate',
         message: 'Later than 2021-06-07T15:12:30.000Z',
         value: Mon Jun 07 2021 18:12:30 GMT+0300 (Восточная Европа, летнее время) } ] */
   console.log(validation.errors);
   /* true */
   console.log(validation.isInvalid);

   result.converted.valid //?

   expect(log).toEqual([
      `1 (EXCLUDED) -> {...} | Validation failed for all fields`,
      `1 (EXCLUDED) -> dob -> aaacccc | Expected number, but got string`,
      `2 (1) -> dob -> 2021-06-07T15:12:31.000Z | Later than 2021-06-07T15:12:30.000Z`,
      `3 (2) -> dob -> 2021-06-07T15:12:27.000Z | Earlier than 2021-06-07T15:12:28.000Z`,
   ].join(`\n`));
   expect(validation.errors).toEqual([{ id: 'maxDate', message: 'Later than 2021-06-07T15:12:30.000Z', value: laterThanMax }]);
   expect(deconverted).toEqual(values[0]);
   expect(result.origin.corrupted).toEqual([1, 2, 3].map(i => values[i]));
});
