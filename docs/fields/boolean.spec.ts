import { BooleanField, JTC, ObjectArrayMeta, ObjectMeta, VALIDATORS } from "@taedr/jsontoclass";

it(`Creation`, () => {
   class User {
      public isAdmin: boolean;
   }

   const isAdminField = new BooleanField();

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         isAdmin: isAdminField,
      }
   });

   const values = [
      {
         isAdmin: true,
      },
      {
         isAdmin: 0,
      }
   ];

   const result = JTC.convert({
      id: `Boolean`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values,
   });

   const log = JTC.log.asString(result.corruption);

   console.log(result.converted.all);

   /* 1 (EXCLUDED) -> {...} | Validation failed for all fields
      1 (EXCLUDED) -> isAdmin -> 0 | Expected boolean, but got number */
   console.log(log);
});


it(`Truthy/Falsy`, () => {
   class User {
      public isAdmin: boolean;
      public isAlive: boolean;
   }

   const isAdminField = new BooleanField({
      validators: [
         VALIDATORS.falsy(),
      ]
   });
   const isAliveField = new BooleanField({
      validators: [
         VALIDATORS.truthy(),
      ]
   });

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         isAdmin: isAdminField,
         isAlive: isAliveField
      }
   });

   const values = [
      {
         isAdmin: false,
         isAlive: true,
      },
      {
         isAdmin: true,
         isAlive: true,
      },
      {
         isAdmin: false,
         isAlive: false,
      },
      {
         isAdmin: true,
         isAlive: false,
      },
   ];

   const result = JTC.convert({
      id: `Truthy/Falsy`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values,
   });

   const validation = isAdminField.validate({ value: true });
   const log = JTC.log.asString(result.corruption);

   console.log(result.converted.all);

   /* 1 -> isAdmin -> true | Should always be falsy
      2 -> isAlive -> false | Should always be truthy
      3 -> isAdmin -> true | Should always be falsy
      3 -> isAlive -> false | Should always be truthy */
   console.log(log);
   /* [ { id: 'falsy', message: 'Should be falsy', value: true } ] */
   console.log(validation.errors);
   /* true */
   console.log(validation.isInvalid);

   expect(log).toEqual([
      `1 -> isAdmin -> true | Should be falsy`,
      `2 -> isAlive -> false | Should be truthy`,
      `3 -> isAdmin -> true | Should be falsy`,
      `3 -> isAlive -> false | Should be truthy`
   ].join(`\n`));
   expect(validation.errors).toEqual([{ id: 'falsy', message: 'Should be falsy', value: true }]);
   expect(result.converted.all).toEqual(values);
   expect(result.converted.valid).toEqual([0].map(i => values[i]));
   expect(result.converted.corrupted).toEqual([1, 2, 3].map(i => values[i]));

});
