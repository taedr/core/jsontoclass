import { JTC, ObjectArrayMeta, ObjectMeta, StringField, VALIDATORS } from "@taedr/jsontoclass";

it(`Creation`, () => {
   class User {
      public name: string;
   }

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         name: new StringField(),
      }
   });

   const values = [
      {
         name: `Vasya`
      },
      {
         name: 12
      },
   ];

   const result = JTC.convert({
      id: `String`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values,
   });

   const log = JTC.log.asString(result.corruption);

   console.log(result.converted.all);
   /* 1 (EXCLUDED) -> {...} | Validation failed for all fields
      1 (EXCLUDED) -> name -> 12 | Expected string, but got number */
   console.log(log);
});

it(`Enum`, () => {
   enum EUserType {
      admin = `admin`,
      user = `user`,
      manager = `manager`
   }

   class User {
      public type: EUserType;
   }

   const typeField = new StringField({
      validators: [
         VALIDATORS.enum(`type`, EUserType)
      ]
   });

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         type: typeField
      }
   });

   const values = [
      {
         type: EUserType.admin
      },
      {
         type: EUserType.user
      },
      {
         type: `anonymous`
      },
      {
         type: EUserType.manager
      },
   ];

   const result = JTC.convert({
      id: `Enum`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values,
   });
   const validation = typeField.validate({ value: `guest` });
   const log = JTC.log.asString(result.corruption);

   console.log(result.converted.valid);
   /* 2 -> type -> anonymous | Not present in enum: admin, user, manager */
   console.log(log);
   /* [ { id: 'type', message: 'Not present in enum: admin, user, manager', value: 'guest' } ] */
   console.log(validation.errors);
   /* true */
   console.log(validation.isInvalid);

   expect(log).toEqual(`2 -> type -> anonymous | Not present in enum: admin, user, manager`);
   expect(validation.errors).toEqual([{ id: 'type', message: 'Not present in enum: admin, user, manager', value: 'guest' }]);
   expect(result.converted.all).toEqual(values);
   expect(result.converted.valid).toEqual([0, 1, 3].map(i => values[i]));
   expect(result.converted.corrupted).toEqual([values[2]]);
});


it(`Min/max`, () => {
   class User {
      public name: string;
   }

   const nameField = new StringField({
      validators: [
         VALIDATORS.min.length(4),
         VALIDATORS.max.length(10),
      ]
   });

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         name: nameField
      }
   });

   const values = [
      {
         name: `Andrey`
      },
      {
         name: `Joi`,
      },
      {
         name: `Apollinaria`
      },
   ];

   const result = JTC.convert({
      id: `Min/max`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values,
   });
   const validation = nameField.validate({ value: `Max` });
   const log = JTC.log.asString(result.corruption);

   console.log(result.converted.valid);
   /* 1 -> name -> Joi | Length(3) is less than expected(4)
      2 -> name -> Apollinaria | Length(11) is more than expected(10) */
   console.log(log);
   /* [ { code: 5, value: 'Max', message: 'Length(3) is less than expected(4)' } ] */
   console.log(validation.errors);
   /* true */
   console.log(validation.isInvalid);

   expect(log).toEqual([
      `1 -> name -> Joi | Length(3) is less than expected(4)`,
      `2 -> name -> Apollinaria | Length(11) is more than expected(10)`
   ].join(`\n`));
   expect(result.converted.all).toEqual(values);
   expect(result.converted.valid).toEqual([0].map(i => values[i]));
   expect(result.converted.corrupted).toEqual([1, 2].map(i => values[i]));
});

it(`Pattern`, () => {
   class User {
      public email: string;
   }

   const emailMessage = `Not valid email`;
   const emailRegexp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
   const emailField = new StringField({
      validators: [
         VALIDATORS.pattern(`email`, emailRegexp, () => emailMessage)
      ]
   });

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         email: emailField
      }
   });

   const values = [
      {
         email: `test@gmail.com`
      },
      {
         email: `test.mail.com`,
      },
      {
         email: `test@mail`
      },
   ];

   const result = JTC.convert({
      id: `Pattern`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values,
   });
   const validation = emailField.validate({ value: `test@mail@for.cu` });
   const log = JTC.log.asString(result.corruption);

   console.log(result.converted.valid);
   /* 1 -> email -> test.mail.com | Not valid email */
   console.log(log);
   /* [ { id: 'email',  message: 'Not valid email', value: 'test@mail@for.cu' } ]*/
   console.log(validation.errors);
   /* true */
   console.log(validation.isInvalid);

   expect(log).toEqual([
      `1 -> email -> test.mail.com | ${emailMessage}`
   ].join(`\n`));
   expect(validation.errors).toEqual([{ id: 'email', message: emailMessage, value: 'test@mail@for.cu' }]);
   expect(result.converted.all).toEqual(values);
   expect(result.converted.all).toEqual(values);
   expect(result.converted.valid).toEqual([0, 2].map(i => values[i]));
   expect(result.converted.corrupted).toEqual([1].map(i => values[i]));
});
