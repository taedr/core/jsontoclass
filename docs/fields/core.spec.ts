import { AField, JTC, NumberField, ObjectArrayMeta, ObjectMeta, StringField, TValidator, TValidatorEntry } from "@taedr/jsontoclass";

it(`Creation`, () => {
   function oddValidator(value: number) {
      if (value % 2 === 0) return; // #Return#
      return `Odd number`;
   }

   const field: AField<number> = new NumberField({
      /* Marks field as getter which should not be expected in data during conversion */
      isCalculated: false,
      /* Marks that field can have null or undefined as value
         and this should NOT be considered as corruption*/
      isNullable: false,
      /* Validates provided value based on field validation settings,
         in case of validation errors their descriptions can be found under 'errors' key */
      validators: [
         [oddValidator.name, oddValidator]
      ]
   });

   const odd = field.validate({ value: 1 });
   console.log(odd.errors); //  [ { id: 'oddValidator', message: 'Odd number', value: 1 } ]
   const even = field.validate({ value: 2 });
   console.log(even.errors); // []

   expect(odd.errors).toEqual([{ id: 'oddValidator', message: 'Odd number', value: 1 }]);
   expect(even.errors).toEqual([]);
});


it(`Calculated`, () => {
   class User {
      public id: number;
      public name: string;
      public get formatted() { return `${this.id} | ${this.name}`; }
   }

   const formattedField = new StringField({ isCalculated: true });
   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         id: new NumberField(),
         name: new StringField(),
         formatted: formattedField,
      }
   });

   const users = [
      {
         id: 1,
         name: `Masha`,
      }
   ];

   const result = JTC.convert({
      id: `Calculated`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values: users
   });

   /* true */
   console.log(formattedField.isCalculated);
   /* false */
   console.log(result.isCorrupted);
   /* 1 | Masha */
   console.log(result.converted.all[0].formatted);

   expect(result.isCorrupted).toBeFalsy();
   expect(result.converted.all[0].formatted).toEqual(`1 | Masha`);
});

it(`Nullable`, () => {
   enum ERole {
      user = `user`,
      admin = `admin`,
   }

   class User {
      public id: number;
      public name?: string;
      public age?: number = null;
      public role: ERole = ERole.user;
   }

   const roleField = new StringField({ isNullable: true });
   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         id: new NumberField(),
         name: new StringField({ isNullable: true }),
         age: new NumberField({ isNullable: true }),
         role: roleField,
      }
   });

   const users = [
      {
         id: 1,
         name: `Masha`,
      },
      {
         id: 2,
         age: 21
      },
      {
         id: 3,
         name: null,
         role: ERole.admin,
      },
   ];

   const result = JTC.convert({
      id: `Nullable`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values: users
   });

   /* true */
   console.log(roleField.isNullable);
   /* false */
   console.log(result.isCorrupted);
   /* { id: 1, name: 'Masha', age: null, role: 'user' } */
   console.log(result.converted.all[0]);
   /* { id: 2, age: 21, role: 'user' } */
   console.log(result.converted.all[1]);
   /* { id: 3, age: null, role: 'admin' } */
   console.log(result.converted.all[2]);

   expect(result.isCorrupted).toBeFalsy();
   expect(result.converted.all[0].name).toEqual(`Masha`);
   expect(result.converted.all[1].name).toBeUndefined();
   expect(result.converted.all[1].name).toBeUndefined();
});

it(`Validators`, () => {
   class User {
      public id: number;
      public name: string;
   }

   function digitsCountValidator(count: number): TValidatorEntry<number> {
      return [digitsCountValidator.name, value => {
         if (`${value}`.length === count) return;
         return `Should consist of ${count} digits`;
      }];
   }

   function wordsCountValidator(count: number): TValidatorEntry<string> {
      return [wordsCountValidator.name, value => {
         if (value.split(` `).length === count) return;
         return `Should consist of ${count} words`;
      }];
   }

   const idField = new NumberField({
      validators: [
         digitsCountValidator(6)
      ]
   });
   const nameField = new StringField({
      validators: [
         wordsCountValidator(3),
      ]
   });
   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         id: idField,
         name: nameField,
      }
   });

   const users = [
      {
         id: 1,
         name: `Masha`,
      },
      {
         id: 222222,
         name: `Petya`,
      },
      {
         id: 3333,
         name: `Petrov Eugene Vasilievich`,
      },
      {
         id: 444444,
         name: `Holubiev Andrii Mykolayovych`,
      }
   ];

   const result = JTC.convert({
      id: `Calculated`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values: users
   });

   const log = JTC.log.asString(result.corruption);

   /* true */
   console.log(result.isCorrupted);
   /* 0 -> id -> 1 | Should consist of 6 digits
      0 -> name -> Masha | Should consist of 3 words
      1 -> name -> Petya | Should consist of 3 words
      2 -> id -> 3333 | Should consist of 6 digits
   */
   console.log(log);

   const idErrors = idField.validate({ value: 1 }).errors;
   const nameErrors = nameField.validate({ value: `Masha` }).errors;

   /*  [ { id: 'digitsCountValidator', message: 'Should consist of 6 digits', value: 1 } ] */
   console.log(idErrors);
   /*  [ { id: 'wordsCountValidator', message: 'Should consist of 3 words', value: 'Masha' } ] */
   console.log(nameErrors);

   expect(log).toEqual([
      `0 -> id -> 1 | Should consist of 6 digits`,
      `0 -> name -> Masha | Should consist of 3 words`,
      `1 -> name -> Petya | Should consist of 3 words`,
      `2 -> id -> 3333 | Should consist of 6 digits`,
   ].join(`\n`));
   expect(idErrors).toEqual([{ id: 'digitsCountValidator', message: 'Should consist of 6 digits', value: 1 }]);
   expect(nameErrors).toEqual([{ id: 'wordsCountValidator', message: 'Should consist of 3 words', value: 'Masha' }]);
   expect(result.converted.valid.length).toEqual(1);
   expect(result.converted.corrupted.length).toEqual(3);
});
