import { BigintArrayField, BigintArrayMeta, BooleanArrayField, BooleanArrayMeta, DimensionalArrayField, DimensionalArrayMeta, JTC, MapArrayField, MapArrayMeta, NumberArrayField, NumberArrayMeta, NumberField, NumberMapMeta, ObjectArrayField, ObjectArrayMeta, ObjectMeta, StringArrayField, StringArrayMeta, UnknownArrayField, UnknownArrayMeta, VALIDATORS } from "@taedr/jsontoclass";

describe(`Doc - Meta - Fields - Array`, () => {
   it(`Creation`, () => {
      class User {
         public numbers: number[];
      }

      const numbersField = new NumberArrayField();
      const USER_META = new ObjectMeta({
         builder: User,
         fields: {
            numbers: numbersField
         }
      });

      const values = [
         {
            numbers: [1, 2, 3, 4, 5]
         },
         {
            numbers: {}
         }
      ];

      const result = JTC.convert({
         id: `Array`,
         meta: new ObjectArrayMeta({ meta: USER_META }),
         values,
      });
      const log = JTC.log.asString(result.corruption);

      /* 1 (EXCLUDED) -> {...} | Validation failed for all fields
         1 (EXCLUDED) -> numbers -> {...} | Expected array, but got object
      */
      console.log(log);

      expect(result.isCorrupted).toBeTruthy();
      expect(result.converted.all[0]).toEqual(values[0]);
   });


   it(`Validation`, () => {
      class User {
         public numbers: number[];
      }

      const numbersField = new NumberArrayField({
         validators: [
            /* Minimal array size */
            VALIDATORS.min.size(2),
            /* Maximum array size */
            VALIDATORS.max.size(3),
         ],
         /* Entry validation config */
         entry: {
            isNullable: true,
            validators: [
               VALIDATORS.min.number(2),
               VALIDATORS.max.number(3),
            ]
         }
      });

      const USER_META = new ObjectMeta({
         builder: User,
         fields: {
            numbers: numbersField
         }
      });

      const values = [
         {
            numbers: [1, 2, 3, null, 4, 5]
         }
      ];

      const result = JTC.convert({
         id: `Number Array`,
         meta: new ObjectArrayMeta({ meta: USER_META }),
         values,
      });

      const log = JTC.log.asString(result.corruption);

      /* 0 -> numbers -> 0 -> 1 | Smaller than expected(2)
         0 -> numbers -> 4 -> 4 | Bigger than expected(3)
         0 -> numbers -> 5 -> 5 | Bigger than expected(3)
         0 -> numbers -> INSTANCE -> [...] | Size(6) is more than expected(3)
      */
      console.log(log);


      expect(log).toEqual([
         `0 -> numbers -> 0 -> 1 | Smaller than expected(2)`,
         `0 -> numbers -> 4 -> 4 | Bigger than expected(3)`,
         `0 -> numbers -> 5 -> 5 | Bigger than expected(3)`,
         `0 -> numbers -> INSTANCE -> [...] | Size(6) is more than expected(3)`,
      ].join(`\n`));
      expect(result.isCorrupted).toBeTruthy();
   });

   it(`Fields types`, () => {
      /* Array of numbers - [1, 2, 3] */
      const number = new NumberArrayField();
      /* Array of strings - ['a', 'b', 'c'] */
      const string = new StringArrayField();
      /* Array of booleans - [true, false, true] */
      const boolean = new BooleanArrayField();
      /* Array of bigints - [1n, 2n, 3n] */
      const bigint = new BigintArrayField();
      /* Array of unknowns - [1, 'a, false] */
      const unknown = new UnknownArrayField();
      /* Array of user class objects - [{id: 1}, {id: 2}] */
      class User {
         public id: number;
      }

      const USER_META = new ObjectMeta({
         builder: User,
         fields: {
            id: new NumberField(),
         }
      });

      const object = new ObjectArrayField({ meta: USER_META });
      /* Array of arrays. Currently numbers -
         [
            [1, 2, 3],
            [4, 5, 6],
         ]
      */
      const array = new DimensionalArrayField({
         meta: new NumberArrayMeta(),
      });
      /* Array of maps. Currently numbers -
         [
            {
               one: 1,
               two: 2
            },
            {
               three: 3,
               four: 4,
            }
         ]
      */
      const map = new MapArrayField({
         entry: {
            meta: new NumberMapMeta(),
         }
      });
   });


});

describe(`Meta types`, () => {
   it(`Number`, () => {
      const values = [1, 2, 3];
      const numbers = JTC.convert({
         id: `Number Array`,
         meta: new NumberArrayMeta(),
         values,
      });

      /* false */
      console.log(numbers.isCorrupted);
      /* [1, 2, 3] */
      console.log(numbers.converted.all);

      expect(numbers.isCorrupted).toBeFalsy();
      expect(numbers.converted.valid).toEqual(values);
   });

   it(`String`, () => {
      const values = [`a`, `b`, `c`];
      const strings = JTC.convert({
         id: `String Array`,
         meta: new StringArrayMeta(),
         values,
      });

      /* false */
      console.log(strings.isCorrupted);
      /* [`a`, `b`, `c`] */
      console.log(strings.converted.all);

      expect(strings.isCorrupted).toBeFalsy();
      expect(strings.converted.valid).toEqual(values);
   });

   it(`Boolean`, () => {
      const values = [true, false, true];
      const booleans = JTC.convert({
         id: `Boolean Array`,
         meta: new BooleanArrayMeta(),
         values,
      });

      /* false */
      console.log(booleans.isCorrupted);
      /* [true, false, true] */
      console.log(booleans.converted.all);

      expect(booleans.isCorrupted).toBeFalsy();
      expect(booleans.converted.valid).toEqual(values);
   });

   it(`Bigint`, () => {
      const values = [1n, 2n, 3n];
      const bigints = JTC.convert({
         id: `Bigint Array`,
         meta: new BigintArrayMeta(),
         values,
      });

      /* false */
      console.log(bigints.isCorrupted);
      /* [1, 2, 3] */
      console.log(bigints.converted.all);

      expect(bigints.isCorrupted).toBeFalsy();
      expect(bigints.converted.valid).toEqual(values);
   });

   it(`Unknown`, () => {
      const values = [1, true, `a`];
      const unknowns = JTC.convert({
         id: `Unknown Array`,
         meta: new UnknownArrayMeta(),
         values,
      });

      /* false */
      console.log(unknowns.isCorrupted);
      /* [1, true, 'a'] */
      console.log(unknowns.converted.all);

      expect(unknowns.isCorrupted).toBeFalsy();
      expect(unknowns.converted.valid).toEqual(values);
   });

   it(`Object`, () => {
      class User {
         public id: number;
      }

      const USER_META = new ObjectMeta({
         builder: User,
         fields: {
            id: new NumberField()
         }
      });

      const values = [
         { id: 1, }
      ];

      const objects = JTC.convert({
         id: `Objects Array`,
         meta: new ObjectArrayMeta({ meta: USER_META }),
         values,
      });

      /* false */
      console.log(objects.isCorrupted);
      /* {id: 1} */
      console.log(objects.converted.all);

      expect(objects.isCorrupted).toBeFalsy();
      expect(objects.converted.valid).toEqual(values);
   });

   it(`Array`, () => {
      const values = [
         [1, 2, 3],
         [4, 5, 6],
      ];
      const arrays = JTC.convert({
         id: `Dimensional array`,
         meta: new DimensionalArrayMeta({
            meta: new NumberArrayMeta(),
         }),
         values,
      });

      /* false */
      console.log(arrays.isCorrupted);
      /*  [ [ 1, 2, 3 ], [ 4, 5, 6 ] ] */
      console.log(arrays.converted.all);

      expect(arrays.isCorrupted).toBeFalsy();
      expect(arrays.converted.valid).toEqual(values);
   });

   it(`Map`, () => {
      const values = [
         { one: 1, two: 2, },
         { three: 3, four: 4, },
      ];
      const maps = JTC.convert({
         id: `Dimensional array`,
         meta: new MapArrayMeta({
            meta: new NumberArrayMeta(),
         }),
         values,
      });

      /* false */
      console.log(maps.isCorrupted);
      /*  [ [ 1, 2, 3 ], [ 4, 5, 6 ] ] */
      console.log(maps.converted.all);

      expect(maps.isCorrupted).toBeFalsy();
      expect(maps.converted.valid).toEqual(values);
   });
});
