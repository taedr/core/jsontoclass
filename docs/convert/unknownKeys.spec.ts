import { JTC, NumberField, ObjectArrayMeta, ObjectMeta, StringField } from '@taedr/jsontoclass';

class User {
   public id: number;
   public name: string;
}

const USER_META = new ObjectMeta({
   builder: User,
   fields: {
      id: new NumberField(),
      name: new StringField(),
   }
});

const users = [
   { id: 1, name: `Vasya`, age: 24 },
   { id: 2, name: `Katya`, phone: `3809566624` },
   { id: 3, name: `Masha` },
];

it(`IGNORE`, () => {
   const result = JTC.convert({
      id: `Users`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values: users,
      SUnknownKey: `IGNORE`,
   });

   console.log(result.isCorrupted);    // false
   console.log(result.converted.all);

   expect(result.isCorrupted).toBeFalsy();
});


it(`ERROR`, () => {
   const result = JTC.convert({
      id: `Users`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values: users,
      SUnknownKey: `ERROR`
   });

   console.log(result.isCorrupted);    // true
   console.log(result.converted.valid);
   console.log(result.converted.corrupted)


   expect(result.isCorrupted).toBeTruthy();
});


it(`SAVE`, () => {
   const result = JTC.convert({
      id: `Users`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values: users,
      SUnknownKey: `SAVE`
   });

   console.log(result.isCorrupted);    // false
   console.log(result.converted.all);

   expect(result.isCorrupted).toBeFalsy();
});
