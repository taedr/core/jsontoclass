import { JTC, NumberField, ObjectArrayMeta, ObjectMeta, StringField } from '@taedr/jsontoclass';


it(`Doc - convert - result`, () => {
   class User {
      public id: number;
      public name: string;
   }

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         id: new NumberField(),
         name: new StringField(),
      }
   });

   const users = [
      { id: 1, name: `Vasya` },
      { id: `2`, name: 132 },
      null,
      { id: 3, name: `Petya` },
      { id: `3`, name: `Masha` },
   ];

   const result = JTC.convert({
      id: `Users`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values: users,
   });


   console.log(result);

   // Array of all converted objects
   console.log(result.converted.all);
   // Array of converted objects, where all fields passed validation
   console.log(result.converted.valid);
   // Array of converted objects, where one or more fields failed to validation
   console.log(result.converted.corrupted);
   // Boolean that will be 'true' if validation failed for one or more fields
   console.log(result.isCorrupted);
   // Info about convertation, which can be used to build log with "JTC.log.asString"  and "JTC.logToConsole"
   console.log(result.corruption);
   // Array of initial values, same as provided in "values"
   console.log(result.origin.all);
   // Array of initial values, where one or more field validation failed
   console.log(result.origin.corrupted);
   // Array of initial values, which didn't match any expected meta
   console.log(result.origin.excluded);


   expect(result.isCorrupted).toBeTruthy();
   expect(users[0]).toEqual(result.converted.valid[0]);
   expect(users[1]).toEqual(result.origin.corrupted[0]);
   expect(users[3]).toEqual(result.converted.valid[1]);
   expect(result.converted.valid).toHaveLength(2);
   expect(result.converted.corrupted).toHaveLength(1);
   expect(result.origin.all).toHaveLength(5);
   expect(result.origin.corrupted).toHaveLength(2);
   expect(result.origin.excluded).toHaveLength(2);
});
