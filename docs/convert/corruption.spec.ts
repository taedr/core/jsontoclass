import { JTC, NumberField, ObjectArrayMeta, ObjectMapField, ObjectMeta, StringField } from '@taedr/jsontoclass';


it(`Object`, () => {
   class User {
      public id: number;
      public name: string;
      public age: number;
   }

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         id: new NumberField(),
         age: new NumberField(),
         name: new StringField(),
      }
   });

   const users = [
      {
         id: 1,
         name: `Vasya`,
         age: 43,
      },
      {
         id: `2`,
         name: `Petya`,
         age: 54,
      },
      {
         id: 3,
         name: true,
         age: `Masha`,
      },
   ];

   const result = JTC.convert({
      id: `Users`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values: users,
   });

   console.log(result.converted.all);
   console.log(result.converted.corrupted);
   console.log(result.converted.valid);

   /*
   1 -> id -> 2 | Expected number, but got string
   2 -> age -> Masha | Expected number, but got string
   2 -> name -> true | Expected string, but got boolean
   */
   console.log(JTC.log.asString(result.corruption));


   expect(result.isCorrupted).toBeTruthy();
   expect(users[0]).toEqual(result.converted.valid[0]);
   expect(users[1]).toEqual(result.origin.corrupted[0]);
   expect(users[3]).toEqual(result.converted.valid[1]);
   expect(result.converted.valid).toHaveLength(1);
   expect(result.converted.corrupted).toHaveLength(2);
   expect(result.origin.corrupted).toHaveLength(2);
   expect(result.origin.excluded).toHaveLength(0);
});


it(`Iterable`, () => {
   class Note {
      public text: string;
      public date: number;
   }

   const NOTE_META = new ObjectMeta({
      builder: Note,
      fields: {
         text: new StringField(),
         date: new NumberField(),
      }
   });

   class User {
      public id: number;
      public notes: Record<string, Note>;
   }

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         id: new NumberField(),
         notes: new ObjectMapField({ meta: NOTE_META }),
      }
   });

   const users = [
      null,
      {
         id: 1,
         notes: {
            first: {
               text: `1`,
               date: 1,
            }
         }
      },
      {
         aaa: 12,
      },
      {
         id: 2,
         notes: {
            1: {
               text: `2`,
               date: 2,
            },
            2: null,
            3: 12,
            4: {
               aaa: 1,
            },
            5: {
               text: `5`,
               aaa: 2
            }
         }
      },
      ``,
      true
   ];

   const result = JTC.convert({
      id: `Users`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values: users,
   });

   console.log(result.converted.all);
   console.log(result.converted.corrupted);
   console.log(result.converted.valid);

   /*
   0 -> null | Members can only be of "object" type, but got "null"
   4 -> '' | Members can only be of "object" type, but got "string"
   5 -> true | Members can only be of "object" type, but got "boolean"
   2 (EXCLUDED) -> {...} | Validation failed for all fields
   2 (EXCLUDED) -> id -> undefined | Value is required
   2 (EXCLUDED) -> notes -> undefined | Value is required
   3 (1) -> notes -> 2 -> null | Members can only be of "object" type, but got "null"
   3 (1) -> notes -> 3 -> 12 | Members can only be of "object" type, but got "number"
   3 (1) -> notes -> 4 (EXCLUDED) -> {...} | Validation failed for all fields
   3 (1) -> notes -> 4 (EXCLUDED) -> date -> undefined | Value is required
   3 (1) -> notes -> 4 (EXCLUDED) -> text -> undefined | Value is required
   3 (1) -> notes -> 5 -> date -> undefined | Value is required
   */
   console.log(JTC.log.asString(result.corruption));


   expect(result.isCorrupted).toBeTruthy();
   expect(users[1]).toEqual(result.converted.valid[0]);
   expect(result.converted.valid).toHaveLength(1);
   expect(result.converted.corrupted).toHaveLength(1);
   expect(result.origin.all).toHaveLength(6);
   expect(result.origin.corrupted).toHaveLength(2);
   expect(result.origin.excluded).toHaveLength(4);
});
