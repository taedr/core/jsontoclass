import { JTC, NumberField, ObjectArrayField, ObjectArrayMeta, ObjectMeta, StringField } from '@taedr/jsontoclass';


it(`Doc - convert - partial`, () => {
   class Note {
      public text: string;
      public date: number;
   }

   const NOTE_META = new ObjectMeta({
      builder: Note,
      fields: {
         date: new NumberField(),
         text: new StringField()
      }
   });

   class User {
      public name: string;
      public age: number;
      public city: string;
      public notes: Note[];
   }

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         age: new NumberField(),
         name: new StringField(),
         city: new StringField(),
         notes: new ObjectArrayField({ meta: NOTE_META }),
      }
   });

   const users = [
      {
         name: `Vasya`
      },
      {
         age: 12
      },
      {
         city: `Odessa`,
         notes: [
            { text: `AAA` },
            { date: 1 },
         ]
      },
      {
         notes: [
            null,
            { text: `BBB` },
            null,
            { date: 2 },
         ]
      },
   ];

   const result = JTC.convert({
      id: `Users`,
      meta: new ObjectArrayMeta({ meta: USER_META, isPartial: true }),
      values: users,
   });

   console.log(result.isCorrupted);    // false
   console.log(result.converted.all);

   expect(result.isCorrupted).toBeFalsy();
   expect(users).toEqual(result.converted.all);
   expect(users).toEqual(result.converted.valid);
});
