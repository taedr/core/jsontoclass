import { JTC, NumberField, ObjectArrayField, ObjectArrayMeta, ObjectField, ObjectMeta, StringField } from '@taedr/jsontoclass';
import { ADDRESS_META } from '../../spec/convert/utils/models/user/address';


it(`Doc - convert - freeze`, () => {
   class Note {
      public text: string;
      public date: number;
   }

   const NOTE_META = new ObjectMeta({
      builder: Note,
      fields: {
         date: new NumberField(),
         text: new StringField()
      }
   })


   class Address {
      public country: string;
      public city: string;
      public street: string;
   }

   const ADDRESS_META = new ObjectMeta({
      builder: Address,
      fields: {
         city: new StringField(),
         country: new StringField(),
         street: new StringField(),
      }
   })

   class User {
      public id: number;
      public name: string;
      public address: Address;
      public notes: Note[];
   }

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         id: new NumberField(),
         name: new StringField(),
         address: new ObjectField({ meta: ADDRESS_META }),
         notes: new ObjectArrayField({ meta: NOTE_META }),
      }
   });

   const users = [
      {
         id: 1,
         name: `Vasya`,
         address: {
            country: `Ukraine`,
            city: `Odessa`,
            street: `Arnautskaya`
         },
         notes: [
            { text: `Go to work`, date: 1235342 },
            { text: `Get home`, date: 654321 },
         ]
      }
   ];

   const result = JTC.convert({
      id: `Users`,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values: users,
      isFreeze: true,
   });

   const [first] = result.converted.all;

   console.log(first);

   // // TypeError: Cannot assign to read only property 'id' of object '#<User>'
   // first.id = 12;
   // // TypeError: Cannot assign to read only property 'city' of object '#<Address>'
   // first.address.city: string;
   // // TypeError: Cannot add property 2, object is not extensible
   // first.notes.push(new Note());
   // // TypeError: Cannot assign to read only property 'date' of object '#<Note>'
   // first.notes[0].date = 12;

   expect(result.isCorrupted).toBeFalsy();
   expect(users).toEqual(result.converted.all);
   expect(users).toEqual(result.converted.valid);


   expect(() => first.id = 12).toThrow(`Cannot assign to read only property 'id' of object '#<User>'`);
   expect(() => first.address.city = ``).toThrow(`Cannot assign to read only property 'city' of object '#<Address>'`);
   expect(() => first.notes.push(new Note())).toThrow(`Cannot add property 2, object is not extensible`);
   expect(() => first.notes[0].date = 12).toThrow(`Cannot assign to read only property 'date' of object '#<Note>'`);
});
