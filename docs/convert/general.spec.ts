import { DimensionalArrayMeta, JTC, MillisDateField, NumberArrayMeta, NumberField, ObjectArrayField, ObjectArrayMeta, ObjectField, ObjectMeta, StringArrayMeta, StringField } from '@taedr/jsontoclass';


it(`Numbers`, () => {
   const values = [1, 2, 3, 4, 5];
   const result = JTC.convert({
      id: `Numbers`,
      meta: new NumberArrayMeta(),
      values,
   });

   console.log(result);

   expect(result.isCorrupted).toBeFalsy();
   expect(result.converted.all).toEqual(values);
});

it(`Strings`, () => {
   const values = [`1`, `2`, `3`, `4`, `5`];
   const result = JTC.convert({
      id: `Strings`,
      meta: new StringArrayMeta(),
      values,
   });

   console.log(result);

   expect(result.isCorrupted).toBeFalsy();
   expect(result.converted.all).toEqual(values);
});

it(`Objects`, () => {
   class Note {
      public text: string;
      public date: Date;
   }

   const NOTE_META = new ObjectMeta({
      builder: Note,
      fields: {
         text: new StringField(),
         date: new MillisDateField(),
      }
   });

   class Address {
      public country: string;
      public city: string;
      public street: string;
   }

   const ADDRESS_META = new ObjectMeta({
      builder: Address,
      fields: {
         city: new StringField(),
         country: new StringField(),
         street: new StringField(),
      }
   });

   class User {
      public id: number;
      public name: string;
      public address: Address;
      public notes: Note[];

      public get short() {
         return `${this.id} - ${this.address.country} - ${this.name}`;
      }
   }

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         id: new NumberField(),
         name: new StringField(),
         address: new ObjectField({ meta: ADDRESS_META }),
         notes: new ObjectArrayField({ meta: NOTE_META }),
         short: new StringField({ isCalculated: true }),
      }
   });

   const users = [
      {
         id: 1,
         name: `Vasya`,
         address: {
            country: `Ukraine`,
            city: `Odessa`,
            street: `Arnautskaya`
         },
         notes: [
            { text: `Go to work`, date: 1235342 },
            { text: `Get home`, date: 654321 },
         ]
      },
      {
         id: 2,
         name: `Petya`,
         address: {
            country: `Ukraine`,
            city: `Kyiv`,
            street: `Shevchenko`
         },
         notes: [
            { text: `Go to store`, date: 6666666 },
            { text: `Make breakfast`, date: 777777 },
         ]
      },
   ];

   const result = JTC.convert({
      // Identifier that will be shown in console before log
      id: `Users`,
      // Meta information about expected data
      meta: new ObjectArrayMeta({ meta: USER_META }),
      // Array of plain objects
      values: users,
   });

   console.log(result.converted.all);

   const all = result.converted.all.map(value => {
      return JTC.deconvert({ value })
   });

   all //?

   const valid = result.converted.valid.map(value => {
      return JTC.deconvert({ value })
   });

   expect(result.isCorrupted).toBeFalsy();
   expect(users).toEqual(all);
   expect(users).toEqual(valid);
});

it(`Arrays`, () => {
   const values = [
      [1, 2, 3, 4, 5],
      [6, 7, 8, 9, 10]
   ];

   const result = JTC.convert({
      id: `Arrays`,
      meta: new DimensionalArrayMeta({
         meta: new NumberArrayMeta(),
      }),
      values,
   });

   console.log(result);

   expect(result.isCorrupted).toBeFalsy();
   expect(result.converted.all).toEqual(values);
});
