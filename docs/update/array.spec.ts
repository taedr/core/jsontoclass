


import { IUpdateEntry, JTC, NumberField, ObjectArrayField, ObjectMeta, StringField } from '@taedr/jsontoclass';
import { TDeepPartial } from '@taedr/utils';


it(`Doc - Update - Array`, () => {
   class Note {
      public text: string;
      public date: number;
   }

   const NOTE_META = new ObjectMeta({
      builder: Note,
      fields: {
         text: new StringField(),
         date: new NumberField()
      }
   });

   class User {
      public id: number;
      public notes: Note[];
   }

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         id: new NumberField(),
         notes: new ObjectArrayField({ meta: NOTE_META }),
      }
   });

   const users: User[] = [
      {
         id: 1,
         notes: [
            { text: `Test_1`, date: 1 },
            { text: `Test_2`, date: 2 },
         ]
      },
      {
         id: 2,
         notes: [
            { text: `Test_3`, date: 3 },
            { text: `Test_4`, date: 4 },
            { text: `Test_5`, date: 5 },
         ]
      }
   ];

   const deltas: TDeepPartial<User>[] = [
      {
         notes: [
            { text: `Hi` },
            { date: 22 }
         ]
      },
      {
         notes: [
            null,
            null,
            { text: `HOI` },
            { text: `Will be skiped` },
         ]
      }
   ];

   const entries: IUpdateEntry<User>[] = [
      { value: users[0], partial: deltas[0] },
      { value: users[1], partial: deltas[1] },
   ];

   const updated = JTC.update({
      id: `Update`,
      metas: [USER_META],
      entries,
   });

   console.log(updated);

   updated[0] //?
   updated[1] //?

   expect(updated[0].id).toEqual(users[0].id);
   expect(updated[0].notes[0].text).toEqual(deltas[0].notes[0].text);
   expect(updated[0].notes[1].date).toEqual(deltas[0].notes[1].date);
   expect(updated[1].notes[0]).toEqual(users[1].notes[0]);
   expect(updated[1].notes[1]).toEqual(users[1].notes[1]);
   expect(updated[1].notes[2].text).toEqual(deltas[1].notes[2].text);
   expect(updated[1].notes[3]).toBeUndefined();
});
