


import { IUpdateEntry, JTC, MillisDateField, NumberField, ObjectArrayField, ObjectArrayMeta, ObjectField, ObjectMeta, StringField } from '@taedr/jsontoclass';


it(`Doc - Update - Complex`, () => {
   class Note {
      public text: string;
      public date: Date;
   }

   const NOTE_META = new ObjectMeta({
      builder: Note,
      fields: {
         text: new StringField(),
         date: new MillisDateField(),
      }
   });

   class Address {
      public country: string;
      public city: string;
   }

   const ADDRESS_META = new ObjectMeta({
      builder: Address,
      fields: {
         city: new StringField(),
         country: new StringField(),
      }
   });

   class User {
      public id: number;
      public name: string;
      public address: Address;
      public notes: Note[];
   }

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         id: new NumberField(),
         name: new StringField(),
         address: new ObjectField({ meta: ADDRESS_META }),
         notes: new ObjectArrayField({ meta: NOTE_META })
      }
   });

   const values = [
      {
         id: 1,
         name: `Vasya`,
         address: {
            country: `Ukraine`,
            city: `Odessa`
         },
         notes: [
            { text: `Do something`, date: 1618088045162 },
            { text: `Don't do anything`, date: 2615408808161 },
            { text: `Reflect`, date: 4233496853323 },
         ]
      },
      {
         id: 2,
         name: `Masha`,
         address: {
            country: `Ukraine`,
            city: `Odessa`
         },
         notes: [
            { text: `Do something`, date: 1618088045162 },
            { text: `Don't do anything`, date: 2615408808161 },
            { text: `Reflect`, date: 4233496853323 },
         ]
      }
   ];

   const result = JTC.convert({
      id: ``,
      meta: new ObjectArrayMeta({ meta: USER_META }),
      values,
   });

   const users = result.converted.all;

   const entries: IUpdateEntry<User>[] = [
      {
         value: users[0],
         partial: {
            name: `Petya`,
            address: {
               city: `Chernomorsk`
            },
            notes: [
               { text: `Hoi` }
            ]
         },
      },
      {
         value: users[1],
         partial: {
            id: 10,
            address: {
               country: `Moldova`
            },
            notes: [
               null,
               { text: `DUI` },
               { date: 11111111111 }
            ]
         },
      }
   ];

   const updated = JTC.update({
      id: `Update`,
      metas: [USER_META],
      entries,
   });

   console.log(updated);


   updated[1]//?


});
