


import { IUpdateEntry, JTC, NumberField, ObjectMeta, StringField, UpdaterError } from '@taedr/jsontoclass';
import { TDeepPartial } from '@taedr/utils';


it(`Doc - Update - Error`, () => {
   class User {
      public id: number;
      public name: string;
      public age: number;
   }

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         id: new NumberField(),
         name: new StringField(),
         age: new NumberField(),
      }
   });

   const users = [
      {
         id: 1,
         name: 567,
      },
      {
         name: `Masha`,
         age: `25`,
      }
   ];

   const deltas: TDeepPartial<User>[] = [
      {
         name: `Andrey`,
      },
      null
   ];

   const entries: IUpdateEntry<any>[] = [
      { value: users[0], partial: deltas[0] },
      { value: users[1], partial: deltas[1] },
   ];

   try {
      JTC.update({
         id: `Update`,
         metas: [USER_META],
         entries,
      });
   } catch (e) {
      const error = e as UpdaterError;

      console.log(error);
      /* 0 -> age -> undefined | Value is required
         0 -> name -> 567 | Expected string, but got number
         1 -> age -> 25 | Expected number, but got string
         1 -> id -> undefined | Value is required
      */
      console.log(JTC.log.asString(error.valuesTree));
      /* 1 -> null | Members can only be of "object" type, but got "null" */
      console.log(JTC.log.asString(error.partialsTree));


      // expect(e).toBeInstanceOf(UpdaterError);
      expect(JTC.log.asString(error.partialsTree)).toEqual(`1 -> null | Members can only be of "object" type, but got "null"`);
      expect(JTC.log.asString(error.valuesTree)).toEqual(
         [
            `0 -> age -> undefined | Value is required`,
            `0 -> name -> 567 | Expected string, but got number`,
            `1 -> age -> 25 | Expected number, but got string`,
            `1 -> id -> undefined | Value is required`
         ].join(`\n`)
      );
   }
});
