import { IUpdateEntry, JTC, NumberField, ObjectField, ObjectMeta, StringField } from "@taedr/jsontoclass";
import { TDeepPartial } from "@taedr/utils";

it(`Doc - Update - Object`, () => {
   class Address {
      public country: string;
      public city: string;
   }

   const ADDRESS_META = new ObjectMeta({
      builder: Address,
      fields: {
         country: new StringField(),
         city: new StringField(),
      }
   });

   class User {
      public id: number;
      public name: string;
      public age: number;
      public address: Address;
   }

   const USER_META = new ObjectMeta({
      builder: User,
      fields: {
         id: new NumberField(),
         name: new StringField(),
         age: new NumberField(),
         address: new ObjectField({ meta: ADDRESS_META })
      }
   });

   const users: User[] = [
      {
         id: 1,
         name: `Vasya`,
         age: 21,
         address: {
            country: `Ukraine`,
            city: `Odessa`
         }
      },
      {
         id: 2,
         name: `Masha`,
         age: 25,
         address: {
            country: `USA`,
            city: `Alexandria`
         }
      }
   ];

   const deltas: TDeepPartial<User>[] = [
      {
         name: `Andrey`,
         address: {
            city: `Chornomorsk`
         }
      },
      {
         age: 32,
         address: {
            country: `Egypt`
         }
      },
   ];

   const entries: IUpdateEntry<User>[] = [
      { value: users[0], partial: deltas[0] },
      { value: users[1], partial: deltas[1] },
   ];

   const updated = JTC.update({
      id: `Update`,
      metas: [USER_META],
      entries,
   });

   console.log(updated);

   expect(updated[0].id).toEqual(users[0].id);
   expect(updated[0].age).toEqual(users[0].age);
   expect(updated[0].name).toEqual(deltas[0].name);
   expect(updated[0].address.country).toEqual(users[0].address.country);
   expect(updated[0].address.city).toEqual(deltas[0].address.city);
   expect(updated[1].id).toEqual(users[1].id);
   expect(updated[1].age).toEqual(deltas[1].age);
   expect(updated[1].name).toEqual(users[1].name);
   expect(updated[1].address.country).toEqual(deltas[1].address.country);
   expect(updated[1].address.city).toEqual(users[1].address.city);
});
